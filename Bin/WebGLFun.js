
/**
 * Root module
 *
 * @module Root
 * @main Root
 */
define('Context',[],function() {
    gl = null;// gl is a global var.
    glExt = null;// glExt is global var
    ctx = null;// ctx is also a global var

    /**
     * This class handles the WebGL context for you.
     * @class Context
     * @constructor
     */
    Context = function() 
    {
        /**
         * A high performance time function.
         * @method getTimestamp
         * @return {Number} Returns the time in milliseconds, with microseconds in the fractional part.
         */       
        if (window.performance.now)
        {
            Context.getTimestamp = function() { return window.performance.now(); };
        } 
        else 
        {
            if (window.performance.webkitNow) 
            {
                Context.getTimestamp = function() { return window.performance.webkitNow(); };
            } 
            else 
            {
                Context.getTimestamp = function() { return new Date().getTime(); };
            }
        }

        ctx = this;
    }

    /**
     * Setups WebGL for the user defined canvas.
     *
     * @method setup
     * @param {Canvas} canvas The HTML element, which must be a canvas element.
     * @return {Bool} Returns false if an error, else true.
     */
    Context.prototype.setup = function(canvas)
    {
        if(!window.WebGLRenderingContext)
        {
            alert("Your browser does not support WebGL.");
            return false;
        }

        try 
        {
            // Try to grab the standard context. If it fails, fallback to experimental.
            gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
        }
        catch(e) 
        {
            return false;
        }

        if(!gl)
        {
            alert("Failed to create a GL context.");
            return null;
        }

        glExt = gl.getExtension("WEBKIT_WEBGL_depth_texture") || gl.getExtension("MOZ_WEBGL_depth_texture");
        gl.getExtension("OES_texture_float");

        this.width = canvas.width;
        this.height = canvas.height;

        gl.viewport(0, 0, this.width, this.height);
        gl.clearColor(0.3, 0.3, 0.3, 1.0);

        return true;
    }

    return Context;
});
/**
 * Math module contains math related stuff.
 *
 * @module Math
 */

/**
 * Added some constants and functions to the built in Math library.
 *
 * @class Math
 * @static
 */
define('Math/MathExt',[],function()
{
	/**
	 * Added a new constant to the built in Math library.
	 * The value equals to Pi / 360.
	 *
	 * @property Math.PI_OVER_360
	 * @type Float
	 */
	Math.PI_OVER_360 = Math.PI / 360.0;

	/**
	 * Added a new constant to the built in Math library.
	 * The value equals to Pi / 180.
	 *
	 * @property Math.PI_OVER_180
	 * @type Float
	 */
	Math.PI_OVER_180 = Math.PI / 180.0;

	/**
	 * Added a new constant to the built in Math library.
	 * The value equals to 180 / Pi.
	 *
	 * @property Math._180_OVER_PI
	 * @type Float
	 */
	Math._180_OVER_PI = 180.0 / Math.PI;

	/**
	 * Checks if two floating point numbers are equal.
	 *
	 * @method isEqual
	 * @param {Float} a Left side
	 * @param {Float} b Right side
	 * @param {Float} epsilon Epsilon defines the precision of the check. Optional, default: 0.00000001
	 * @return {Bool} Returns true if a == b, else false.
	 */
	Math.isEqual = function(a, b, epsilon)
	{
		if(!epsilon)
		{
			epsilon = 0.00000001;
		}
		return Math.abs(a - b) < epsilon;
	}

    /**
     * Converts a angle in degrees to radians.
     *
     * @method degToRad
     * @param {Float} deg Degrees to convert to radians.
     * @return {Float} The angle in radians.
     */
    Math.degToRad = function(deg) 
    {
        return Math.PI_OVER_180 * deg;
    };

    /**
     * Converts a angle in radians to degrees.
     *
     * @method radToDeg
     * @param {float} rad Radians to convert to degrees.
     * @return {Float} The angle in degrees.
     */
    Math.radToDeg = function(rad) 
    {
        return Math._180_OVER_PI * rad;
    };
});
define('ShaderProgram',[],function()
{
	/**
	 * Shader program wraps the GLSL Progam.
	 * You got more features with ShaderProgram.
	 * Use {{#crossLink "Utils/createShaderProgram:method"}}{{/crossLink}} to create a ShaderProgram.
	 *
	 * @class ShaderProgram
	 * @constructor
	 */
	ShaderProgram = function()
	{
	}

	/**
	 * Creates a ShaderProgram by using a GL Program.
	 *
	 * @private
	 * @method _create
	 * @param {Program} programObject GL program object.
	 * @return {ShaderProgram} Returns a ShaderProgram.
	 */
	ShaderProgram._create = function(programObject) 
	{
		var out = new ShaderProgram;

		/**
		 * GL Program Object
		 * @private
		 * @property _programObject
		 * @type Program
		 */
		out._programObject = programObject;

		/**
		 * Uniform locations
		 * @private
		 * @property _uniformLocations
		 * @type [String] = int
		 */
		out._uniformLocations = new Object;

		return out;
	};

	/**
	 * Makes this program as current program for rendering.
	 *
	 * @method use
	 */
	ShaderProgram.prototype.use = function() 
	{
		gl.useProgram(this._programObject);
	};

	/**
	 * Returns the uniform location of a uniform.
	 * @method getUniformLocation
	 * @param {String} name Uniform name.
	 */
	ShaderProgram.prototype.getUniformLocation = function(name) 
	{
		if(!(name in this._uniformLocations))
		{
			this._uniformLocations[name] = gl.getUniformLocation(this._programObject, name);
		}

		return this._uniformLocations[name];
	};
});
/**
 * This class gives you access to some nice to have functions.
 *
 * @class Utils
 * @static
 */
 define('Utils',["Math/MathExt", "ShaderProgram"], function()
 {
 	Utils = {};

    var numLoadRequests = 0;
    var numComplete = 0;

	/**
     * Compiles a shader.
     * 
     * @method compileShader
     * @param {String} src The shader source.
     * @param {Object} shaderType The type of shader, which maybe VERTEX_SHADER or FRAGMENT_SHADER.
     * @return Returns null if an error, else the shader object.
     */
    Utils.compileShader = function(src, shaderType)
    {
        shader = gl.createShader (shaderType);
        gl.shaderSource(shader, src);
        gl.compileShader(shader);
        if (gl.getShaderParameter(shader, gl.COMPILE_STATUS) == 0)
        {
            alert("\n" + gl.getShaderInfoLog(shader));
        }

        return shader;
    }

    /**
     * Downloads a file from the server, using XMLHttpRequest.
     * If you want to load more than one file, you should instead use {{#crossLink "Utils/loadFiles:method"}}{{/crossLink}}.
     * 
     * @method loadFile
     * @param {String} url Location of file.
     * @param {Object} extraData This parameter would be passed 
     *                           to the callback function as a parameter.
     * @param {Function} callback A function that would be called when loading is ready.
     * @async
     */
    Utils.loadFile = function(url, extraData, callback)
    {
        var request = new XMLHttpRequest();
        request.open('GET', url, true);
        var realThis = this;

        // Hook the event that gets called as the request progresses
        request.onreadystatechange = function () 
        {
            // If the request is "DONE" (completed or failed)
            if (request.readyState == 4) 
            {
                // If we got HTTP status 200 (OK)
                if (request.status == 200) 
                {
                    callback.call(realThis, request.responseText, extraData);
                } else 
                { 
                    console.error("Couldn't download " + url);
                }
            }
        };

        request.send(null);
    }

    /**
     * Downloads files from the server, using XMLHttpRequest.
     *
     * @method loadFiles
     * @param {Array(String)} urls Locations of the files.
     * @param {Function} callback A function that would be called when loading is ready.
     * @async
     */
    Utils.loadFiles = function(urls, callback)
    {
        numLoadRequests += urls.length;
        var result = new Array;

        function callback_internal(str, urlIdx)
        {
            result[urlIdx] = str;
            ++numComplete;

            if(numComplete == numLoadRequests)
            {
                callback.call(this, result);
            }
        }

        for(var i = 0;i < numLoadRequests;++i)
        {
            Utils.loadFile.call(this, urls[i], i, callback_internal);
        }
    }

    /**
     * Creates a shader program, and attaches the given shaders, 
     * and finally links the program.
     *
     * @method createShaderProgram
     * @param {Shader} shaders Shader objects, which would be attached to the program.
     * @param {Strings} attributes An array of strings, which stores the name of the program attributes in GLSL. 
     * @return {Program} Returns the program object, else null. 
     */
    Utils.createShaderProgram = function(shaders, attributes)
    {
        var prog = gl.createProgram();

        for(var i = 0;i < shaders.length;++i)
        {
            gl.attachShader(prog, shaders[i]);
        }
        
        for(var i = 0;i < attributes.length;++i)
        {
            gl.bindAttribLocation(prog, i, attributes[i]);
        }
        
        gl.linkProgram(prog);

        return ShaderProgram._create(prog);
    }

    /**
     * Implements isInteger for all browsers.
     *
     * Found here:
     * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger
     *
     * @method Number.isInteger
     * @param {Number} nVal Number to check.
     * @return {Bool} Returns true if nVal is a integer.
     */
    if (!Number.isInteger) 
    {
        Number.isInteger = function isInteger (nVal) 
        {
            return typeof nVal === "number" && isFinite(nVal) && nVal > -9007199254740992 && nVal < 9007199254740992 && Math.floor(nVal) === nVal;
        };
    }

    return Utils;
 });
/**
 * @fileoverview gl-matrix - High performance matrix and vector operations
 * @author Brandon Jones
 * @author Colin MacKenzie IV
 * @version 2.2.0
 */
/* Copyright (c) 2013, Brandon Jones, Colin MacKenzie IV. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation 
    and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
(function(e){var t={};typeof exports=="undefined"?typeof define=="function"&&typeof define.amd=="object"&&define.amd?(t.exports={},define('libs/gl-matrix',[],function(){return t.exports})):t.exports=typeof window!="undefined"?window:e:t.exports=exports,function(e){if(!t)var t=1e-6;if(!n)var n=typeof Float32Array!="undefined"?Float32Array:Array;if(!r)var r=Math.random;var i={};i.setMatrixArrayType=function(e){n=e},typeof e!="undefined"&&(e.glMatrix=i);var s={};s.create=function(){var e=new n(2);return e[0]=0,e[1]=0,e},s.clone=function(e){var t=new n(2);return t[0]=e[0],t[1]=e[1],t},s.fromValues=function(e,t){var r=new n(2);return r[0]=e,r[1]=t,r},s.copy=function(e,t){return e[0]=t[0],e[1]=t[1],e},s.set=function(e,t,n){return e[0]=t,e[1]=n,e},s.add=function(e,t,n){return e[0]=t[0]+n[0],e[1]=t[1]+n[1],e},s.subtract=function(e,t,n){return e[0]=t[0]-n[0],e[1]=t[1]-n[1],e},s.sub=s.subtract,s.multiply=function(e,t,n){return e[0]=t[0]*n[0],e[1]=t[1]*n[1],e},s.mul=s.multiply,s.divide=function(e,t,n){return e[0]=t[0]/n[0],e[1]=t[1]/n[1],e},s.div=s.divide,s.min=function(e,t,n){return e[0]=Math.min(t[0],n[0]),e[1]=Math.min(t[1],n[1]),e},s.max=function(e,t,n){return e[0]=Math.max(t[0],n[0]),e[1]=Math.max(t[1],n[1]),e},s.scale=function(e,t,n){return e[0]=t[0]*n,e[1]=t[1]*n,e},s.scaleAndAdd=function(e,t,n,r){return e[0]=t[0]+n[0]*r,e[1]=t[1]+n[1]*r,e},s.distance=function(e,t){var n=t[0]-e[0],r=t[1]-e[1];return Math.sqrt(n*n+r*r)},s.dist=s.distance,s.squaredDistance=function(e,t){var n=t[0]-e[0],r=t[1]-e[1];return n*n+r*r},s.sqrDist=s.squaredDistance,s.length=function(e){var t=e[0],n=e[1];return Math.sqrt(t*t+n*n)},s.len=s.length,s.squaredLength=function(e){var t=e[0],n=e[1];return t*t+n*n},s.sqrLen=s.squaredLength,s.negate=function(e,t){return e[0]=-t[0],e[1]=-t[1],e},s.normalize=function(e,t){var n=t[0],r=t[1],i=n*n+r*r;return i>0&&(i=1/Math.sqrt(i),e[0]=t[0]*i,e[1]=t[1]*i),e},s.dot=function(e,t){return e[0]*t[0]+e[1]*t[1]},s.cross=function(e,t,n){var r=t[0]*n[1]-t[1]*n[0];return e[0]=e[1]=0,e[2]=r,e},s.lerp=function(e,t,n,r){var i=t[0],s=t[1];return e[0]=i+r*(n[0]-i),e[1]=s+r*(n[1]-s),e},s.random=function(e,t){t=t||1;var n=r()*2*Math.PI;return e[0]=Math.cos(n)*t,e[1]=Math.sin(n)*t,e},s.transformMat2=function(e,t,n){var r=t[0],i=t[1];return e[0]=n[0]*r+n[2]*i,e[1]=n[1]*r+n[3]*i,e},s.transformMat2d=function(e,t,n){var r=t[0],i=t[1];return e[0]=n[0]*r+n[2]*i+n[4],e[1]=n[1]*r+n[3]*i+n[5],e},s.transformMat3=function(e,t,n){var r=t[0],i=t[1];return e[0]=n[0]*r+n[3]*i+n[6],e[1]=n[1]*r+n[4]*i+n[7],e},s.transformMat4=function(e,t,n){var r=t[0],i=t[1];return e[0]=n[0]*r+n[4]*i+n[12],e[1]=n[1]*r+n[5]*i+n[13],e},s.forEach=function(){var e=s.create();return function(t,n,r,i,s,o){var u,a;n||(n=2),r||(r=0),i?a=Math.min(i*n+r,t.length):a=t.length;for(u=r;u<a;u+=n)e[0]=t[u],e[1]=t[u+1],s(e,e,o),t[u]=e[0],t[u+1]=e[1];return t}}(),s.str=function(e){return"vec2("+e[0]+", "+e[1]+")"},typeof e!="undefined"&&(e.vec2=s);var o={};o.create=function(){var e=new n(3);return e[0]=0,e[1]=0,e[2]=0,e},o.clone=function(e){var t=new n(3);return t[0]=e[0],t[1]=e[1],t[2]=e[2],t},o.fromValues=function(e,t,r){var i=new n(3);return i[0]=e,i[1]=t,i[2]=r,i},o.copy=function(e,t){return e[0]=t[0],e[1]=t[1],e[2]=t[2],e},o.set=function(e,t,n,r){return e[0]=t,e[1]=n,e[2]=r,e},o.add=function(e,t,n){return e[0]=t[0]+n[0],e[1]=t[1]+n[1],e[2]=t[2]+n[2],e},o.subtract=function(e,t,n){return e[0]=t[0]-n[0],e[1]=t[1]-n[1],e[2]=t[2]-n[2],e},o.sub=o.subtract,o.multiply=function(e,t,n){return e[0]=t[0]*n[0],e[1]=t[1]*n[1],e[2]=t[2]*n[2],e},o.mul=o.multiply,o.divide=function(e,t,n){return e[0]=t[0]/n[0],e[1]=t[1]/n[1],e[2]=t[2]/n[2],e},o.div=o.divide,o.min=function(e,t,n){return e[0]=Math.min(t[0],n[0]),e[1]=Math.min(t[1],n[1]),e[2]=Math.min(t[2],n[2]),e},o.max=function(e,t,n){return e[0]=Math.max(t[0],n[0]),e[1]=Math.max(t[1],n[1]),e[2]=Math.max(t[2],n[2]),e},o.scale=function(e,t,n){return e[0]=t[0]*n,e[1]=t[1]*n,e[2]=t[2]*n,e},o.scaleAndAdd=function(e,t,n,r){return e[0]=t[0]+n[0]*r,e[1]=t[1]+n[1]*r,e[2]=t[2]+n[2]*r,e},o.distance=function(e,t){var n=t[0]-e[0],r=t[1]-e[1],i=t[2]-e[2];return Math.sqrt(n*n+r*r+i*i)},o.dist=o.distance,o.squaredDistance=function(e,t){var n=t[0]-e[0],r=t[1]-e[1],i=t[2]-e[2];return n*n+r*r+i*i},o.sqrDist=o.squaredDistance,o.length=function(e){var t=e[0],n=e[1],r=e[2];return Math.sqrt(t*t+n*n+r*r)},o.len=o.length,o.squaredLength=function(e){var t=e[0],n=e[1],r=e[2];return t*t+n*n+r*r},o.sqrLen=o.squaredLength,o.negate=function(e,t){return e[0]=-t[0],e[1]=-t[1],e[2]=-t[2],e},o.normalize=function(e,t){var n=t[0],r=t[1],i=t[2],s=n*n+r*r+i*i;return s>0&&(s=1/Math.sqrt(s),e[0]=t[0]*s,e[1]=t[1]*s,e[2]=t[2]*s),e},o.dot=function(e,t){return e[0]*t[0]+e[1]*t[1]+e[2]*t[2]},o.cross=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=n[0],u=n[1],a=n[2];return e[0]=i*a-s*u,e[1]=s*o-r*a,e[2]=r*u-i*o,e},o.lerp=function(e,t,n,r){var i=t[0],s=t[1],o=t[2];return e[0]=i+r*(n[0]-i),e[1]=s+r*(n[1]-s),e[2]=o+r*(n[2]-o),e},o.random=function(e,t){t=t||1;var n=r()*2*Math.PI,i=r()*2-1,s=Math.sqrt(1-i*i)*t;return e[0]=Math.cos(n)*s,e[1]=Math.sin(n)*s,e[2]=i*t,e},o.transformMat4=function(e,t,n){var r=t[0],i=t[1],s=t[2];return e[0]=n[0]*r+n[4]*i+n[8]*s+n[12],e[1]=n[1]*r+n[5]*i+n[9]*s+n[13],e[2]=n[2]*r+n[6]*i+n[10]*s+n[14],e},o.transformMat3=function(e,t,n){var r=t[0],i=t[1],s=t[2];return e[0]=r*n[0]+i*n[3]+s*n[6],e[1]=r*n[1]+i*n[4]+s*n[7],e[2]=r*n[2]+i*n[5]+s*n[8],e},o.transformQuat=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=n[0],u=n[1],a=n[2],f=n[3],l=f*r+u*s-a*i,c=f*i+a*r-o*s,h=f*s+o*i-u*r,p=-o*r-u*i-a*s;return e[0]=l*f+p*-o+c*-a-h*-u,e[1]=c*f+p*-u+h*-o-l*-a,e[2]=h*f+p*-a+l*-u-c*-o,e},o.forEach=function(){var e=o.create();return function(t,n,r,i,s,o){var u,a;n||(n=3),r||(r=0),i?a=Math.min(i*n+r,t.length):a=t.length;for(u=r;u<a;u+=n)e[0]=t[u],e[1]=t[u+1],e[2]=t[u+2],s(e,e,o),t[u]=e[0],t[u+1]=e[1],t[u+2]=e[2];return t}}(),o.str=function(e){return"vec3("+e[0]+", "+e[1]+", "+e[2]+")"},typeof e!="undefined"&&(e.vec3=o);var u={};u.create=function(){var e=new n(4);return e[0]=0,e[1]=0,e[2]=0,e[3]=0,e},u.clone=function(e){var t=new n(4);return t[0]=e[0],t[1]=e[1],t[2]=e[2],t[3]=e[3],t},u.fromValues=function(e,t,r,i){var s=new n(4);return s[0]=e,s[1]=t,s[2]=r,s[3]=i,s},u.copy=function(e,t){return e[0]=t[0],e[1]=t[1],e[2]=t[2],e[3]=t[3],e},u.set=function(e,t,n,r,i){return e[0]=t,e[1]=n,e[2]=r,e[3]=i,e},u.add=function(e,t,n){return e[0]=t[0]+n[0],e[1]=t[1]+n[1],e[2]=t[2]+n[2],e[3]=t[3]+n[3],e},u.subtract=function(e,t,n){return e[0]=t[0]-n[0],e[1]=t[1]-n[1],e[2]=t[2]-n[2],e[3]=t[3]-n[3],e},u.sub=u.subtract,u.multiply=function(e,t,n){return e[0]=t[0]*n[0],e[1]=t[1]*n[1],e[2]=t[2]*n[2],e[3]=t[3]*n[3],e},u.mul=u.multiply,u.divide=function(e,t,n){return e[0]=t[0]/n[0],e[1]=t[1]/n[1],e[2]=t[2]/n[2],e[3]=t[3]/n[3],e},u.div=u.divide,u.min=function(e,t,n){return e[0]=Math.min(t[0],n[0]),e[1]=Math.min(t[1],n[1]),e[2]=Math.min(t[2],n[2]),e[3]=Math.min(t[3],n[3]),e},u.max=function(e,t,n){return e[0]=Math.max(t[0],n[0]),e[1]=Math.max(t[1],n[1]),e[2]=Math.max(t[2],n[2]),e[3]=Math.max(t[3],n[3]),e},u.scale=function(e,t,n){return e[0]=t[0]*n,e[1]=t[1]*n,e[2]=t[2]*n,e[3]=t[3]*n,e},u.scaleAndAdd=function(e,t,n,r){return e[0]=t[0]+n[0]*r,e[1]=t[1]+n[1]*r,e[2]=t[2]+n[2]*r,e[3]=t[3]+n[3]*r,e},u.distance=function(e,t){var n=t[0]-e[0],r=t[1]-e[1],i=t[2]-e[2],s=t[3]-e[3];return Math.sqrt(n*n+r*r+i*i+s*s)},u.dist=u.distance,u.squaredDistance=function(e,t){var n=t[0]-e[0],r=t[1]-e[1],i=t[2]-e[2],s=t[3]-e[3];return n*n+r*r+i*i+s*s},u.sqrDist=u.squaredDistance,u.length=function(e){var t=e[0],n=e[1],r=e[2],i=e[3];return Math.sqrt(t*t+n*n+r*r+i*i)},u.len=u.length,u.squaredLength=function(e){var t=e[0],n=e[1],r=e[2],i=e[3];return t*t+n*n+r*r+i*i},u.sqrLen=u.squaredLength,u.negate=function(e,t){return e[0]=-t[0],e[1]=-t[1],e[2]=-t[2],e[3]=-t[3],e},u.normalize=function(e,t){var n=t[0],r=t[1],i=t[2],s=t[3],o=n*n+r*r+i*i+s*s;return o>0&&(o=1/Math.sqrt(o),e[0]=t[0]*o,e[1]=t[1]*o,e[2]=t[2]*o,e[3]=t[3]*o),e},u.dot=function(e,t){return e[0]*t[0]+e[1]*t[1]+e[2]*t[2]+e[3]*t[3]},u.lerp=function(e,t,n,r){var i=t[0],s=t[1],o=t[2],u=t[3];return e[0]=i+r*(n[0]-i),e[1]=s+r*(n[1]-s),e[2]=o+r*(n[2]-o),e[3]=u+r*(n[3]-u),e},u.random=function(e,t){return t=t||1,e[0]=r(),e[1]=r(),e[2]=r(),e[3]=r(),u.normalize(e,e),u.scale(e,e,t),e},u.transformMat4=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=t[3];return e[0]=n[0]*r+n[4]*i+n[8]*s+n[12]*o,e[1]=n[1]*r+n[5]*i+n[9]*s+n[13]*o,e[2]=n[2]*r+n[6]*i+n[10]*s+n[14]*o,e[3]=n[3]*r+n[7]*i+n[11]*s+n[15]*o,e},u.transformQuat=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=n[0],u=n[1],a=n[2],f=n[3],l=f*r+u*s-a*i,c=f*i+a*r-o*s,h=f*s+o*i-u*r,p=-o*r-u*i-a*s;return e[0]=l*f+p*-o+c*-a-h*-u,e[1]=c*f+p*-u+h*-o-l*-a,e[2]=h*f+p*-a+l*-u-c*-o,e},u.forEach=function(){var e=u.create();return function(t,n,r,i,s,o){var u,a;n||(n=4),r||(r=0),i?a=Math.min(i*n+r,t.length):a=t.length;for(u=r;u<a;u+=n)e[0]=t[u],e[1]=t[u+1],e[2]=t[u+2],e[3]=t[u+3],s(e,e,o),t[u]=e[0],t[u+1]=e[1],t[u+2]=e[2],t[u+3]=e[3];return t}}(),u.str=function(e){return"vec4("+e[0]+", "+e[1]+", "+e[2]+", "+e[3]+")"},typeof e!="undefined"&&(e.vec4=u);var a={};a.create=function(){var e=new n(4);return e[0]=1,e[1]=0,e[2]=0,e[3]=1,e},a.clone=function(e){var t=new n(4);return t[0]=e[0],t[1]=e[1],t[2]=e[2],t[3]=e[3],t},a.copy=function(e,t){return e[0]=t[0],e[1]=t[1],e[2]=t[2],e[3]=t[3],e},a.identity=function(e){return e[0]=1,e[1]=0,e[2]=0,e[3]=1,e},a.transpose=function(e,t){if(e===t){var n=t[1];e[1]=t[2],e[2]=n}else e[0]=t[0],e[1]=t[2],e[2]=t[1],e[3]=t[3];return e},a.invert=function(e,t){var n=t[0],r=t[1],i=t[2],s=t[3],o=n*s-i*r;return o?(o=1/o,e[0]=s*o,e[1]=-r*o,e[2]=-i*o,e[3]=n*o,e):null},a.adjoint=function(e,t){var n=t[0];return e[0]=t[3],e[1]=-t[1],e[2]=-t[2],e[3]=n,e},a.determinant=function(e){return e[0]*e[3]-e[2]*e[1]},a.multiply=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=t[3],u=n[0],a=n[1],f=n[2],l=n[3];return e[0]=r*u+i*f,e[1]=r*a+i*l,e[2]=s*u+o*f,e[3]=s*a+o*l,e},a.mul=a.multiply,a.rotate=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=t[3],u=Math.sin(n),a=Math.cos(n);return e[0]=r*a+i*u,e[1]=r*-u+i*a,e[2]=s*a+o*u,e[3]=s*-u+o*a,e},a.scale=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=t[3],u=n[0],a=n[1];return e[0]=r*u,e[1]=i*a,e[2]=s*u,e[3]=o*a,e},a.str=function(e){return"mat2("+e[0]+", "+e[1]+", "+e[2]+", "+e[3]+")"},typeof e!="undefined"&&(e.mat2=a);var f={};f.create=function(){var e=new n(6);return e[0]=1,e[1]=0,e[2]=0,e[3]=1,e[4]=0,e[5]=0,e},f.clone=function(e){var t=new n(6);return t[0]=e[0],t[1]=e[1],t[2]=e[2],t[3]=e[3],t[4]=e[4],t[5]=e[5],t},f.copy=function(e,t){return e[0]=t[0],e[1]=t[1],e[2]=t[2],e[3]=t[3],e[4]=t[4],e[5]=t[5],e},f.identity=function(e){return e[0]=1,e[1]=0,e[2]=0,e[3]=1,e[4]=0,e[5]=0,e},f.invert=function(e,t){var n=t[0],r=t[1],i=t[2],s=t[3],o=t[4],u=t[5],a=n*s-r*i;return a?(a=1/a,e[0]=s*a,e[1]=-r*a,e[2]=-i*a,e[3]=n*a,e[4]=(i*u-s*o)*a,e[5]=(r*o-n*u)*a,e):null},f.determinant=function(e){return e[0]*e[3]-e[1]*e[2]},f.multiply=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=t[3],u=t[4],a=t[5],f=n[0],l=n[1],c=n[2],h=n[3],p=n[4],d=n[5];return e[0]=r*f+i*c,e[1]=r*l+i*h,e[2]=s*f+o*c,e[3]=s*l+o*h,e[4]=f*u+c*a+p,e[5]=l*u+h*a+d,e},f.mul=f.multiply,f.rotate=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=t[3],u=t[4],a=t[5],f=Math.sin(n),l=Math.cos(n);return e[0]=r*l+i*f,e[1]=-r*f+i*l,e[2]=s*l+o*f,e[3]=-s*f+l*o,e[4]=l*u+f*a,e[5]=l*a-f*u,e},f.scale=function(e,t,n){var r=n[0],i=n[1];return e[0]=t[0]*r,e[1]=t[1]*i,e[2]=t[2]*r,e[3]=t[3]*i,e[4]=t[4]*r,e[5]=t[5]*i,e},f.translate=function(e,t,n){return e[0]=t[0],e[1]=t[1],e[2]=t[2],e[3]=t[3],e[4]=t[4]+n[0],e[5]=t[5]+n[1],e},f.str=function(e){return"mat2d("+e[0]+", "+e[1]+", "+e[2]+", "+e[3]+", "+e[4]+", "+e[5]+")"},typeof e!="undefined"&&(e.mat2d=f);var l={};l.create=function(){var e=new n(9);return e[0]=1,e[1]=0,e[2]=0,e[3]=0,e[4]=1,e[5]=0,e[6]=0,e[7]=0,e[8]=1,e},l.fromMat4=function(e,t){return e[0]=t[0],e[1]=t[1],e[2]=t[2],e[3]=t[4],e[4]=t[5],e[5]=t[6],e[6]=t[8],e[7]=t[9],e[8]=t[10],e},l.clone=function(e){var t=new n(9);return t[0]=e[0],t[1]=e[1],t[2]=e[2],t[3]=e[3],t[4]=e[4],t[5]=e[5],t[6]=e[6],t[7]=e[7],t[8]=e[8],t},l.copy=function(e,t){return e[0]=t[0],e[1]=t[1],e[2]=t[2],e[3]=t[3],e[4]=t[4],e[5]=t[5],e[6]=t[6],e[7]=t[7],e[8]=t[8],e},l.identity=function(e){return e[0]=1,e[1]=0,e[2]=0,e[3]=0,e[4]=1,e[5]=0,e[6]=0,e[7]=0,e[8]=1,e},l.transpose=function(e,t){if(e===t){var n=t[1],r=t[2],i=t[5];e[1]=t[3],e[2]=t[6],e[3]=n,e[5]=t[7],e[6]=r,e[7]=i}else e[0]=t[0],e[1]=t[3],e[2]=t[6],e[3]=t[1],e[4]=t[4],e[5]=t[7],e[6]=t[2],e[7]=t[5],e[8]=t[8];return e},l.invert=function(e,t){var n=t[0],r=t[1],i=t[2],s=t[3],o=t[4],u=t[5],a=t[6],f=t[7],l=t[8],c=l*o-u*f,h=-l*s+u*a,p=f*s-o*a,d=n*c+r*h+i*p;return d?(d=1/d,e[0]=c*d,e[1]=(-l*r+i*f)*d,e[2]=(u*r-i*o)*d,e[3]=h*d,e[4]=(l*n-i*a)*d,e[5]=(-u*n+i*s)*d,e[6]=p*d,e[7]=(-f*n+r*a)*d,e[8]=(o*n-r*s)*d,e):null},l.adjoint=function(e,t){var n=t[0],r=t[1],i=t[2],s=t[3],o=t[4],u=t[5],a=t[6],f=t[7],l=t[8];return e[0]=o*l-u*f,e[1]=i*f-r*l,e[2]=r*u-i*o,e[3]=u*a-s*l,e[4]=n*l-i*a,e[5]=i*s-n*u,e[6]=s*f-o*a,e[7]=r*a-n*f,e[8]=n*o-r*s,e},l.determinant=function(e){var t=e[0],n=e[1],r=e[2],i=e[3],s=e[4],o=e[5],u=e[6],a=e[7],f=e[8];return t*(f*s-o*a)+n*(-f*i+o*u)+r*(a*i-s*u)},l.multiply=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=t[3],u=t[4],a=t[5],f=t[6],l=t[7],c=t[8],h=n[0],p=n[1],d=n[2],v=n[3],m=n[4],g=n[5],y=n[6],b=n[7],w=n[8];return e[0]=h*r+p*o+d*f,e[1]=h*i+p*u+d*l,e[2]=h*s+p*a+d*c,e[3]=v*r+m*o+g*f,e[4]=v*i+m*u+g*l,e[5]=v*s+m*a+g*c,e[6]=y*r+b*o+w*f,e[7]=y*i+b*u+w*l,e[8]=y*s+b*a+w*c,e},l.mul=l.multiply,l.translate=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=t[3],u=t[4],a=t[5],f=t[6],l=t[7],c=t[8],h=n[0],p=n[1];return e[0]=r,e[1]=i,e[2]=s,e[3]=o,e[4]=u,e[5]=a,e[6]=h*r+p*o+f,e[7]=h*i+p*u+l,e[8]=h*s+p*a+c,e},l.rotate=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=t[3],u=t[4],a=t[5],f=t[6],l=t[7],c=t[8],h=Math.sin(n),p=Math.cos(n);return e[0]=p*r+h*o,e[1]=p*i+h*u,e[2]=p*s+h*a,e[3]=p*o-h*r,e[4]=p*u-h*i,e[5]=p*a-h*s,e[6]=f,e[7]=l,e[8]=c,e},l.scale=function(e,t,n){var r=n[0],i=n[1];return e[0]=r*t[0],e[1]=r*t[1],e[2]=r*t[2],e[3]=i*t[3],e[4]=i*t[4],e[5]=i*t[5],e[6]=t[6],e[7]=t[7],e[8]=t[8],e},l.fromMat2d=function(e,t){return e[0]=t[0],e[1]=t[1],e[2]=0,e[3]=t[2],e[4]=t[3],e[5]=0,e[6]=t[4],e[7]=t[5],e[8]=1,e},l.fromQuat=function(e,t){var n=t[0],r=t[1],i=t[2],s=t[3],o=n+n,u=r+r,a=i+i,f=n*o,l=n*u,c=n*a,h=r*u,p=r*a,d=i*a,v=s*o,m=s*u,g=s*a;return e[0]=1-(h+d),e[3]=l+g,e[6]=c-m,e[1]=l-g,e[4]=1-(f+d),e[7]=p+v,e[2]=c+m,e[5]=p-v,e[8]=1-(f+h),e},l.normalFromMat4=function(e,t){var n=t[0],r=t[1],i=t[2],s=t[3],o=t[4],u=t[5],a=t[6],f=t[7],l=t[8],c=t[9],h=t[10],p=t[11],d=t[12],v=t[13],m=t[14],g=t[15],y=n*u-r*o,b=n*a-i*o,w=n*f-s*o,E=r*a-i*u,S=r*f-s*u,x=i*f-s*a,T=l*v-c*d,N=l*m-h*d,C=l*g-p*d,k=c*m-h*v,L=c*g-p*v,A=h*g-p*m,O=y*A-b*L+w*k+E*C-S*N+x*T;return O?(O=1/O,e[0]=(u*A-a*L+f*k)*O,e[1]=(a*C-o*A-f*N)*O,e[2]=(o*L-u*C+f*T)*O,e[3]=(i*L-r*A-s*k)*O,e[4]=(n*A-i*C+s*N)*O,e[5]=(r*C-n*L-s*T)*O,e[6]=(v*x-m*S+g*E)*O,e[7]=(m*w-d*x-g*b)*O,e[8]=(d*S-v*w+g*y)*O,e):null},l.str=function(e){return"mat3("+e[0]+", "+e[1]+", "+e[2]+", "+e[3]+", "+e[4]+", "+e[5]+", "+e[6]+", "+e[7]+", "+e[8]+")"},typeof e!="undefined"&&(e.mat3=l);var c={};c.create=function(){var e=new n(16);return e[0]=1,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=1,e[6]=0,e[7]=0,e[8]=0,e[9]=0,e[10]=1,e[11]=0,e[12]=0,e[13]=0,e[14]=0,e[15]=1,e},c.clone=function(e){var t=new n(16);return t[0]=e[0],t[1]=e[1],t[2]=e[2],t[3]=e[3],t[4]=e[4],t[5]=e[5],t[6]=e[6],t[7]=e[7],t[8]=e[8],t[9]=e[9],t[10]=e[10],t[11]=e[11],t[12]=e[12],t[13]=e[13],t[14]=e[14],t[15]=e[15],t},c.copy=function(e,t){return e[0]=t[0],e[1]=t[1],e[2]=t[2],e[3]=t[3],e[4]=t[4],e[5]=t[5],e[6]=t[6],e[7]=t[7],e[8]=t[8],e[9]=t[9],e[10]=t[10],e[11]=t[11],e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15],e},c.identity=function(e){return e[0]=1,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=1,e[6]=0,e[7]=0,e[8]=0,e[9]=0,e[10]=1,e[11]=0,e[12]=0,e[13]=0,e[14]=0,e[15]=1,e},c.transpose=function(e,t){if(e===t){var n=t[1],r=t[2],i=t[3],s=t[6],o=t[7],u=t[11];e[1]=t[4],e[2]=t[8],e[3]=t[12],e[4]=n,e[6]=t[9],e[7]=t[13],e[8]=r,e[9]=s,e[11]=t[14],e[12]=i,e[13]=o,e[14]=u}else e[0]=t[0],e[1]=t[4],e[2]=t[8],e[3]=t[12],e[4]=t[1],e[5]=t[5],e[6]=t[9],e[7]=t[13],e[8]=t[2],e[9]=t[6],e[10]=t[10],e[11]=t[14],e[12]=t[3],e[13]=t[7],e[14]=t[11],e[15]=t[15];return e},c.invert=function(e,t){var n=t[0],r=t[1],i=t[2],s=t[3],o=t[4],u=t[5],a=t[6],f=t[7],l=t[8],c=t[9],h=t[10],p=t[11],d=t[12],v=t[13],m=t[14],g=t[15],y=n*u-r*o,b=n*a-i*o,w=n*f-s*o,E=r*a-i*u,S=r*f-s*u,x=i*f-s*a,T=l*v-c*d,N=l*m-h*d,C=l*g-p*d,k=c*m-h*v,L=c*g-p*v,A=h*g-p*m,O=y*A-b*L+w*k+E*C-S*N+x*T;return O?(O=1/O,e[0]=(u*A-a*L+f*k)*O,e[1]=(i*L-r*A-s*k)*O,e[2]=(v*x-m*S+g*E)*O,e[3]=(h*S-c*x-p*E)*O,e[4]=(a*C-o*A-f*N)*O,e[5]=(n*A-i*C+s*N)*O,e[6]=(m*w-d*x-g*b)*O,e[7]=(l*x-h*w+p*b)*O,e[8]=(o*L-u*C+f*T)*O,e[9]=(r*C-n*L-s*T)*O,e[10]=(d*S-v*w+g*y)*O,e[11]=(c*w-l*S-p*y)*O,e[12]=(u*N-o*k-a*T)*O,e[13]=(n*k-r*N+i*T)*O,e[14]=(v*b-d*E-m*y)*O,e[15]=(l*E-c*b+h*y)*O,e):null},c.adjoint=function(e,t){var n=t[0],r=t[1],i=t[2],s=t[3],o=t[4],u=t[5],a=t[6],f=t[7],l=t[8],c=t[9],h=t[10],p=t[11],d=t[12],v=t[13],m=t[14],g=t[15];return e[0]=u*(h*g-p*m)-c*(a*g-f*m)+v*(a*p-f*h),e[1]=-(r*(h*g-p*m)-c*(i*g-s*m)+v*(i*p-s*h)),e[2]=r*(a*g-f*m)-u*(i*g-s*m)+v*(i*f-s*a),e[3]=-(r*(a*p-f*h)-u*(i*p-s*h)+c*(i*f-s*a)),e[4]=-(o*(h*g-p*m)-l*(a*g-f*m)+d*(a*p-f*h)),e[5]=n*(h*g-p*m)-l*(i*g-s*m)+d*(i*p-s*h),e[6]=-(n*(a*g-f*m)-o*(i*g-s*m)+d*(i*f-s*a)),e[7]=n*(a*p-f*h)-o*(i*p-s*h)+l*(i*f-s*a),e[8]=o*(c*g-p*v)-l*(u*g-f*v)+d*(u*p-f*c),e[9]=-(n*(c*g-p*v)-l*(r*g-s*v)+d*(r*p-s*c)),e[10]=n*(u*g-f*v)-o*(r*g-s*v)+d*(r*f-s*u),e[11]=-(n*(u*p-f*c)-o*(r*p-s*c)+l*(r*f-s*u)),e[12]=-(o*(c*m-h*v)-l*(u*m-a*v)+d*(u*h-a*c)),e[13]=n*(c*m-h*v)-l*(r*m-i*v)+d*(r*h-i*c),e[14]=-(n*(u*m-a*v)-o*(r*m-i*v)+d*(r*a-i*u)),e[15]=n*(u*h-a*c)-o*(r*h-i*c)+l*(r*a-i*u),e},c.determinant=function(e){var t=e[0],n=e[1],r=e[2],i=e[3],s=e[4],o=e[5],u=e[6],a=e[7],f=e[8],l=e[9],c=e[10],h=e[11],p=e[12],d=e[13],v=e[14],m=e[15],g=t*o-n*s,y=t*u-r*s,b=t*a-i*s,w=n*u-r*o,E=n*a-i*o,S=r*a-i*u,x=f*d-l*p,T=f*v-c*p,N=f*m-h*p,C=l*v-c*d,k=l*m-h*d,L=c*m-h*v;return g*L-y*k+b*C+w*N-E*T+S*x},c.multiply=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=t[3],u=t[4],a=t[5],f=t[6],l=t[7],c=t[8],h=t[9],p=t[10],d=t[11],v=t[12],m=t[13],g=t[14],y=t[15],b=n[0],w=n[1],E=n[2],S=n[3];return e[0]=b*r+w*u+E*c+S*v,e[1]=b*i+w*a+E*h+S*m,e[2]=b*s+w*f+E*p+S*g,e[3]=b*o+w*l+E*d+S*y,b=n[4],w=n[5],E=n[6],S=n[7],e[4]=b*r+w*u+E*c+S*v,e[5]=b*i+w*a+E*h+S*m,e[6]=b*s+w*f+E*p+S*g,e[7]=b*o+w*l+E*d+S*y,b=n[8],w=n[9],E=n[10],S=n[11],e[8]=b*r+w*u+E*c+S*v,e[9]=b*i+w*a+E*h+S*m,e[10]=b*s+w*f+E*p+S*g,e[11]=b*o+w*l+E*d+S*y,b=n[12],w=n[13],E=n[14],S=n[15],e[12]=b*r+w*u+E*c+S*v,e[13]=b*i+w*a+E*h+S*m,e[14]=b*s+w*f+E*p+S*g,e[15]=b*o+w*l+E*d+S*y,e},c.mul=c.multiply,c.translate=function(e,t,n){var r=n[0],i=n[1],s=n[2],o,u,a,f,l,c,h,p,d,v,m,g;return t===e?(e[12]=t[0]*r+t[4]*i+t[8]*s+t[12],e[13]=t[1]*r+t[5]*i+t[9]*s+t[13],e[14]=t[2]*r+t[6]*i+t[10]*s+t[14],e[15]=t[3]*r+t[7]*i+t[11]*s+t[15]):(o=t[0],u=t[1],a=t[2],f=t[3],l=t[4],c=t[5],h=t[6],p=t[7],d=t[8],v=t[9],m=t[10],g=t[11],e[0]=o,e[1]=u,e[2]=a,e[3]=f,e[4]=l,e[5]=c,e[6]=h,e[7]=p,e[8]=d,e[9]=v,e[10]=m,e[11]=g,e[12]=o*r+l*i+d*s+t[12],e[13]=u*r+c*i+v*s+t[13],e[14]=a*r+h*i+m*s+t[14],e[15]=f*r+p*i+g*s+t[15]),e},c.scale=function(e,t,n){var r=n[0],i=n[1],s=n[2];return e[0]=t[0]*r,e[1]=t[1]*r,e[2]=t[2]*r,e[3]=t[3]*r,e[4]=t[4]*i,e[5]=t[5]*i,e[6]=t[6]*i,e[7]=t[7]*i,e[8]=t[8]*s,e[9]=t[9]*s,e[10]=t[10]*s,e[11]=t[11]*s,e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15],e},c.rotate=function(e,n,r,i){var s=i[0],o=i[1],u=i[2],a=Math.sqrt(s*s+o*o+u*u),f,l,c,h,p,d,v,m,g,y,b,w,E,S,x,T,N,C,k,L,A,O,M,_;return Math.abs(a)<t?null:(a=1/a,s*=a,o*=a,u*=a,f=Math.sin(r),l=Math.cos(r),c=1-l,h=n[0],p=n[1],d=n[2],v=n[3],m=n[4],g=n[5],y=n[6],b=n[7],w=n[8],E=n[9],S=n[10],x=n[11],T=s*s*c+l,N=o*s*c+u*f,C=u*s*c-o*f,k=s*o*c-u*f,L=o*o*c+l,A=u*o*c+s*f,O=s*u*c+o*f,M=o*u*c-s*f,_=u*u*c+l,e[0]=h*T+m*N+w*C,e[1]=p*T+g*N+E*C,e[2]=d*T+y*N+S*C,e[3]=v*T+b*N+x*C,e[4]=h*k+m*L+w*A,e[5]=p*k+g*L+E*A,e[6]=d*k+y*L+S*A,e[7]=v*k+b*L+x*A,e[8]=h*O+m*M+w*_,e[9]=p*O+g*M+E*_,e[10]=d*O+y*M+S*_,e[11]=v*O+b*M+x*_,n!==e&&(e[12]=n[12],e[13]=n[13],e[14]=n[14],e[15]=n[15]),e)},c.rotateX=function(e,t,n){var r=Math.sin(n),i=Math.cos(n),s=t[4],o=t[5],u=t[6],a=t[7],f=t[8],l=t[9],c=t[10],h=t[11];return t!==e&&(e[0]=t[0],e[1]=t[1],e[2]=t[2],e[3]=t[3],e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15]),e[4]=s*i+f*r,e[5]=o*i+l*r,e[6]=u*i+c*r,e[7]=a*i+h*r,e[8]=f*i-s*r,e[9]=l*i-o*r,e[10]=c*i-u*r,e[11]=h*i-a*r,e},c.rotateY=function(e,t,n){var r=Math.sin(n),i=Math.cos(n),s=t[0],o=t[1],u=t[2],a=t[3],f=t[8],l=t[9],c=t[10],h=t[11];return t!==e&&(e[4]=t[4],e[5]=t[5],e[6]=t[6],e[7]=t[7],e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15]),e[0]=s*i-f*r,e[1]=o*i-l*r,e[2]=u*i-c*r,e[3]=a*i-h*r,e[8]=s*r+f*i,e[9]=o*r+l*i,e[10]=u*r+c*i,e[11]=a*r+h*i,e},c.rotateZ=function(e,t,n){var r=Math.sin(n),i=Math.cos(n),s=t[0],o=t[1],u=t[2],a=t[3],f=t[4],l=t[5],c=t[6],h=t[7];return t!==e&&(e[8]=t[8],e[9]=t[9],e[10]=t[10],e[11]=t[11],e[12]=t[12],e[13]=t[13],e[14]=t[14],e[15]=t[15]),e[0]=s*i+f*r,e[1]=o*i+l*r,e[2]=u*i+c*r,e[3]=a*i+h*r,e[4]=f*i-s*r,e[5]=l*i-o*r,e[6]=c*i-u*r,e[7]=h*i-a*r,e},c.fromRotationTranslation=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=t[3],u=r+r,a=i+i,f=s+s,l=r*u,c=r*a,h=r*f,p=i*a,d=i*f,v=s*f,m=o*u,g=o*a,y=o*f;return e[0]=1-(p+v),e[1]=c+y,e[2]=h-g,e[3]=0,e[4]=c-y,e[5]=1-(l+v),e[6]=d+m,e[7]=0,e[8]=h+g,e[9]=d-m,e[10]=1-(l+p),e[11]=0,e[12]=n[0],e[13]=n[1],e[14]=n[2],e[15]=1,e},c.fromQuat=function(e,t){var n=t[0],r=t[1],i=t[2],s=t[3],o=n+n,u=r+r,a=i+i,f=n*o,l=n*u,c=n*a,h=r*u,p=r*a,d=i*a,v=s*o,m=s*u,g=s*a;return e[0]=1-(h+d),e[1]=l+g,e[2]=c-m,e[3]=0,e[4]=l-g,e[5]=1-(f+d),e[6]=p+v,e[7]=0,e[8]=c+m,e[9]=p-v,e[10]=1-(f+h),e[11]=0,e[12]=0,e[13]=0,e[14]=0,e[15]=1,e},c.frustum=function(e,t,n,r,i,s,o){var u=1/(n-t),a=1/(i-r),f=1/(s-o);return e[0]=s*2*u,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=s*2*a,e[6]=0,e[7]=0,e[8]=(n+t)*u,e[9]=(i+r)*a,e[10]=(o+s)*f,e[11]=-1,e[12]=0,e[13]=0,e[14]=o*s*2*f,e[15]=0,e},c.perspective=function(e,t,n,r,i){var s=1/Math.tan(t/2),o=1/(r-i);return e[0]=s/n,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=s,e[6]=0,e[7]=0,e[8]=0,e[9]=0,e[10]=(i+r)*o,e[11]=-1,e[12]=0,e[13]=0,e[14]=2*i*r*o,e[15]=0,e},c.ortho=function(e,t,n,r,i,s,o){var u=1/(t-n),a=1/(r-i),f=1/(s-o);return e[0]=-2*u,e[1]=0,e[2]=0,e[3]=0,e[4]=0,e[5]=-2*a,e[6]=0,e[7]=0,e[8]=0,e[9]=0,e[10]=2*f,e[11]=0,e[12]=(t+n)*u,e[13]=(i+r)*a,e[14]=(o+s)*f,e[15]=1,e},c.lookAt=function(e,n,r,i){var s,o,u,a,f,l,h,p,d,v,m=n[0],g=n[1],y=n[2],b=i[0],w=i[1],E=i[2],S=r[0],x=r[1],T=r[2];return Math.abs(m-S)<t&&Math.abs(g-x)<t&&Math.abs(y-T)<t?c.identity(e):(h=m-S,p=g-x,d=y-T,v=1/Math.sqrt(h*h+p*p+d*d),h*=v,p*=v,d*=v,s=w*d-E*p,o=E*h-b*d,u=b*p-w*h,v=Math.sqrt(s*s+o*o+u*u),v?(v=1/v,s*=v,o*=v,u*=v):(s=0,o=0,u=0),a=p*u-d*o,f=d*s-h*u,l=h*o-p*s,v=Math.sqrt(a*a+f*f+l*l),v?(v=1/v,a*=v,f*=v,l*=v):(a=0,f=0,l=0),e[0]=s,e[1]=a,e[2]=h,e[3]=0,e[4]=o,e[5]=f,e[6]=p,e[7]=0,e[8]=u,e[9]=l,e[10]=d,e[11]=0,e[12]=-(s*m+o*g+u*y),e[13]=-(a*m+f*g+l*y),e[14]=-(h*m+p*g+d*y),e[15]=1,e)},c.str=function(e){return"mat4("+e[0]+", "+e[1]+", "+e[2]+", "+e[3]+", "+e[4]+", "+e[5]+", "+e[6]+", "+e[7]+", "+e[8]+", "+e[9]+", "+e[10]+", "+e[11]+", "+e[12]+", "+e[13]+", "+e[14]+", "+e[15]+")"},typeof e!="undefined"&&(e.mat4=c);var h={};h.create=function(){var e=new n(4);return e[0]=0,e[1]=0,e[2]=0,e[3]=1,e},h.rotationTo=function(){var e=o.create(),t=o.fromValues(1,0,0),n=o.fromValues(0,1,0);return function(r,i,s){var u=o.dot(i,s);return u<-0.999999?(o.cross(e,t,i),o.length(e)<1e-6&&o.cross(e,n,i),o.normalize(e,e),h.setAxisAngle(r,e,Math.PI),r):u>.999999?(r[0]=0,r[1]=0,r[2]=0,r[3]=1,r):(o.cross(e,i,s),r[0]=e[0],r[1]=e[1],r[2]=e[2],r[3]=1+u,h.normalize(r,r))}}(),h.setAxes=function(){var e=l.create();return function(t,n,r,i){return e[0]=r[0],e[3]=r[1],e[6]=r[2],e[1]=i[0],e[4]=i[1],e[7]=i[2],e[2]=n[0],e[5]=n[1],e[8]=n[2],h.normalize(t,h.fromMat3(t,e))}}(),h.clone=u.clone,h.fromValues=u.fromValues,h.copy=u.copy,h.set=u.set,h.identity=function(e){return e[0]=0,e[1]=0,e[2]=0,e[3]=1,e},h.setAxisAngle=function(e,t,n){n*=.5;var r=Math.sin(n);return e[0]=r*t[0],e[1]=r*t[1],e[2]=r*t[2],e[3]=Math.cos(n),e},h.add=u.add,h.multiply=function(e,t,n){var r=t[0],i=t[1],s=t[2],o=t[3],u=n[0],a=n[1],f=n[2],l=n[3];return e[0]=r*l+o*u+i*f-s*a,e[1]=i*l+o*a+s*u-r*f,e[2]=s*l+o*f+r*a-i*u,e[3]=o*l-r*u-i*a-s*f,e},h.mul=h.multiply,h.scale=u.scale,h.rotateX=function(e,t,n){n*=.5;var r=t[0],i=t[1],s=t[2],o=t[3],u=Math.sin(n),a=Math.cos(n);return e[0]=r*a+o*u,e[1]=i*a+s*u,e[2]=s*a-i*u,e[3]=o*a-r*u,e},h.rotateY=function(e,t,n){n*=.5;var r=t[0],i=t[1],s=t[2],o=t[3],u=Math.sin(n),a=Math.cos(n);return e[0]=r*a-s*u,e[1]=i*a+o*u,e[2]=s*a+r*u,e[3]=o*a-i*u,e},h.rotateZ=function(e,t,n){n*=.5;var r=t[0],i=t[1],s=t[2],o=t[3],u=Math.sin(n),a=Math.cos(n);return e[0]=r*a+i*u,e[1]=i*a-r*u,e[2]=s*a+o*u,e[3]=o*a-s*u,e},h.calculateW=function(e,t){var n=t[0],r=t[1],i=t[2];return e[0]=n,e[1]=r,e[2]=i,e[3]=-Math.sqrt(Math.abs(1-n*n-r*r-i*i)),e},h.dot=u.dot,h.lerp=u.lerp,h.slerp=function(e,t,n,r){var i=t[0],s=t[1],o=t[2],u=t[3],a=n[0],f=n[1],l=n[2],c=n[3],h,p,d,v,m;return p=i*a+s*f+o*l+u*c,p<0&&(p=-p,a=-a,f=-f,l=-l,c=-c),1-p>1e-6?(h=Math.acos(p),d=Math.sin(h),v=Math.sin((1-r)*h)/d,m=Math.sin(r*h)/d):(v=1-r,m=r),e[0]=v*i+m*a,e[1]=v*s+m*f,e[2]=v*o+m*l,e[3]=v*u+m*c,e},h.invert=function(e,t){var n=t[0],r=t[1],i=t[2],s=t[3],o=n*n+r*r+i*i+s*s,u=o?1/o:0;return e[0]=-n*u,e[1]=-r*u,e[2]=-i*u,e[3]=s*u,e},h.conjugate=function(e,t){return e[0]=-t[0],e[1]=-t[1],e[2]=-t[2],e[3]=t[3],e},h.length=u.length,h.len=h.length,h.squaredLength=u.squaredLength,h.sqrLen=h.squaredLength,h.normalize=u.normalize,h.fromMat3=function(){var e=typeof Int8Array!="undefined"?new Int8Array([1,2,0]):[1,2,0];return function(t,n){var r=n[0]+n[4]+n[8],i;if(r>0)i=Math.sqrt(r+1),t[3]=.5*i,i=.5/i,t[0]=(n[7]-n[5])*i,t[1]=(n[2]-n[6])*i,t[2]=(n[3]-n[1])*i;else{var s=0;n[4]>n[0]&&(s=1),n[8]>n[s*3+s]&&(s=2);var o=e[s],u=e[o];i=Math.sqrt(n[s*3+s]-n[o*3+o]-n[u*3+u]+1),t[s]=.5*i,i=.5/i,t[3]=(n[u*3+o]-n[o*3+u])*i,t[o]=(n[o*3+s]+n[s*3+o])*i,t[u]=(n[u*3+s]+n[s*3+u])*i}return t}}(),h.str=function(e){return"quat("+e[0]+", "+e[1]+", "+e[2]+", "+e[3]+")"},typeof e!="undefined"&&(e.quat=h)}(t.exports)})(this);

/**
 * Bounding volumes stores different types of volumes used to encapsulate objects.
 *
 * @module Bounding Volumes
 */
define('Math/AABBox',["libs/gl-matrix"], function(glm)
{
    /** 
     * AABBox is an axially aligned bounding box.
     * @constructor
     * @class AABBox
     */
    AABBox = function()
    {
        /**
         * Minimum point of an AABBox.
         * @property min
         * @type vec3
         */
        this.min = glm.vec3.fromValues(Number.MAX_VALUE, Number.MAX_VALUE, Number.MAX_VALUE);

        /**
         * Maximum point of an AABBox.
         * @property max
         * @type vec3
         */
        this.max = glm.vec3.fromValues(-Number.MAX_VALUE, -Number.MAX_VALUE, -Number.MAX_VALUE);
    }

    /**
     * Clones the AABBox.
     * @method clone
     * @return {AABBox} Returns a new instance of a AABBox.
     */
    AABBox.prototype.clone = function()
    {
        var aabb = new AABBox;
        aabb.min = glm.vec3.clone(this.min);
        aabb.max = glm.vec3.clone(this.max);
        return aabb;
    };

    /**
     * Returns one of the corner points.
     *
     *        .6------7
     *      .' |    .'|
     *     2---+--3'  |
     *     |   |  |   |
     *     |  ,4--+---5
     *     |.'    | .' 
     *     0------1'
     * 
     * @method getCornerPoint
     * @param {Number} index The index of the corner point.
     * @return {vec3} The corner point. 
     */
    AABBox.prototype.getCornerPoint = function(index)
    {
        return glm.vec3.fromValues((index & 1) ? this.max[0] : this.min[0],
                                   (index & 2) ? this.max[1] : this.min[1],
                                   (index & 3) ? this.min[2] : this.max[2]);
    };

    /**
     * Compute an outcode for an AABBox in clipspace.
     * 
     * An outcode is a flag variable, 
     * which specifices which of the clip planes 
     * that the AABBox is outside of.
     *
     * The data is stored in each bit as following shows:
     *
     *     | Clip plane = Bit |
     *     |    Left    =  0  |
     *     |    Right   =  1  |
     *     |    Bottom  =  2  |
     *     |    Top     =  3  |
     *     |    Near    =  4  |
     *     |    Far     =  5  |
     *
     * @method computeOutCode
     * @param {mat4} clipMat The clip matrix to use.
     */
    AABBox.prototype.computeOutCode = function(clipMat)
    {
        var offScreen = true;
        for(var i = 0;i < 8;i++)
        {
            var point = this.getCornerPoint(i);
            offScreen = offScreen && glm.vec3.computeOutCode(point, clipMat);
        }

        return offScreen;
    };

    /**
     * Checks if the AABBox is inside the frustum, 
     * defined by a set of six planes.
     * @param {Plane} planes An array of planes.
     * @return {Bool} Returns true if the AABBox is inside the frustum.
     */
    AABBox.prototype.isInFrustum = function(planes)
    {
        for(var i = 0;i < 6;i++)
        {
            var vertices = this.computeVertexNP(planes[i].normal);
            if(planes[i].distance(vertices.p) < 0)
                return false;
        }

        return true;
    };

    /**
     * Returns the center of the AABBox.
     * @method center
     * @return {vec3} The center of the AABBox.
     */
    AABBox.prototype.center = function()
    {
        var minMax = glm.vec3.add(glm.vec3.create(), this.min, this.max);
        return glm.vec3.scale(minMax, minMax, 0.5);
    };

    /**
     * Computes the positive and negative vertex projected on the normal.
     * Used for faster frustum check.
     * @method computeVertexNP
     * @param {vec3} normal The normal
     * @return {vec3 p, vec3 n} Returns the positive vertex and negative vertex.
     */
    AABBox.prototype.computeVertexNP = function(normal)
    {
        var p = glm.vec3.clone(this.min);
        var n = glm.vec3.clone(this.max);
        if(normal[0] >= 0)
        {
            p[0] = this.max[0];
            n[0] = this.min[0];
        }
        if(normal[1] >= 0)
        {
            p[1] = this.max[1];
            n[1] = this.min[1];
        }
        if(normal[2] >= 0)
        {
            p[2] = this.max[2];
            n[2] = this.min[2];
        }

        return {p: p, n: n};
    };

    /**
     * Set the AABBox to a transformed box.
     * @method setToTransformedBox
     * @param {AABBox} box The box that becomes transformed.
     * @param {mat4} m The matrix which transforms the given box.
     */
    AABBox.prototype.setToTransformedBox = function(box, m)
    {
        if(this == box)
        {
            box = this.clone();
        }

        // First take the translation part.
        this.min[0] = this.max[0] = m[12];
        this.min[1] = this.max[1] = m[13];
        this.min[2] = this.max[2] = m[14];

        if(m[0] > 0)
        {
            this.min[0] += m[0] * box.min[0]; this.max[0] += m[0] * box.max[0];
        }
        else
        {
            this.min[0] += m[0] * box.max[0]; this.max[0] += m[0] * box.min[0];
        }

        if(m[1] > 0)
        {
            this.min[0] += m[1] * box.min[0]; this.max[0] += m[1] * box.max[0];
        }
        else
        {
            this.min[0] += m[1] * box.max[0]; this.max[0] += m[1] * box.min[0];
        }

        if(m[2] > 0)
        {
            this.min[0] += m[2] * box.min[0]; this.max[0] += m[2] * box.max[0];
        }
        else
        {
            this.min[0] += m[2] * box.max[0]; this.max[0] += m[2] * box.min[0];
        }

        if(m[4] > 0)
        {
            this.min[1] += m[4] * box.min[1]; this.max[1] += m[4] * box.max[1];
        }
        else
        {
            this.min[1] += m[4] * box.max[1]; this.max[1] += m[4] * box.min[1];
        }

        if(m[5] > 0)
        {
            this.min[1] += m[5] * box.min[1]; this.max[1] += m[5] * box.max[1];
        }
        else
        {
            this.min[1] += m[5] * box.max[1]; this.max[1] += m[5] * box.min[1];
        }

        if(m[6] > 0)
        {
            this.min[1] += m[6] * box.min[1]; this.max[1] += m[6] * box.max[1];
        }
        else
        {
            this.min[1] += m[6] * box.max[1]; this.max[1] += m[6] * box.min[1];
        }

        if(m[8] > 0)
        {
            this.min[2] += m[8] * box.min[2]; this.max[2] += m[8] * box.max[2];
        }
        else
        {
            this.min[2] += m[8] * box.max[2]; this.max[2] += m[8] * box.min[2];
        }

        if(m[9] > 0)
        {
            this.min[2] += m[9] * box.min[2]; this.max[2] += m[9] * box.max[2];
        }
        else
        {
            this.min[2] += m[9] * box.max[2]; this.max[2] += m[9] * box.min[2];
        }

        if(m[10] > 0)
        {
            this.min[2] += m[10] * box.min[2]; this.max[2] += m[10] * box.max[2];
        }
        else
        {
            this.min[2] += m[10] * box.max[2]; this.max[2] += m[10] * box.min[2];
        }
    };

    /**
     * Make the AABBox encapsulate a set of points.
     * @method addPoints
     * @param {Array.Number} points The points represented as 3 x numbers for each point.
     */
    AABBox.prototype.addPoints = function(points) 
    {
        for(var i = 2;i < points.length;i += 3)
        {
            var pointX = points[i - 2];
            var pointY = points[i - 1];
            var pointZ = points[i];

            if(pointX < this.min[0]) 
                this.min[0] = pointX;
            if(pointX > this.max[0]) 
                this.max[0] = pointX; 

            if(pointY < this.min[1]) 
                this.min[1] = pointY;
            if(pointY > this.max[1])
                this.max[1] = pointY;

            if(pointZ < this.min[2]) 
                this.min[2] = pointZ;
            if(pointZ > this.max[2])
                this.max[2] = pointZ;
        }
    };

    /**
     * Returns the size of the AABBox.
     * @method getSize
     * @return {vec3} Size of AABBox.
     */
    AABBox.prototype.getSize = function()
    {
        return glm.vec3.sub(glm.vec3.create(), this.max, this.min);
    }

    /**
     * Returns the volume of the AABBox.
     * @method getVolume
     * @return {Number} Volume of AABBox.
     */
    AABBox.prototype.getVolume = function()
    {
        var size = this.getSize();
        return size[0] * size[1] * size[2];
    };

    /**
     * Returns the amount of growth needed to encapsulate an other AABB.
     * @method getGrowth
     * @param {AABBox} a The box to check.
     * @return {Number} The amount of growth needed, represented as volume.
     */
    AABBox.prototype.getGrowth = function(a)
    {
        return AABBox.createFromAABBox(this, a).getVolume();
    };

    /**
     * Check if two AABBs intersect, and return true if so.
     * @static
     * @method intersectAABB
     * @param {AABBox} a One of the AABBox to check for intersection.
     * @param {AABBox} b One of the AABBox to check for intersection.
     * @return {Bool} True if the boxes intersect, else false.
     */
    AABBox.intersectAABB = function(a, b)
    {
        if(a.min[0] > b.max[0]) return false;
        if(a.max[0] < b.min[0]) return false;
        if(a.min[1] > b.max[1]) return false;
        if(a.max[1] < b.min[1]) return false;
        if(a.min[2] > b.max[2]) return false;
        if(a.max[2] < b.min[2]) return false;

        return true;
    };

    /**
     * Checks if a point is inside the bounding box.
     * @method isPointInside
     * @param {vec3} p The point to check.
     * @return True 
     */
    AABBox.prototype.isPointInside = function(p)
    {
        return (p[0] >= this.min[0]) && (p[0] <= this.max[0]) &&
               (p[1] >= this.min[1]) && (p[1] <= this.max[1]) &&
               (p[2] >= this.min[2]) && (p[2] <= this.max[2]);  
    };

    /**
     * Create an new AABBox from two AABBoxes.
     * @static
     * @method createFromAABBox
     * @param {AABBox} a One of the AABBox to use in creation.
     * @param {AABBox} b One of the AABBox to use in creation.
     */
    AABBox.createFromAABBox = function(a, b)
    {
        var result = new AABBox;

        result.addPoints(a.min);
        result.addPoints(a.max);
        result.addPoints(b.min);
        result.addPoints(b.max);

        return result;
    }

    return AABBox;
});
/**
 * A series of Game Object modules.
 * @module Modules
 */
define('Modules/Mesh',["Math/AABBox"], function(AABBox)
{
	/**
	 * Mesh is a GameObject module, which stores 3D data, used during rendering.
	 * It stores the data on the GPU.
	 * @class Mesh
	 * @constructor
	 */
	Mesh = function()
	{
		/**
		 * Name of the module
		 * 
		 * @property name
		 * @type String
		 */
		this.name = "Mesh";

		/**
		 * Vertex buffer object for the vertices, normals and uv-coordinates. 
		 *
		 * @private
		 * @property _vbo
		 */
		this._vbo = null;

		/** 
		 * Index buffer object used for triangle indices.
		 * @private
		 * @property _ibo
		 */
		this._ibo = null;

		this._numIndices = 0;

		this._indexDataType = 0;

		this._uvCoordsOffset = 0; 

		/**
		 * The render mode, which may be any GL render mode supported.
		 *
		 * @property renderMode
		 * @type GLenum
		 * @default	gl.TRIANGLES
		 */
		this.renderMode = gl.TRIANGLES;

		/**
		 * Stores the bounding volume for the mesh.
		 * @property boundingVolume
		 * @type AABBox
		 */
		this.boundingVolume = new AABBox;

		/**
		 * Number of vertices for this mesh.
		 * NOTE: READ ONLY 
		 *
		 * @property numVertices
		 * @type Int
		 * @default 0 
		 */
		this.numVertices = 0;

		/**
		 * True if the mesh have normals.
		 *
		 * @property hasNormals
		 * @type Bool
		 * @default	false
		 */
		this.hasNormals = false;

		/**
		 * True if the mesh have uv-coordinates.
		 *
		 * @property hasUVCoords
		 * @type Bool
		 * @default	false
		 */
		this.hasUVCoords = false;
	}

	/**
	 * Sets the vertices of the mesh, including normals and texture coords, if you want.
	 * 
	 * @method setVertices
	 * @param {vec3} vertices An array of vec3 for each vertex.
	 * @param {vec3} normals The normals of the mesh.
	 * @param {vec2} uvCoords The uv coordinates of the mesh.
	 */
	Mesh.prototype.setVertices = function(vertices, normals, uvCoords) 
	{
		// Create bounding volume.
		this.boundingVolume = new AABBox;
		this.boundingVolume.addPoints(vertices);

		if(this._vbo)
		{
			// Be sure to delete the old _vbo
			gl.deleteBuffer(this._vbo);
		}

		this._vbo = gl.createBuffer();

		var bufferSize = vertices.length;
		if(normals)
		{
			bufferSize += normals.length;
		}
		if(uvCoords)
		{
			bufferSize += uvCoords.length;
		}

		this.numVertices = vertices.length / 3;
		this._uvCoordsOffset = vertices.length;

		var buffer = new Float32Array(bufferSize);
		buffer.set(vertices);

		if(normals)
		{
			buffer.set(normals, vertices.length);
			this.hasNormals = true;
			this._uvCoordsOffset += normals.length;
		}

		if(uvCoords)
		{
			buffer.set(uvCoords, this._uvCoordsOffset);
			this.hasUVCoords = true;
		}

		gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo);
		gl.bufferData(gl.ARRAY_BUFFER, buffer, gl.STATIC_DRAW);
	};

	/**
	 * Set the indices of the mesh.
	 *
	 * @method setIndices
	 * @param {int} indices The indices of the mesh.
	 */
	Mesh.prototype.setIndices = function(indices)
	{
		if(this._ibo)
		{
			gl.deleteBuffer(this._ibo);
		}
		
		this._ibo = gl.createBuffer();
		this._numIndices = indices.length;

		// Find the most suitable data type for the indices.
		var buffer;
		if(this.numVertices > 0xFFFF)
		{
			this._indexDataType = gl.UNSIGNED_INT;
			buffer = new Uint32Array(indices);
		}
		else if(this.numVertices > 0xFF)
		{
			this._indexDataType = gl.UNSIGNED_SHORT;
			buffer = new Uint16Array(indices);
		}
		else
		{
			this._indexDataType = gl.UNSIGNED_BYTE;
			buffer = new Uint8Array(indices);
		}

		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._ibo);
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, buffer, gl.STATIC_DRAW);
	};

	/**
	 * Render the mesh.
	 * @method render
	 */
	Mesh.prototype.render = function()
	{
		if(this._ibo)
		{
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._ibo);
		}

		gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo);

		gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(0);

		gl.vertexAttribPointer(1, 3, gl.FLOAT, false, 0, this.numVertices * 3 * 4);
        gl.enableVertexAttribArray(1);

		gl.vertexAttribPointer(2, 2, gl.FLOAT, false, 0, this._uvCoordsOffset * 4);
        gl.enableVertexAttribArray(2);

        if(!this.hasNormals)
        {
        	gl.disableVertexAttribArray(1);
        }

        if(!this.hasUVCoords)
        {
			gl.disableVertexAttribArray(2);
        }

        if(this._ibo)
        {
        	gl.drawElements(this.renderMode, this._numIndices, this._indexDataType, 0);
        }
        else
        {
        	gl.drawArrays(this.renderMode, 0, this.numVertices);
        }
	};

	return Mesh;
});
/**
 * A series of different types of meshes, and utilities for that.
 *
 * @module Mesh
 */
define('Mesh/CubeMesh',["Modules/Mesh"], function(Mesh)
{
    var vertices = [
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
    
        1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0,
        1.0, 1.0, -1.0,
        -1.0, 1.0, -1.0,

        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,

        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, -1.0,

        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        -1.0, 1.0, -1.0,
        1.0, 1.0, -1.0,

        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0
    ];

    var normals = [
        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, -1.0,
        0.0, 0.0, -1.0,
        0.0, 0.0, -1.0,
        0.0, 0.0, -1.0,

        -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,

        1.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        1.0, 0.0, 0.0,

        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,

        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0
    ];

    var texCoords = [
        0, 0,
        1, 0,
        0, 1,
        1, 1,

        0, 0,
        1, 0,
        0, 1,
        1, 1,

        0, 0,
        1, 0,
        0, 1,
        1, 1,

        0, 0,
        1, 0,
        0, 1,
        1, 1,

        0, 0,
        1, 0,
        0, 1,
        1, 1,

        0, 0,
        1, 0,
        0, 1,
        1, 1
    ];

    var indices = [
        0, 1 ,2,
        1, 3, 2,

        4, 5, 6,
        5, 7, 6,

        8, 9, 10,
        9, 11, 10,

        12, 13, 14,
        13, 15, 14,

        16, 17, 18,
        17, 19, 18,

        20, 21, 22,
        21, 23, 22
    ];

    /**
     * Predefined mesh that represents a cube.
     * @static
     * @class CubeMesh
     * @constructor
     * @return {Mesh} Returns the mesh.
     */
    CubeMesh = function()
    {
        if(!CubeMesh.staticMesh)
        {
            CubeMesh.staticMesh = new Mesh;
            CubeMesh.staticMesh.setVertices(vertices, normals, texCoords);
            CubeMesh.staticMesh.setIndices(indices);
        }
        return CubeMesh.staticMesh;
    }

    CubeMesh.staticMesh = null;

    /**
     * Returns the vertices of a cube mesh.
     * @method getVertices
     * @return {Array.Number} Returns the vertices.
     */
    CubeMesh.getVertices = function()
    {
        return vertices;
    };

    return CubeMesh;
});
/**
 * A series of Scene related classes.
 * @module Scene
 */
define('Scene/GameObject',[],function()
{
	/**
 	 * A GameObject is an empty object that can be customized by adding GameObject modules.
 	 * The GameObject is used to represent something in your game.
 	 *
 	 * @class GameObject
 	 * @constructor
 	 * @param {String} name The name of the GameObject.
 	 */
	GameObject = function(name)
	{
		/**
		 * Name of GameObject instance.
		 *
		 * @property name
		 * @type String
		 */
		this.name = name;

		/**
		 * True if the game object should NOT be rendered.
		 *
		 * NOTE: The Game Object may only be rendered if it can.
		 *
		 * @property isHidden
		 * @type Bool
		 * @default false
		 */
		this.isHidden = false;

		/**
		 * Stores the modules.
		 *
		 * @private
		 * @property _modules
		 */
		this._modules = new Object();

		/**
		 * A Script is a special case of a GameObject module. 
		 * So we store a pointer to the scripts, so that we 
		 * can easily call the scripts.
		 *
		 * @private
		 * @property _scripts
		 */
		this._scripts = new Object();

		/**
		 * A readonly property, that is true if the object is visible this frame.
		 * @property isVisible
		 * @type Bool
		 * @default false
		 */
		this.isVisible = false;

		/** 
		 * Stores events that the modules can send or subscribe for.
		 * @private
		 * @property _events 
		 */
		this._events = new Object();
		this._events.subscribers = new Array();
	}

	/**
	 * Adds a module to the current GameObject.
	 *
	 * @method addModule
	 * @param {Module} mod The module, which becomes part of the object.
	 * @return {Module} Returns the added module.
	 */
	GameObject.prototype.addModule = function(mod) 
	{
		this._modules[mod.name] = mod;
		mod.gameObject = this;

		if(mod.start)
		{
			mod.start();
		}

		if(mod.isScript)
		{
			this._scripts[mod.name] = mod;
		}

		return mod;
	};

	/**
	 * Sends an event to all the subscribers.
	 * @method addEvent
	 * @param {String} name The name of the event.
	 * @param {Module} fromModule The module that the event is sent from.
	 * @parm {Object} extraData Optional extra data, sent with the event.
	 */
	GameObject.prototype.sendEvent = function(name, fromModule, extraData)
	{
		for(var i = 0;i < this._events[name].subscribers.length;i++)
		{
			var subscriber = this._events[name].subscribers[i];
			subscriber.callback.call(subscriber.module, fromModule, extraData);
		}
	};

	/**
	 * Subscribes a module to a specific event.
	 * @method subscribeEvent
	 * @param {String} eventName Name of event.
	 * @param {Module} module The module that subscribes to the event.
	 * @param {Function} callback The function called when the event is received.
	 */
	GameObject.prototype.subscribeEvent = function(eventName, module, callback)
	{
		this._events[eventName].subscribers.push({module: module, callback: callback});
	};

	/**
	 * Updates the GameObject.
	 *
	 * This function is called from the scene.
	 * @method update
	 */
	GameObject.prototype.update = function()
	{
		for(var key in this._scripts)
		{
			this._scripts[key].update();
		}

		for(var key in this._modules)
		{
			if(this._modules[key].update)
			{
				this._modules[key].update();
			}
		}
	}

	/**
	 * Returns a module from the current GameObject.
	 *
	 * @method getModule
	 * @param {String} modName The module name.
	 * @return {Module} The module if we found one, else null.
	 */
	GameObject.prototype.getModule = function(modName) 
	{
		return this._modules[modName];
	};

	/**
	 * Removes a module from the current GameObject.
	 *
	 * @method removeModule
	 * @param {String} modName The module name of the module, which gonna be removed.
	 */
	GameObject.prototype.removeModule = function(modName)
	{
		if(this._modules[objName].isScript)
		{
			this._modules[objName].exit();
		}

        // We don't need to check if modName exist,
        // since we let delete do the work.
        delete this._modules[objName];
	};

	return GameObject;
});
define('DebugDraw',["libs/gl-matrix", "Mesh/CubeMesh"], function(glm, cubeMesh)
{
	/**
	 * @class DebugDraw
	 * @static
	 */
	DebugDraw = {};

    /**
     * Draws a cube. You can set the rendering mode.
     * @method drawCube
     * @param {Scene} scene The scene to draw to.
     * @param {gl.TRIANGLES, gl.LINES, gl.POINTS} type The rendering mode.
     * @param {vec3} scale Scale of cube.
     * @param {quat} orient Orientation of cube.
     * @param {vec3} position Position of cube.
     * @param {vec4} color The color of the cube.
     */
    DebugDraw.drawCube = function(scene, type, scale, orient, position, color)
    {
		var scaleMat = glm.mat4.create(); 
		scaleMat[0] = scale[0];
		scaleMat[5] = scale[1];
		scaleMat[10] = scale[2];

    	var modelMat = glm.mat4.fromRotationTranslation(glm.mat4.create(), orient, position);
    	glm.mat4.mul(modelMat, modelMat, scaleMat);

    	var mesh = new cubeMesh;
    	scene.addDebugObject({mesh: mesh, modelMatrix: modelMat, color: color, type: type});
    };

    return DebugDraw;
});
/**
 * A series of Scene related classes.
 * @module Scene
 */
define('Scene/BVHNode',["libs/gl-matrix", "DebugDraw"], function(glm, DebugDraw)
{
	/**
	 * A base class for nodes in a bounding volume hierarchy.
	 * @constructor
 	 * @class BVHNode
 	 * @param {BVHNode} parentNode parentNode node. Optional
 	 * @param {AABBox} volume Bounding volume. Optional
 	 * @param {GameObject} gameObject The GameObject. Optional
	 */
	BVHNode = function(parentNode, volume, gameObject)
	{
		/**
		 * This node's GameObject.
		 * @property gameObject
		 * @type {GameObject}
		 */
		this.gameObject = gameObject;

		/**
		 * Stores the bounding volume for the node.
		 * @property boundingVolume
		 * @type {AABBox}
		 */
		this.boundingVolume = volume;

		/**
		 * Stores a reference to the parentNode node.
		 * @property parentNode
		 * @type {BVHNode}
		 */
		this.parentNode = parentNode;

		/**
		 * Stores the child nodes, which are two.
		 * @property children
		 * @type {BVHNode}
		 */
		this.children = new Array(2);
	}

	/**
	 * Destroys the BVHNode, and efficently removes it from the tree.
	 * It will also delete the children.
	 * @method destroyMe 
	 * @return {BVHNode} The sibling node that was moved.
	 */
	BVHNode.prototype.destroyMe = function()
	{
		if(this.parentNode)
		{
			// Find our sibling.
			var sibling;
			if(this.parentNode.children[0] == this)
			{
				sibling = this.parentNode.children[1];
			}
			else
			{
				sibling = this.parentNode.children[0];
			}

			this.parentNode.boundingVolume = sibling.boundingVolume;
			this.parentNode.gameObject = sibling.gameObject;
			this.parentNode.children[0] = sibling.children[0];
			this.parentNode.children[1] = sibling.children[1];

			if(sibling.children[0])
			{
				sibling.children[0].parentNode = this.parentNode;
				sibling.children[1].parentNode = this.parentNode;
			}

			this.parentNode.recalculateVolume();

			return this.parentNode;
		}

		this.parentNode = null;

		return null;
	}

	/**
	 * Searches the tree for objects that are in the frustum,
	 * and fills an array with the result.
	 * @method findObjectsInFrustum
	 * @param {Plane} planes An array of planes.
	 */
	BVHNode.prototype.findObjectsInFrustum = function(planes)
	{
		var isInFrustum = this.boundingVolume.isInFrustum(planes);

		if(isInFrustum)
		{
			if(!this.isLeaf())
			{
				this.children[0].findObjectsInFrustum(planes);
				this.children[1].findObjectsInFrustum(planes);
			}
			else
			{
				this.gameObject.isVisible = true;
			}
		}
	};

	/**
	 * Returns true if this node is at the bottom of the hierarchy.
	 * @method isLeaf
	 * @return {Bool} True if this node is counted as a leaf, else false.
	 */
	BVHNode.prototype.isLeaf = function() 
	{
		return this.gameObject != null;
	};

	/**
	 * Insert a GameObject into the tree, with the given bounding volume.
	 * @method insert
	 * @param {GameObject} gameObject The GameObject.
	 * @param {AABBox} volume Bounding volume.
	 * @return {BVHNode, BVHNode} Returns one of the node that have been moved and the new node.
	 */
	BVHNode.prototype.insert = function(gameObject, volume)
	{
		// If we are a leaf, then we would move the gameObject
		// into the list of children.
		if(this.isLeaf())
		{
			//Copy of current
			this.children[0] = new BVHNode(this, this.boundingVolume, this.gameObject);

			//New node
			this.children[1] = new BVHNode(this, volume, gameObject);

			this.gameObject = null;

			//Recalculate our bounding volume.
			this.recalculateVolume();

			return {oldNode: this.children[0], newNode: this.children[1]};
		}
		else
		{
			var childOne = this.children[0];
			var childTwo = this.children[1];

			// We put the node in the child that would grow the least.
			if(childOne.boundingVolume.getGrowth(volume) <
			   childTwo.boundingVolume.getGrowth(volume))
			{
				return childOne.insert(gameObject, volume);
			}
			else
			{
				return childTwo.insert(gameObject, volume);
			}
		}
	};

	/**
	 * Recalculate the bounding volume for this node.
	 * It would only work for non-leaf nodes.
	 * @method recalculateVolume
	 */
	BVHNode.prototype.recalculateVolume = function()
	{
		if(!this.isLeaf())
		{
			var childOne = this.children[0];
			var childTwo = this.children[1];
			this.boundingVolume = new AABBox.createFromAABBox(childOne.boundingVolume, 
															  childTwo.boundingVolume);
		}

		if(this.parentNode)
		{
			this.parentNode.recalculateVolume();
		}
	};

	/**
	 * Debug draw the node, and its children.
	 * @method debugDraw
	 * @param {Scene} scene The scene to draw to.
	 */ 
	BVHNode.prototype.debugDraw = function(scene, level)
	{
		if(!level)
		{
			level = 0;
		}

		DebugDraw.drawCube(scene, gl.TRIANGLES, glm.vec3.scale(glm.vec3.create(),
							this.boundingVolume.getSize(), 0.51),
						   glm.quat.create(), this.boundingVolume.center(), 
						   glm.vec4.fromValues(1 - level, level / 3, 1, 0.5));
		if(!this.isLeaf())
		{
			++level;
			this.children[0].debugDraw(scene, level);
			this.children[1].debugDraw(scene, level);
		}
	};

	return BVHNode;
});
define('Scene/BaseLight',["libs/gl-matrix"], function(glm, GameObject)
{	
	/**
	 * Base light stores basic information 
	 * that are needed for light calculations.
	 * @class BaseLight
	 */
	BaseLight = function()
	{
		/**
		 * The color of the light.
		 * @property color
		 * @type vec3
		 */
		this.color = glm.vec3.create();

		/**
		 * The ambient intensity of the light.
		 * @property ambientIntensity
		 * @type Number
		 */
		this.ambientIntensity = 0;

		/**
		 * The diffuse intensity of the light.
		 * @property diffuseIntensity
		 * @type Number
		 */
		this.diffuseIntensity = 0;

		/**
		 * Used to differentiate between the GameObjects type.
		 * @property isLight
		 * @type Bool
		 */
		this.isLight = true;
	}

	return BaseLight;
});
/**
 * Math module contains math related stuff.
 *
 * @module Math
 */

/**
 * Some extra functions for glMatrix.vec3.
 *
 * @class vec3
 * @static
 */
define('Math/vec3Ext',["libs/gl-matrix", "Math/MathExt"], function(glMatrix)
{
	/**
	 * Checks if two vectors are equal.
	 *
	 * It does a safe floating point check.
	 * @method isEqual
	 * @param {vec3} a Left side
	 * @param {vec3} b Right side
	 * @return {Bool} Returns true if the vectors are equal, else false. 
	 */
	glMatrix.vec3.isEqual = function(a, b)
	{
		return Math.isEqual(a[0], b[0]) && 
			   Math.isEqual(a[1], b[1]) &&
			   Math.isEqual(a[2], b[2]);
	}

	/**
	 * Computes an out code for a point.
	 * 
	 * An outcode is a flag variable, 
     * which specifices which of the clip planes 
     * that the point is outside of.
     *
     * The data is stored in each bit as following shows:
     *
     *     | Clip plane = Bit |
     *     |    Left    =  0  |
     *     |    Right   =  1  |
     *     |    Bottom  =  2  |
     *     |    Top     =  3  |
     *     |    Near    =  4  |
     *     |    Far     =  5  |
     *
     * @method computeOutCode
	 * @param {vec3} p The point in question.
	 * @param {mat4} m The clip matrix.
	 */
	glMatrix.vec3.computeOutCode = function(p, m)
	{
		var code = 0;
		var point = glMatrix.vec4.fromValues(p[0], p[1], p[2], 1.0);
		glMatrix.vec4.transformMat4(point, point, m);

        var w = point[3];
        if(point[0] < -w)
            code |= 0x01;
        if(point[0] > w)
            code |= 0x02;
        if(point[1] < -w)
            code |= 0x04;
        if(point[1] > w)
            code |= 0x08;
        if(point[2] < -w)
            code |= 0x10;
        if(point[2] > w)
            code |= 0x20;

        return code;
	};
});
/**
 * A series of Game Object modules.
 * @module Modules
 */
define('Modules/BoundingVolume',["Math/AABBox", "libs/gl-matrix", "Math/vec3Ext"], function(AABBox, glm)
{
	/**
	 * BoundingVolume is a module for GameObjects. 
	 * Bounding volume encapsulates an GameObject with a simple primitive.
	 * @class BoundingVolume
	 * @constructor
	 * @param {Bool} autoUpdate Set to true if you want the bounding volume 
	 * to update so that the GameObject always fit.
	 */
	BoundingVolume = function(autoUpdate)
	{
		/**
		 * Name of the module
		 * 
		 * @property name
		 * @type String
		 */
		this.name = "BoundingVolume";

		/**
		 * Stores the bounding volume data.
		 * @property boundingVolume 
		 */
		this.boundingVolume = null;

		/**
		 * If this property is true, the bounding volume would be linked with the
		 * GameObject, when added.
		 * @property autoUpdate
		 */
		this.autoUpdate = autoUpdate;

		/**
		 * True if the bounding volume have changed lastly.
		 * @private
		 * @property _isDirty
		 * @type Bool
		 */
		this._isDirty = false;
	}

	/**
	 * Start the function.
	 * @method start
	 */
	BoundingVolume.prototype.start = function()
	{
		var mesh = this.gameObject.getModule("Mesh");
		if(mesh)
		{
			this.boundingVolume = mesh.boundingVolume;
		}
	}

	/**
	 * Function called by the GameObject.
	 * @private
	 * @method update
	 */
	BoundingVolume.prototype.update = function()
	{
		if(this.autoUpdate)
		{
			var transformation = this.gameObject.getModule("Transformation");
			var mesh = this.gameObject.getModule("Mesh");
			if(mesh && transformation)
			{
				var oldVolume = null;
				if(this.boundingVolume)
				{
					oldVolume = this.boundingVolume.clone();
				}
				this.boundingVolume = new AABBox;
				this.boundingVolume.setToTransformedBox(mesh.boundingVolume, 
									transformation.getModelMatrix());

				if(oldVolume)
				{
					if( !glm.vec3.isEqual(oldVolume.min, this.boundingVolume.min) ||
						!glm.vec3.isEqual(oldVolume.max, this.boundingVolume.max))
					{
						this._isDirty = true;
					}
					else
					{
						this._isDirty = false;
					}
				}
			}
		}
	};

	/**
	 * Set the bounding volume, which would overwrite the existing one.
	 * If you call the constructor with the autoUpdate parameter as true,
	 * the bounding volume would automatically be generated for you.
	 * 
	 * The autoUpdate uses the Mesh and Transformation module.
	 * @method set
	 * @param {AABBox} volume The volume that this instance would use.
	 */
	BoundingVolume.prototype.set = function(volume) 
	{
		this.boundingVolume = volume;
	};

	return BoundingVolume;
});
define('Scene/PointLight',["libs/gl-matrix", "Scene/BaseLight", 
		"Modules/BoundingVolume", "Math/AABBox", "Scene/GameObject"], 
	function(glm, BaseLight, BoundingVolume, AABBox, GameObject)
{
	/**
	 * Point light is a type of light, that simulates
	 * lights that goes in all direction however with attenuation.
	 * 
	 * A light bulb is in this category.
	 * @class PointLight
	 * @constructor
	 * @extends BaseLight
	 * @extends GameObject
	 */
 	PointLight = function()
 	{
		this.name = "PointLight";

		/**
		 * The position of the point light.
		 * @property position
		 * @type vec3
		 */
		this.position = glm.vec3.create();

		/**
		 * Attenuation factors.
		 * @property attenuation
		 * @type {constant, linear, exp}
		 */
		this.attenuation = {constant: 0, linear: 0, exp: 0};

		/**
		 * The point light is dirty if one of the members have been changed.
		 * The user must set this, to update the bounding volume.
		 * @property isDirty
		 * @type Bool
		 */
		this.isDirty = false;

		/**
		 * Base light data.
		 * @property base
		 * @type BaseLight
		 */
		this.base = new BaseLight();
 	} 

	PointLight.prototype.constructor = PointLight;

	/**
	 * Updates the bounding volume for the light.
	 * @method update
	 */
	PointLight.prototype.update = function()
	{
		var volume = this.gameObject.getModule("BoundingVolume");
		if(volume && (this.isDirty || !volume.boundingVolume))
		{
			var radius = 2.0 * this.calculateRadius();
			var aabb = new AABBox;
			var pointA = glm.vec3.sub(glm.vec3.create(),
										     this.position, 
										     glm.vec3.fromValues(radius, radius, radius));
			var pointB = glm.vec3.add(glm.vec3.create(),
										     this.position, 
										     glm.vec3.fromValues(radius, radius, radius));
			aabb.addPoints(pointA);
			aabb.addPoints(pointB);
			volume.set(aabb);
			volume._isDirty = true;
		}
		else
		{
			volume._isDirty = false;
		}
	} 

	/**
	 * Calculates the radius of the point light,
	 * according to the parameters of the light.
	 * @method calculateRadius
	 * @return {Number} The radius of the point light.
	 */
	PointLight.prototype.calculateRadius = function() 
	{
		var maxChannel = Math.max(Math.max(this.base.color[0], this.base.color[1]), this.base.color[2]);
		return (-this.attenuation.linear + 
					Math.sqrt(this.attenuation.linear * this.attenuation.linear - 
					4 * this.attenuation.exp * (this.attenuation.exp - maxChannel * 
							                        this.base.diffuseIntensity))) /
					2 * this.attenuation.exp;
	};

	return PointLight;
});
define('Scene/LightManager',["Scene/PointLight", "libs/gl-matrix"], function(PointLight, glm)
{
    /**
     * LightManager manages your lights.
     * @class LightManager
     * @constructor
     */
    LightManager = function()
    {
        /**
         * Stores the point lights.
         * @private
         * @property _pointLights
         */
        this._pointLights = new Object;

        /**
         * The framebuffer object used 
         * for the off-screen rendering.
         * @private
         * @property _fbo
         */
        this._fbo = null;

        /**
         * Stores the textures.
         * @private
         * @property _textures
         */
        this._textures = new Array();

        /**
         * Our directional light.
         * @private
         * @property _dirLight
         * @type DirectLight
         */
        this._dirLight = null;

        /**
         * The light mode used for rendering.
         * Can either be LightModes.DEFERRED_RENDERING or LightModes.FORWARD_RENDERING
         * @property _lightMode
         */
        this._lightMode = LightManager.LightModes.FORWARD_RENDERING;

        this._tmpPointLights = new Array();
    }

    LightManager.LightModes = {DEFERRED_RENDERING: 1, FORWARD_RENDERING: 2};

    LightManager.NUM_TEXTURES = 4;

    LightManager.MAX_POINT_LIGHT_RENDERING = 5;

    /**
     * Adds a light to the light manager.
     *
     * NOTE: There can only be one direct light per scene!
     * @method addLight
     * @param {BaseLight} light Light to be added.
     */
    LightManager.prototype.addLight = function(light) 
    {
        var pointLightMod = light.getModule("PointLight");
        var dirLightMod = light.getModule("DirectLight");

        // If the light is a point light, add the point light to the list.
        if(pointLightMod)
        {
            this._pointLights[light.name] = light;
        }
        else if(dirLightMod)
        {
            this._dirLight = light;
        }
    };

    /**
     * Removes a light from the light manager.
     * @method removeLight
     * @param {String} name Name of light.
     */
    LightManager.prototype.removeLight = function(name)
    {
        delete this._pointLights[name];
    };

    /**
     * Setups what's needed to render the lights for the specific material
     * @method useOn
     * @param {Material} material The material that reflects/absorbs the lights.
     */
    LightManager.prototype.useOn = function(material)
    {
        if(this._tmpPointLights.length == 0)
        {
            for(var i in this._pointLights)
            {
                var light = this._pointLights[i];
                if(light.isVisible && 
                    this._tmpPointLights.length < LightManager.MAX_POINT_LIGHT_RENDERING)
                {
                    this._tmpPointLights.push(light);
                }
            }
        }

        material.setUniform("wgf_numPointLights", this._tmpPointLights.length,
                            Material.UNIFORM_TYPES.INT);
        for(var i = 0;i < this._tmpPointLights.length;i++)
        {
            var light = this._tmpPointLights[i];
            var lightModule = light.getModule("PointLight");
            var rootName = "wgf_pointLights[" + i + "].";
            material.setUniform(rootName + "position", lightModule.position);
            material.setUniform(rootName + "attenuation", 
            glm.vec3.fromValues(
                lightModule.attenuation.constant, 
                lightModule.attenuation.linear,
                lightModule.attenuation.exp));
            material.setUniform(rootName + "base.color", lightModule.base.color);
            material.setUniform(rootName + "base.intensity", 
                        glm.vec2.fromValues(lightModule.base.ambientIntensity, lightModule.base.diffuseIntensity));
        }

        if(this._dirLight)
        {
            var lightModule = this._dirLight.getModule("DirectLight");
            material.setUniform("wgf_dirLightBase.color", lightModule.base.color);
            material.setUniform("wgf_dirLightBase.intensity", 
                glm.vec2.fromValues(lightModule.base.ambientIntensity, lightModule.base.diffuseIntensity));
            material.setUniform("wgf_dirLightDir", lightModule.direction);
        }
    };

    /**
     * Resets information that were temporary used during rendering
     * for optimization.
     * @method reset
     */
    LightManager.prototype.reset = function()
    {
        this._tmpPointLights.length = 0;
    };

    /**
     * Initialize the light manager for rendering.
     * @method init
     * @param {Number} contextWidth Width of context.
     * @param {Number} contextHeight Height of context.
     * @param {LightModes} lightMode The type of lightning used for rendering. 
     */
    LightManager.prototype.init = function(contextWidth, contextHeight, lightMode)
    {
        if(lightMode != null)
        {
            this._lightMode = lightMode;
        }

        if(this._lightMode == LightManager.LightModes.DEFERRED_RENDERING)
        {
            console.log("Deferred rendering is under development!");
            this._fbo = gl.createFramebuffer();
            gl.bindFramebuffer(gl.FRAMEBUFFER, this._fbo);

            for(var i = 0;i < LightManager.NUM_TEXTURES;i++)
            {
                var texture = gl.createTexture();
                gl.bindTexture(gl.TEXTURE_2D, texture);
                //gl.texImage2D(gl.TEXTURE_2D, 0, )
            }

            gl.bindFramebuffer(gl.FRAMEBUFFER, 0);
        }
    };

    return LightManager;
});
/**
 * A series of Scene related classes.
 * @module Scene
 */
define('Scene/ShadowMap',[], function()
{

	/**
	 * Shadow map is used create shadows, using FBO and depth rendering.
	 * @class ShadowMap
	 * @constructor
	 */
	ShadowMap = function()
	{
		/**
		 * The depth texture that stores the depth information.
		 * @private
		 * @property _depthTexture
		 * @type Texture
		 */
		this._depthTexture = new Texture();

		/**
		 * Our frame buffer used of offscreen rendering.
		 * @private
		 * @property _frameBuffer
		 * @type gl.FRAMEBUFFER
		 */
		this._frameBuffer;
	};

	/**
	 * Initialize the shadow map, which creates a depth texture and framebuffer.
	 * @method init
	 * @param {Number} width The width of the shadow map, should be the canvas size.
	 * @param {Number} height The height of the shadow map, should be the canvas size.
	 * @return {Boolean} Returns true if no errors.
	 */
	ShadowMap.prototype.init = function(width, height) 
	{
		// Create the depth texture
		var texObj = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, texObj);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT, 
					  width, height, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_SHORT, null);
		this._depthTexture = texObj;

		// Create a framebuffer instance, and bind it to the depth texture.
		this._frameBuffer = gl.createFramebuffer();
		gl.bindFramebuffer(gl.FRAMEBUFFER, this._frameBuffer);
		this._frameBuffer.width = width;
		this._frameBuffer.height = height;
		gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, texObj, 0);

		var status = gl.checkFramebufferStatus(gl.FRAMEBUFFER);
		gl.bindFramebuffer(gl.FRAMEBUFFER, null);
		if(status != gl.FRAMEBUFFER_COMPLETE)
		{
			console.log("Failed to create a framebuffer!");
			return false;
		}

		return true;
	};

	/**
	 * Bind the shadow map for writing.
	 * @method bindForWriting
	 */
	ShadowMap.prototype.bindForWriting = function()
	{
		gl.bindFramebuffer(gl.FRAMEBUFFER, this._frameBuffer);
	};

	/**
	 * Bind the shadow map for reading.
	 * @method bindForReading
	 * @param {GLenum} textureUnit The texture unit to bind the shadow map to.
	 */
	ShadowMap.prototype.bindForReading = function(textureUnit)
	{
		gl.activeTexture(textureUnit);
		gl.bindTexture(gl.TEXTURE_2D, this._depthTexture);
	};

	return ShadowMap;
});
define('Mesh/PlaneMesh',["Modules/Mesh"], function(Mesh)
{
	/**
	 * Plane mesh, is used to draw planes.
	 * 
	 * It saves memory by using a shared Mesh class across all instances.
	 * So to change the size etc. Use the transformation class.
	 * @class PlaneMesh
	 * @constructor
	 */
	PlaneMesh = function()
	{
		if(!PlaneMesh.staticMesh)
		{
			PlaneMesh.staticMesh = new Mesh();
			PlaneMesh.staticMesh.setVertices(PlaneMesh.vertices, PlaneMesh.normals, PlaneMesh.texCoords);
			
			// We let the GC do the shitty work
			PlaneMesh.vertices = null;
			PlaneMesh.normals = null;
			PlaneMesh.texCoords = null;
		}

		return PlaneMesh.staticMesh;
	};

	PlaneMesh.staticMesh = null;

	PlaneMesh.vertices = [-1.0, -1.0, 0.0, 
						   1.0, -1.0, 0.0,
						   1.0, 1.0, 0.0,

						   1.0, 1.0, 0.0,
						   -1.0, 1.0, 0.0,
						   -1.0, -1.0, 0.0];

	PlaneMesh.normals = [0, 0, -1,
						 0, 0, -1,
						 0, 0, -1,

						 0, 0, -1,
						 0, 0, -1,
						 0, 0, -1];

	PlaneMesh.texCoords = [0, 0,
						   1, 0,
						   1, 1,

						   1, 1,
						   0, 1,
						   0, 0];
    return PlaneMesh;						   
});
/**
 * A series of Scene related classes.
 * @module Scene
 */
define('Scene/Scene',["libs/gl-matrix", "Utils", "Scene/BVHNode", 
        "Math/AABBox", "Scene/GameObject", "Scene/LightManager", "Scene/ShadowMap", "Mesh/PlaneMesh"], 
        function(glm, Utils, BVHNode, AABBox, GameObject, LightManager, ShadowMap, PlaneMesh)
{
    var debugVertShader = 
     "attribute vec3 position;" +
     "uniform mat4 wgf_mMat;" +
     "uniform mat4 wgf_vpMat;" +
     "void main()" + 
     "{" +
        "gl_Position = wgf_vpMat * wgf_mMat * vec4(position, 1.0);" +
        "gl_PointSize = 2.0;" + 
     "}";

    var debugFragShader =
     "precision highp float;" + 
     "uniform vec4 color;" +
     "void main()" +
     "{" +
        "gl_FragColor = color;" +
     "}";

    /**
     * Scene handles what's in game at the moment.
     *
     * The scene is the root class, used during rendering.
     * 
     * Add objects that you want to access into the current scene.
     * Including objects that you want to render.
     * 
     * TODO: Sort objects in the scene by their ShaderProgram
     * to optimize rendering.
     *
     * @class Scene
     * @constructor
     */ 
    Scene = function()
    {
        /**
         * Stores the objects that we have in the scene.
         *
         * @private
         * @property _objects
         */
        this._objects = new Object();

        /**
         * Points to the current camera.
         * @property currentCamera
         * @type Camera
         */
        this.currentCamera = null;

        /**
         * Stores the debug objects.
         * @private
         * @property _debugObjects
         * @type DebugDraw
         */
        this._debugObjects = new Array();

        /**
         * Stores the current visible objects.
         * @private
         * @property _visibleObjects
         * @type Array
         */
        this._visibleObjects = new Array();

        /**
         * Has the current root node of the BVH.
         * @property rootNode
         * @type BVHNode
         */
        this.rootNode = null;

        /**
         * Light manager used for the management of lights.
         * @property lightManager
         * @type LightManager
         */
        this.lightManager = new LightManager;

        /**
         * Shadow map used for creating shadows for directional light and spot lights.
         * @private
         * @property shadowMap
         * @type ShadowMap
         */
        this.shadowMap = new ShadowMap();
        this.shadowMap.init(ctx.width, ctx.height);
    }

    /**
     * Adds an object to the scene.
     *
     * @method addObject
     * @param {GameObject} obj The object that becomes added to the scene.
     */
    Scene.prototype.addObject = function(obj) 
    {
        this._objects[obj.name] = obj;

        if(obj.isCamera && !this.currentCamera)
        {
            this.currentCamera = obj;
        }
        
        var light = obj.getModule("PointLight") || obj.getModule("DirectLight");
        if(light)
        {
            this.lightManager.addLight(obj);
        }
    };

    /**
     * Adds an debug object to the scene.
     *
     * Debug objects are only stored one frame.
     * @method addDebugObject
     * @param {DebugDraw} obj The debug object. 
     */
    Scene.prototype.addDebugObject = function(obj)
    {
        this._debugObjects.push(obj);
        if(!this._debugProgram)
        {
            var vertShader = Utils.compileShader(debugVertShader, gl.VERTEX_SHADER);
            var fragShader = Utils.compileShader(debugFragShader, gl.FRAGMENT_SHADER);

            this._debugProgram = Utils.createShaderProgram([vertShader, fragShader], ["position"]);
            this._debugMaterial = new Material(this._debugProgram);
        }
    }

    /**
     * Removes an object from the scene.
     *
     * @method removeObject
     * @param {String} objName The name of the object to delete.
     */
    Scene.prototype.removeObject = function(objName)
    {
        // We don't need to check if objName exist,
        // since we let delete do the work.
        delete this._objects[objName];
    };

    var testVertShader = 
                         "attribute vec3 position;" +
                         "attribute vec3 normal;" +
                         "attribute vec2 texCoord;" +

                         "varying vec2 varTexCoord;" +

                         "void main(){" +
                         "gl_Position = vec4(position.xy, 0.0, 1.0);" +
                         "varTexCoord = position.xy * 0.5 + 0.5;" +
                         "}";
    var testFragShader = 
                         "precision highp float;" +
                         "varying vec2 varTexCoord;" +
                         "uniform sampler2D tex0;" +
                         "void main(){" +
                         "gl_FragColor = vec4(texture2D(tex0, varTexCoord).r, 0.0, 0.0, 1.0);}";

    /**
     * Render the scene, which would efficiently render all objects that has a mesh.
     * 
     * It will also simulate lights and shadows if specified.
     * @method render
     */
    Scene.prototype.render = function()
    {
        this.lightManager.reset();
        this._visibleObjects.length = 0;

        var viewProjMat;
        var viewMat;
        if(this.currentCamera)
        {
            viewProjMat = this.currentCamera.getViewProjMatrix();
            viewMat = this.currentCamera.getViewMatrix();
        }
        else
        {
            viewProjMat = glm.mat4.create();
            viewMat = glm.mat4.create();
        }

        if(this.rootNode)
        {
            for(var i in this._objects)
            {
                this._objects[i].isVisible = false;
            }

            this.rootNode.findObjectsInFrustum(this.currentCamera.planes);
        }

        for(var i in this._objects)
        {
            var currentObj = this._objects[i];
            currentObj.update();

            var volume = currentObj.getModule("BoundingVolume");
            if(volume)
            {
                if(!currentObj.bvhNode)
                {
                    if(!this.rootNode)
                    {
                        this.rootNode = new BVHNode(null, volume.boundingVolume, currentObj);
                        currentObj.bvhNode = this.rootNode;
                    }
                    else
                    {
                        var result = this.rootNode.insert(currentObj, volume.boundingVolume);
                        if(result.oldNode)
                            this._objects[result.oldNode.gameObject.name].bvhNode = result.oldNode;
                        
                        currentObj.bvhNode = result.newNode;
                    }
                }

                if(volume._isDirty)
                {
                    if(currentObj.bvhNode)
                    {
                        var oldNode = currentObj.bvhNode.destroyMe();
                        if(oldNode && oldNode.gameObject)
                        {
                            this._objects[oldNode.gameObject.name].bvhNode = oldNode;
                        }
                    }
                    
                    if(!this.rootNode)
                    {
                        this.rootNode = new BVHNode(null, volume.boundingVolume, currentObj);
                        currentObj.bvhNode = this.rootNode;
                    }
                    else
                    {
                        var result = this.rootNode.insert(currentObj, volume.boundingVolume);
                        if(result.oldNode)
                            this._objects[result.oldNode.gameObject.name].bvhNode = result.oldNode;
                        
                        currentObj.bvhNode = result.newNode;
                    }
                }
            }
            
            if(currentObj.isVisible)
            {
                this._visibleObjects.push(currentObj);
            }
        }

        if(!this._debugProgram)
        {
            var vertShader = Utils.compileShader(testVertShader, gl.VERTEX_SHADER);
            var fragShader = Utils.compileShader(testFragShader, gl.FRAGMENT_SHADER);

            this._debugProgram = Utils.createShaderProgram([vertShader, fragShader], ["position", "normal", "texCoord"]);
            this._debugMaterial = new Material(this._debugProgram);
        }

        /*this.shadowPass(viewMat, viewProjMat);

        this.shadowMap.bindForReading(gl.TEXTURE0);

        this._debugMaterial.use();
        var planeMesh = new PlaneMesh();
        planeMesh.render();*/

        for(var i = 0;i < this._visibleObjects.length;i++)
        {
            this.drawObject(this._visibleObjects[i], viewProjMat, viewMat, true);
        }

        if(this._debugObjects.length > 0)
        {   
            gl.enable(gl.BLEND);
            gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
            this._debugMaterial.setUniform("wgf_vpMat", viewProjMat);
            
            // Lets render the debug objects.
            for(var i = 0;i < this._debugObjects.length;i++)
            {
                var obj = this._debugObjects[i];
                this._debugMaterial.setUniform("wgf_mMat", obj.modelMatrix);
                this._debugMaterial.setUniform("color", obj.color);
                this._debugMaterial.use();

                var oldRenderMode = obj.mesh.renderMode;
                obj.mesh.renderMode = obj.type;
                obj.mesh.render();

                obj.mesh.renderMode = oldRenderMode;
            }
            gl.disable(gl.BLEND);
        }

        this._debugObjects = new Array();
    }

    /**
     * Shadow pass pre-renders the scene to a depth texture 
     * used for lookup during the real rendering.
     * @method shadowPass
     */
    Scene.prototype.shadowPass = function(viewMat, viewProjMat)
    {   
        gl.bindTexture(gl.TEXTURE_2D, null);
        this.shadowMap.bindForWriting();

        gl.clear(gl.DEPTH_BUFFER_BIT);

        //for(var i = 0;i < this._visibleObjects.length;i++)
        //{
        this.drawObject(this._objects["Teapot"], viewProjMat, viewMat, false);
        //}

        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    };

    /**
     * Draws an object.
     * @method drawObject
     * @param {GameObject} object The object to be drawn.
     * @param {mat4} viewProjMat The view->proj matrix.
     * @param {mat4} viewMat The view matrix.
     * @param {Boolean} [withLight=true] If true the light is rendered with light, else not.
     */
    Scene.prototype.drawObject = function(object, viewProjMat, viewMat, withLight)
    {
        // Set withLight to default value, which is true.
        withLight = withLight === undefined ? true : withLight;

        var materialModule = object.getModule("Material");
        var meshModule = object.getModule("Mesh");
        if(!object.isHidden && materialModule && meshModule)
        {   
            var transformModule = object.getModule("Transformation");
            var modelMat = transformModule.getModelMatrix();

            if(materialModule.hasUniform("wgf_mvpMat"))
            {
                var mvpMat;
                if(transformModule)
                {
                    mvpMat = glm.mat4.mul(glm.mat4.create(), viewProjMat, modelMat);
                }
                else
                {
                    mvpMat = viewProjMat;
                }

                materialModule.setUniform("wgf_mvpMat", mvpMat);
            }

            if(materialModule.hasUniform("wgf_mMat"))
            {
                materialModule.setUniform("wgf_mMat", modelMat);
            }

            if(materialModule.hasUniform("wgf_vMat"))
            {
                materialModule.setUniform("wgf_vMat", viewMat);
            }

            if(materialModule.hasUniform("wgf_vmMat"))
            {
                var vmMat = glm.mat4.mul(glm.mat4.create(), viewMat, modelMat);
                if(materialModule.hasUniform("wgf_vmMat"))
                {
                    materialModule.setUniform("wgf_vmMat", vmMat);
                }
            }

            if(materialModule.hasUniform("wgf_normalMat"))
            {
                var inverseVM = glm.mat4.invert(glm.mat4.create(), modelMat);
                inverseVM = glm.mat3.fromMat4(glm.mat3.create(), inverseVM);
                glm.mat3.transpose(inverseVM, inverseVM);
                materialModule.setUniform("wgf_normalMat", inverseVM);
            }

            if(materialModule.hasUniform("wgf_vpMat"))
            {
                materialModule.setUniform("wgf_vpMat", viewProjMat);
            }

            if(materialModule.hasUniform("wgf_eyePos"))
            {
                materialModule.setUniform("wgf_eyePos", [viewMat[12], viewMat[13], viewMat[14]]);
            }

            if(withLight)
            {
                this.lightManager.useOn(materialModule);
            }

            materialModule.use();

            meshModule.render();
        }
    };

    return Scene;
});
/**
 * A series of Game Object modules.
 * @module Modules
 */
define('Modules/Transformation',["libs/gl-matrix"], function(glm)
{
	/**
	 * Transformation is a module that can be added to a game object.
	 * It gives the game object the ability to transform itself.
	 * Transformation yields to scale, orientation and translation.
	 *
	 * @class Transformation
	 * @constructor
	 */
	Transformation = function()
	{
		/**
		 * Name of the module
		 * 
		 * @property name
		 * @type String
		 */
		this.name = "Transformation";

		/**
		 * Stores the orientation.
	 	 *
		 * @private
		 * @property _orientation
		 * @type quat
		 * @default [0, 0, 0, 1]
		 */
		this._orientation = glm.quat.create();

		/**
		 * Stores the position.
		 *
		 * @private
		 * @property _position
		 * @type vec3
		 * @default [0, 0, 0]
		 */
		this._position = glm.vec3.create();

		/**
		 * Stores the scale.
		 *
		 * @private
		 * @property _scale
		 * @type vec3
		 * @default [1, 1, 1]
		 */
		this._scale = glm.vec3.fromValues(1, 1, 1);

		/**
		 * Stores the model matrix.
		 * 
		 * The matrix may not be up to date.
		 * Use {{#crossLink "Transformation/getModelMatrix:method"}}{{/crossLink}} to get the matrix!
		 *
		 * @private
		 * @property _modelMatrix
		 * @type mat4
		 */
		this._modelMatrix = glm.mat4.create();

		/**
		 * True when the model matrix has not been updated.
		 * @private
		 * @property _isDirty
		 * @type Bool
		 * @default false
		 */
		this._isDirty = false;
	}

	/**
	 * Set the position.
	 * @method setPosition
	 * @param {vec3} p Position to set.
	 */
	Transformation.prototype.setPosition = function(p)
	{
		this._position = glm.vec3.clone(p);
		this._isDirty = true;
	};

	/**
	 * Set the scale.
	 * @method setScale
	 * @param {vec3} s Scale to set.
	 */
	Transformation.prototype.setScale = function(s)
	{
		this._scale = glm.vec3.clone(s);
		this._isDirty = true;
	};

	/**
	 * Set the orientation.
	 * @method setOrientation
	 * @param {quat} o Orientation to set.
	 */
	Transformation.prototype.setOrientation = function(o)
	{
		this._orientation = glm.quat.clone(o);
		this._isDirty = true;
	};

	/**
	 * Get the position.
	 * @method getPosition
	 * @return {vec3} Returns a copy of the position.
	 */
	Transformation.prototype.getPosition = function()
	{
		return glm.vec3.clone(this._position);
	};

	/**
	 * Get the scale.
	 * @method getScale
	 * @return {vec3} Returns a copy of the scale.
	 */
	Transformation.prototype.getScale = function()
	{
		return glm.vec3.clone(this._scale);
	};

	/**
	 * Get the orientation.
	 * @method getOrientation
	 * @return {quat} Returns a copy of the orientation.
	 */
	Transformation.prototype.getOrientation = function()
	{
		return glm.quat.clone(this._orientation);
	};

	/**
	 * Returns the model matrix, 
	 * which is calculated from the orientation, translation and scale.
	 *
	 * @method getModelMatrix
	 * @return {mat4} The model matrix.
	 */
	Transformation.prototype.getModelMatrix = function() 
	{
		if(this._isDirty)
		{
			this._updateMatrix();
			this._isDirty = false;
		}
		return this._modelMatrix;
	};

	/**
	 * Updates the model matrix.
	 * @private
	 * @method _updateMatrix
	 */
	Transformation.prototype._updateMatrix = function()
	{
		//Optimized scaling, because we are working with a identity matrix.
		var scaleMat = glm.mat4.create(); 
		scaleMat[0] = this._scale[0];
		scaleMat[5] = this._scale[1];
		scaleMat[10] = this._scale[2];

		glm.mat4.fromRotationTranslation(this._modelMatrix, this._orientation, this._position);
		glm.mat4.multiply(this._modelMatrix, this._modelMatrix, scaleMat);
	};

	return Transformation;
});
/**
 * A series of Game Object modules.
 * @module Modules
 */
define('Modules/Material',[],function()
{	
	/**
	 * Material is a Game Object module.
	 *
	 * Material is a handler for shader programs.
	 * Used during rendering.
	 *
	 * @class Material
	 * @constructor
	 * @param {ShaderProgram} prog Reference to a program, which the material will use.
	 */
	Material = function(prog)
	{
		/**
		 * Name of the module
		 * 
		 * @property name
		 * @type String
		 */
		this.name = "Material";

		/**
		 * ShaderProgram that belongs to this material.
		 * The program can be shared between materials.
		 *
		 * @private
		 * @property _program
		 * @type ShaderProgram
		 */
		this._program = prog;

		/** 
		 * Stores the values for the uniforms.
		 * @private
		 * @property _uniforms
		 * @type [String] = {value, type}
		 */
		this._uniforms = new Object;

		/**
		 * Textures that are bound to the material.
		 * @private
		 * @property _textures
		 * @type [String] = Texture
		 */
		this._textures = new Object;
	}

	/**
	 * The basic type of a uniform.
	 * @property UNIFORM_TYPES
	 * @type Int
	 */
	Material.UNIFORM_TYPES = {INT: 1, FLOAT: 2} 

	/**
	 * Set this material to current material.
	 *
	 * @method use
	 */
	Material.prototype.use = function() 
	{
		if(!this._program)
		{
			console.log("Can't use material without a shader program!");
			return;
		}

		this._program.use();

		var texCount = 0;
		for(var key in this._textures)
		{
			this._textures[key].bind(gl.TEXTURE0 + texCount);
			gl.uniform1i(this._program.getUniformLocation(key), texCount);

			++texCount;
		}

		for(key in this._uniforms)
		{
			var value = this._uniforms[key].value;
			var type = this._uniforms[key].type;
			var loc = this._program.getUniformLocation(key);
			
			if(type == Material.UNIFORM_TYPES.FLOAT)
			{
				if(value.length != null)
				{
					// I have tried to prioritize the if checks for optimization.
					if(value.length == 3)
					{
						gl.uniform3fv(loc, value);
					}
					else if(value.length == 16)
					{
						gl.uniformMatrix4fv(loc, gl.FALSE, value);
					}
					else if(value.length == 2)
					{
						gl.uniform2fv(loc, value);
					}
					else if(value.length == 4)
					{
						gl.uniform4fv(loc, value);
					}
					else if(value.length == 9)
					{
						gl.uniformMatrix3fv(loc, gl.FALSE, value);
					}
					else
					{
						console.log("Unknown number of elements for uniform " + key)
					}
				}
				else
				{
					gl.uniform1f(loc, value);
				}
			}
			else if(type == Material.UNIFORM_TYPES.INT)
			{
				if(value.length != null)
				{
					// I have tried to prioritize the if checks for optimization.
					if(value.length == 3)
					{
						gl.uniform3iv(loc, value);
					}
					else if(value.length == 16)
					{
						gl.uniformMatrix4iv(loc, gl.FALSE, value);
					}
					else if(value.length == 2)
					{
						gl.uniform2iv(loc, value);
					}
					else if(value.length == 4)
					{
						gl.uniform4iv(loc, value);
					}
					else if(value.length == 9)
					{
						gl.uniformMatrix3iv(loc, gl.FALSE, value);
					}
					else
					{
						console.log("Unknown number of elements for uniform " + key)
					}
				}
				else
				{
					gl.uniform1i(loc, value);
				}
			}
			else
			{
				console.log("Unknown type of uniform " + key + "\n GL only support float or int.");
			}
		}
	};

	/**
	 * Set the shader program for the material.
	 *
	 * @method setShaderProgram
	 * @param {ShaderProgram} prog The shader program.
	 */
	Material.prototype.setShaderProgram = function(prog)
	{
		this._program = prog;
		
		delete this._uniforms;
		this._uniforms = new Object();
	}

	/**
	 * Binds a texture to a uniform sampler.
	 * @method bindTexture
	 * @param {String} uniformName Name of the uniform.
	 * @param {Texture} texture The texture that becomes bound to the uniform.
	 */
	Material.prototype.bindTexture = function(uniformName, texture)
	{
		this._textures[uniformName] = texture;
	};

	/**
	 * Returns true if we got the specific uniform in the program.
	 * @method hasUniform
	 * @return True if we got the uniform else false.
	 */
	Material.prototype.hasUniform = function(uniformName)
	{
		return this._program.getUniformLocation(uniformName) != null;
	};

	/**
	 * Set a uniform.
	 * @method setUniform
	 * @param {String} name Name of the uniform to set.
	 * @param {int, float, vec2, vec3, vec4, mat3, mat4} value 
	 * Can be of different types depending on what you want.
	 * @param {UNIFORM_TYPES} type Type of uniform, which can either be INT, or FLOAT.
	 * Optional, default is FLOAT.
	 */
	Material.prototype.setUniform = function(name, value, type)
	{
		if(!type)
		{
			type = Material.UNIFORM_TYPES.FLOAT;
		}

		this._uniforms[name] = {value: value, type: type};
	};

	return Material;
});
define('Math/Plane',["libs/gl-matrix"], function(glm)
{
	/**
	 * Plane is a mathematical representation of an infinity subdivision of space.
	 * @class Plane
	 */
	Plane = function()
	{
		/**
		 * The plane normal.
		 * @property normal
		 * @type vec3
		 */
		this.normal = glm.vec3.create();

		/**
		 * The d that solves the plane equation.
		 * @property d
		 * @type Number
		 */
		this.d = 0.0;
	}

	/**
	 * Set the plane coefficients, it will normalize the result too.
	 *
	 * In other words:
	 *
	 * 	 a(x1 - x0) + b(y1 - y0) + c(z1 - z0) + d = 0
	 *
	 * Where the normal is n = [a, b, c].
	 * @method setCoefficients
	 * @param {Number} a The x coordinate of the plane normal.
	 * @param {Number} b The y coordinate of the plane normal.
	 * @param {Number} c The z coordinate of the plane normal.
	 * @param {Number} d The plane constant.
	 */
	Plane.prototype.setCoefficients = function(a, b, c, d) 
	{
		this.normal = glm.vec3.fromValues(a, b, c);
		var length = glm.vec3.len(this.normal);
		glm.vec3.scale(this.normal, this.normal, 1.0 / length);
		this.d = d /length;
	};

	/**
	 * Computes the distance from the plane to a point.
	 * @method distance
	 * @param {Number} p The point to compute distance for.
	 * @return {Number} The distance to the point.
	 */
	Plane.prototype.distance = function(p)
	{
		// Calculates the distance.
		return glm.vec3.dot(p, this.normal) + this.d;
	};

	return Plane;
});
/**
 * A series of Scene related classes.
 * @module Scene
 */
define('Scene/Camera',["Scene/GameObject", "libs/gl-matrix", "Modules/Transformation", "Math/Plane", "Math/vec3Ext"], 
		function(GameObject, glm, Transformation, Plane, vec3Ext)
{
	/**
	 * Camera is a predefined GameObject used for camera related stuff.
	 * 
	 * Every scene should have a camera.
	 * You can have multiple cameras in the scene, but then you have to specify which camera to use.
	 *
	 * @class Camera
	 * @constructor
	 * @extends GameObject
	 * @param {String} name Name of the camera.
	 */
	Camera = function(name)
	{
		/**
		 * Name of Camera instance.
		 *
		 * @property name
		 * @type String
		 */
		this.name = name;

		/** 
		 * Projection matrix.
		 *
		 * @property projMat
		 * @type Mat4
		 */
		this.projMat = glm.mat4.create();

		/** 
		 * A switch so that we know that this is a special case of a GameObject.
		 *
		 * @property isCamera
		 * @type Bool
		 * @default true
		 */
		this.isCamera = true;

		/**
		 * Override the isHidden and set it to true.
		 * Since the camera can't be rendered.
		 *
		 * @property isHidden
		 * @type Bool
		 * @default true
		 */
		this.isHidden = true;

		/**
		 * Stores the frustum planes for this camera.
		 * It is updated each time 
		 * {{#crossLink "Camera/getViewProjMatrix:method"}}{{/crossLink}} is called,
		 * or when calling {{#crossLink "Camera/updateFrustumPlanes:method"}}{{/crossLink}}
		 * @property planes
		 * @type Array(6).Plane
		 */
		this.planes = new Array(new Plane, new Plane, new Plane, 
							    new Plane, new Plane, new Plane);

		/**
		 * Stores the latest view->projection matrix.
		 * @private
		 * @property _viewProjMat
		 * @type mat4
		 */
		this._viewProjMat = null;

		/**
		 * Camera uses the transformation module.
		 * @property Transformation
		 * @type Module
		 */
		this.addModule(new Transformation);
	}

	Camera.prototype = new GameObject("Camera");

	Camera.PlaneEnum = {
		NEAR: 0,
		FAR: 1,
		BOTTOM: 2,
		TOP: 3,
		LEFT: 4,
		RIGHT: 5
	};

	/**
	 * Updates the frustum planes.
	 * @method updateFrustumPlanes
	 */
	Camera.prototype.updateFrustumPlanes = function()
	{
		var m = this._viewProjMat;
		var Plane = Camera.PlaneEnum;

		this.planes[Plane.NEAR].setCoefficients(m[3]+ m[2],
									   			m[7]+ m[6],
									   			m[11]+m[10],
									   			m[15]+m[14]);
		
		this.planes[Plane.FAR].setCoefficients(m[3]- m[2],
											   m[7]- m[6],
											   m[11]-m[10],
											   m[15]-m[14]);
		
		this.planes[Plane.BOTTOM].setCoefficients(m[3]+ m[1],
											   	  m[7]+ m[5],
												  m[11]+m[9],
												  m[15]+m[13]);
		
		this.planes[Plane.TOP].setCoefficients(m[3]- m[1],
											   m[7]- m[5],
											   m[11]-m[9],
											   m[15]-m[13]);

		this.planes[Plane.LEFT].setCoefficients(m[3]+ m[0],
											    m[7]+ m[4],
											    m[11]+m[8],
											    m[15]+m[12]);

		this.planes[Plane.RIGHT].setCoefficients(m[3]- m[0],
											     m[7]- m[4],
												 m[11]-m[8],
												 m[15]-m[12]);
	};

	/**
	 * Sets the projection matrix to perspective.
	 * @method makePerspective
	 * @param {Number} Field of view
	 * @param {Number} Aspect ratio
	 * @param {Number} Z-near plane
	 * @param {Number} Z-far plane
	 */
	Camera.prototype.makePerspective = function(fov, aspect, near, far)
	{
		glm.mat4.perspective(this.projMat, fov, aspect, near, far);
	}

	/**
	 * Returns the view projection matrix.
	 *
	 * @method getViewProjMatrix
	 * @return {mat4} Returns the view->projection matrix.
	 */
	Camera.prototype.getViewProjMatrix = function()
	{
		var transformModule = this.getModule("Transformation");
		this._viewMat = glm.mat4.invert(glm.mat4.create(), transformModule.getModelMatrix());
		this._viewProjMat = glm.mat4.mul(glm.mat4.create(), this.projMat, this._viewMat);

		this.updateFrustumPlanes();

		return this._viewProjMat;
	};

	/**
	 * Returns the view matrix.
	 * @return {mat4} Returns the view matrix.
	 */
	Camera.prototype.getViewMatrix = function()
	{
		return this._viewMat;
	};

	return Camera;
});
/**
 * A series of Game Object modules.
 * @module Modules
 */
 define('Modules/Script',[],function()
 {
 	/**
 	 * Script is a Game Object module, which gives you the ability to add logic to your Game Object.
 	 * Inherit this class and override the functions that you want.
 	 * @class Script
 	 * @constructor
 	 * @param {String} The name of the string.
 	 */
 	Script = function(name)
 	{
		/**
		 * Name of the script/module
		 * 
		 * @property name
		 * @type String
		 */
 		this.name = name;

 		/**
 		 * The GameObject that we belongs to.
 		 * @property gameObject
 		 * @type GameObject
 		 */
 		this.gameObject = null;

 		/**
 		 * True if we are a script.
 		 * Used to check for module type.
 		 * @property isScript
 		 * @default true
 		 */
 		this.isScript = true;
 	}

 	/**
 	 * Start is called when this module is added to a GameObject.
 	 * @method start
 	 */
 	Script.prototype.start = function() 
 	{
 	};

 	/**
 	 * Update is called for each update frame.
 	 * @method update
 	 */
 	Script.prototype.update = function()
 	{

 	};

 	/**
 	 * Exit is called when this module is removed from a GameObject.
 	 * @method exit
 	 */
 	Script.prototype.exit = function()
 	{

 	};

 	return Script;
 });
define('Texture',[],function()
{	
	/**
	 * Texture class is used to color your models using an image file as source.
	 * Textures can also be used for more advanced stuff, for example as a framebuffer target etc.
	 *
	 * @class Texture
	 * @constructor
	 * Creates a texture instance, from an image file.
	 * @param {String} fileName Path to an image file. Supported images equals to what the browser support.
	 */
	Texture = function(fileName) 
	{
		/**
		 * GL texture object.
		 *
		 * @property textureObj
		 * @type gl.TextureObject
		 */
		this.textureObj = null;

		/**
		 * Specifies if the image that belongs to the texture has been loaded.
		 * @property hasLoaded
		 * @type Bool
		 * @default false
		 */
		this.hasLoaded = false;

		/**
		 * Texture unit, that our texture is bound to.
		 * @private
		 * @property _textureUnit
		 */
		this._textureUnit = null;

		if(fileName)
		{
			this.loadImage(fileName);
		}
	}

	/**
	 * Loads an image, and creates the GL texture object for that.
	 *
	 * NOTE: This function deletes the old texture object and data,
	 * 		 if a texture object already exist.
	 * @method loadImage
	 * @param {String} fileName Path to the image file.
	 */
	Texture.prototype.loadImage = function(fileName) 
	{
		// Free texture object, if we already have an object.
		if(this.textureObj)
		{
			gl.deleteTexture(texture.textureObj);
		}
		this.textureObj = gl.createTexture();

		var image = new Image();
		image.src = fileName;

		var thisTexture = this;
		image.onload = function()
		{
			gl.bindTexture(gl.TEXTURE_2D, thisTexture.textureObj);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		  	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
			gl.generateMipmap(gl.TEXTURE_2D);
			gl.bindTexture(gl.TEXTURE_2D, null);

			thisTexture.hasLoaded = true;
		};
	};

	/**
	 * Binds the texture as the active texture.
	 * @method bind
	 * @param {GLenum} textureUnit The texure unit to bound to. Optional, default gl.TEXTURE0.
	 */
	Texture.prototype.bind = function(textureUnit)
	{
		if(!textureUnit)
		{
			textureUnit = gl.TEXTURE0;
		}
		this._textureUnit = textureUnit;
		gl.activeTexture(textureUnit);
		gl.bindTexture(gl.TEXTURE_2D, this.textureObj);
	};

	/**
	 * Unbinds the texture from being the active texture.
	 * @method unbind
	 */
	Texture.prototype.unbind = function() 
	{
		if(this._textureUnit)
		{
			gl.activeTexture(this._textureUnit);
			gl.bindTexture(gl.TEXTURE_2D, null);
			this._textureUnit = null;
		}
	};

	return Texture;
});
define('Loaders/ObjParser',["Modules/Mesh"], function(Mesh)
{
	/**
	 * Used to parse models that are in the Obj model format. 
	 * @class ObjParser
	 * @constructor
	 */
	ObjParser = function()
	{
		/**
		 * Stores the vertices after the parse.
		 * @property vertices
		 * @type Array.Number
		 */
		this.vertices = new Array();

		/**
		 * Stores the texture coordinates after parsing obj data.
		 * @property textureCoords
		 * @type Array.Number
		 */
		this.textureCoords = new Array;

		/**
		 * Stores the normals after parsing obj data.
		 * @property normals
		 * @type Array.Number
		 */
		this.normals = new Array;

		/**
		 * Stores the indices for the mesh.
		 * @property indices
		 * @type Array.Number
		 */
		this.indices = new Array;

		/**
		 * Stores the texture coordinates in raw temporary format.
		 * @private
		 * @property _textureCoords
		 * @type Array.Number
		 */
		this._textureCoords = new Array;

		/**
		 * Stores the normals in raw temporary format.
		 * @private
		 * @property _normals
		 * @type Array.Number
		 */
		this._normals = new Array;

		/**
		 * Stores the vertices in raw temporary format.
		 * @private
		 * @property _vertices
		 * @type Array.Number
		 */
		 this._vertices = new Array;
	}

	/**
	 * Parses a string storing obj data.
	 * @param {String} data The data to be parsed.
	 */
	ObjParser.prototype.parseData = function(data)
	{
		var lines = data.split("\n");
		for(var i = 0;i < lines.length;i++)
		{
			var line = lines[i];
			var statements = line.split(" ");
			if(statements[0] == "v")
			{
				ObjParser._getData(statements, this._vertices);
			}
			else if(statements[0] == "vt")
			{
				ObjParser._getData(statements, this._textureCoords);
			}
			else if(statements[0] == "vn")
			{
				ObjParser._getData(statements, this._normals);
			}
			else if(statements[0] == "f")
			{
				var faceIndices = new Array();
				ObjParser._getData(statements, faceIndices, true);

				for(var j = 0;j < faceIndices.length;j++)
				{
					var elements = faceIndices[j].split("/");

					var vertIndex = parseInt(elements[0]);
					if(!isNaN(vertIndex))
					{
						this.vertices.push(this._vertices[(vertIndex-1)*3]);
						this.vertices.push(this._vertices[(vertIndex-1)*3 + 1]);
						this.vertices.push(this._vertices[(vertIndex-1)*3 + 2]);
					}

					if(this._textureCoords.length > 0)
					{
						var texIndex = vertIndex;
						if(elements.length == 2)
						{
							texIndex = parseInt(elements[1]);
						}
						
						if(!isNaN(texIndex))
						{
							this.textureCoords.push(this._textureCoords[(texIndex-1)*2]);
							this.textureCoords.push(this._textureCoords[(texIndex-1)*2 + 1]);
						}
					}
					
					if(this._normals.length > 0)
					{
						var normalIndex = vertIndex;
						if(elements.length == 3)
						{
							normalIndex = parseInt(elements[2]);
						}
						
						if(!isNaN(normalIndex))
						{
							this.normals.push(this._normals[(normalIndex-1)*3]);
							this.normals.push(this._normals[(normalIndex-1)*3 + 1]);
							this.normals.push(this._normals[(normalIndex-1)*3 + 2]);
						}
					}
				}
			}
		}
	};

	/**
	 * Converts the loaded obj file to renderable mesh.
	 * @method toMesh
	 * @return {Mesh} Returns the renderable mesh.
	 */
	ObjParser.prototype.toMesh = function()
	{
		var mesh = new Mesh;
		var normals = this.normals.length > 0 ? this.normals : null;
		var texCoords = this.textureCoords.length > 0 ? this.textureCoords : null;
		mesh.setVertices(this.vertices, normals, texCoords);
		return mesh;
	};

	/**
	 * Returns the data found in a statement.
	 * @private
	 * @method _getData
	 */
	ObjParser._getData = function(knots, out, parseRaw)
	{
		var counter = 0;
		for(var i = 1;i < knots.length;i++)
		{
			if(knots[i] != "")
			{
				++counter;
				if(parseRaw)
				{
					out.push(knots[i]);
				}
				else
				{
					out.push(parseFloat(knots[i]));
				}
			}
		}

		return counter;
	};

	return ObjParser;
});
define('Scene/DirectLight',["Scene/BaseLight", "libs/gl-matrix"], function(BaseLight, glm)
{
	/**
	 * Directional light, only support for one per scene!
	 * @class DirectLight
	 * @constructor
	 */
	DirectLight = function()
	{
		this.name = "DirectLight";

		/**
		 * Light direction
		 * @property direction
		 * @type vec3 
		 */
		this.direction = glm.vec3.create();

		/**
		 * The base light part of the light.
		 * @property base
		 * @type BaseLight
		 */
		this.base = new BaseLight();
	}

	return DirectLight;
});
define('WebGLFun',["Context", "Utils", "Math/MathExt", "libs/gl-matrix", 
		"Modules/Mesh", "Mesh/CubeMesh", "Scene/GameObject", "Scene/Scene",
		"Modules/Transformation", "Modules/Material", "Scene/Camera",
		"Math/vec3Ext", "Modules/Script", "Texture", "Math/AABBox",
		"Scene/BVHNode", "DebugDraw", "Modules/BoundingVolume", "Math/Plane",
		"Loaders/ObjParser", "Scene/PointLight", "Scene/DirectLight"],
function(context, utils, mathext, glMatrix, mesh, cubeMesh, gameObject, 
		scene, transform, material, camera, vec3Ext, script, texture,
		aabbox, bvhNode, debugDraw, boundingVolume, plane, objParser,
		pointLight, directLight)
{
	return {Context: context,
			Utils: utils, 
			Mesh: mesh,
			CubeMesh: cubeMesh,
			GameObject: gameObject,
			Scene: scene,
			Transformation: transform,
			Material: material,
			Camera: camera,
			Script: script,
			Texture: texture,
			AABBox: aabbox,
			BVHNode: bvhNode,
			DebugDraw: debugDraw,
			BoundingVolume: boundingVolume,
			Plane: plane,
			ObjParser: objParser,
			PointLight: pointLight,
			DirectLight: directLight,
			
			/**
			 * Wrappers for glMatrix library
			 */
			vec2: glMatrix.vec2, 
			vec3: glMatrix.vec3, 
			vec4: glMatrix.vec4,
			quat: glMatrix.quat,
			mat2: glMatrix.mat2,
			mat3: glMatrix.mat3,
			mat4: glMatrix.mat4,
			mat2d: glMatrix.mat2d
			};
});