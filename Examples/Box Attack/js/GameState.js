/**
 * GameState defines the state the game is in when in-game. 
 * @class GameState
 */
 define(["./State", "WebGLFun", "CameraMovement", "LightMovement"], function(State, wgf, CameraMovement, LightMovement)
 {
    function init()
    {
        this.hasLoaded = false;
        
        this.scene = new wgf.Scene;

        this.camera = new wgf.Camera("Camera01");
        this.camera.makePerspective(45.0, 1.0, 0.001, 500.0);
        this.camera.addModule(new CameraMovement);
        this.scene.addObject(this.camera);

        var pointLight = new wgf.PointLight();
        pointLight.base.color[0] = 0.2;
        pointLight.base.color[1] = 0.8;
        pointLight.base.color[2] = 0.2;
        pointLight.base.diffuseIntensity = 10.0;
        pointLight.base.ambientIntensity = 40.0;
        pointLight.attenuation.constant = 0.0;
        pointLight.attenuation.linear = 0.0;
        pointLight.attenuation.exp = 1.0;
        pointLight.position[1] = 2.0;

        var pointLightObj = new GameObject("Light0");
        pointLightObj.addModule(pointLight);
        pointLightObj.addModule(new LightMovement());
        pointLightObj.addModule(new BoundingVolume(false));
        this.scene.addObject(pointLightObj);

        var directLightData = new wgf.DirectLight();
        directLightData.base.color = wgf.vec3.fromValues(1, 1, 1);
        directLightData.base.diffuseIntensity = 0.5;
        directLightData.base.ambientIntensity = 0.2;
        directLightData.direction[1] = -0.8; 
        directLightData.direction[2] = -1.0;

        var directLight = new GameObject("LightDirectional");
        directLight.addModule(directLightData);
        this.scene.addObject(directLight);

        this.cubeTex = new wgf.Texture("media/cube.jpg");

        wgf.Utils.loadFiles.call(this, ["shaders/teapot.vert", "shaders/teapot.frag", "media/teapot.obj"], 
            function(shaderSrcs)
        {
            var objParser = new wgf.ObjParser;
            objParser.parseData(shaderSrcs[2]);

            var mesh = objParser.toMesh();

            var vertShader = wgf.Utils.compileShader(shaderSrcs[0], gl.VERTEX_SHADER);
            var fragShader = wgf.Utils.compileShader(shaderSrcs[1], gl.FRAGMENT_SHADER);

            var cubeProgram = wgf.Utils.createShaderProgram([vertShader, fragShader], ["position", "normal", "uvCoord"]);
            var material = new wgf.Material(cubeProgram);
            material.bindTexture("tex0", this.cubeTex);
            material.setUniform("specularIntensity", 1);
            material.setUniform("specularPower", 32);

            var teapot = new wgf.GameObject("Teapot");
            teapot.addModule(mesh);
            //teapot.addModule(new wgf.CubeMesh());
            transform = teapot.addModule(new wgf.Transformation());
            teapot.addModule(new wgf.BoundingVolume(true));
            teapot.addModule(material);

            transform.setPosition(wgf.vec3.fromValues(0, 0, 0));
            this.scene.addObject(teapot);

            this.hasLoaded = true;
        });

        //gl.enable(gl.CULL_FACE);
        gl.enable(gl.DEPTH_TEST);
        gl.cullFace(gl.BACK);
    }

    function execute()
    {
        if(this.hasLoaded)
        { 
            this.scene.render();
        }
    }

    function exit()
    {
    }

    return new State(init, execute, exit);
 });