/**
 * State is used to handle the callbacks from a state machine.
 * @class State
 */
define(function()
{    
    State = function(initCallback, executeCallback, exitCallback)
    {
        this.initCallback = initCallback;
        this.executeCallback = executeCallback;
        this.exitCallback = exitCallback;
    }

    return State;
});