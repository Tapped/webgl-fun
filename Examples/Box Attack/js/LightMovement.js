define(["WebGLFun"], function(wgf)
{
    LightMovement = function() 
    {
        console.log("Constructor was called");
    }

    LightMovement.prototype = new wgf.Script("LightMovement");

    LightMovement.prototype.start = function() 
    {
    };

    LightMovement.prototype.update = function() 
    {
        var time = Context.getTimestamp() / 1000.0;
        var module = this.gameObject.getModule("PointLight"); 
        module.position[0] = 12.0 * Math.cos(time);
        module.position[2] = 12.0 * Math.sin(time);
        module.isDirty = true;
    };

    return LightMovement;
});