requirejs.config({
    //By default load any module IDs from js
    baseUrl: "js",

    paths: {
        libs: "../../../Libs",
        WebGLFun: "../../../Bin/WebGLFun",
    }
});

deltaTime = 0;
frameTime = 0;

requirejs(["WebGLFun", "./StateMachine", "./GameState"], function(WebGLFun, StateMachine, GameState)
{
    var webGLFun = new WebGLFun.Context;
    webGLFun.setup(document.getElementById("glCanvas"));

    var stateMachine = new StateMachine(GameState);

    anim();

    var oldTime = 0;
    function anim()
    {
        frameTime = Context.getTimestamp() - oldTime;
        oldTime = Context.getTimestamp();
        deltaTime = (frameTime / 1000.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        stateMachine.executeState();
        requestAnimationFrame(anim);
    }
});
