/**
 * StateMachine is a module, that gives support for handling different finite states.
 * @class StateMachine
 */
define(["./State"], function(State)
{ 
    StateMachine = function(state)
    {
        this._currentState = state;
        this._currentState.initCallback();
    }

    /**
     * Call this function to set the current state. 
     * It will handle the unloading of the old state.
     * @method useState
     */
    StateMachine.prototype.useState = function(state) 
    {
        if(this._currentState)
        {
            this._currentState.exitCallback();
        }

        if(!state)
        {
            this._currentState = null;
            return;
        }

        this._currentState = state;
        this._currentState.initCallback();
    };

    /**
     * Executes the current state.
     * @method executeState
     */
    StateMachine.prototype.executeState = function()
    {
        if(this._currentState)
        {
            this._currentState.executeCallback();
        }
    }

    return StateMachine;
});