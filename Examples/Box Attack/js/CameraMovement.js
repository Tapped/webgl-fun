define(["WebGLFun"], function(wgf)
{
    var cameraSpeed = 10.0;
    CameraMovement = function() 
    {
        this.moveLeft = false;
        this.moveRight = false;
        this.moveIn = false;
        this.moveOut = false;

        this.oldMousePos = null;
        this.yOrient = 0;
        this.xOrient = 0;
    }

    CameraMovement.prototype = new wgf.Script("CameraMovement");

    CameraMovement.prototype.start = function() 
    {
        var camera = this;
        document.addEventListener("keydown", function(event)
        {    
            if(event.keyCode == 87)
            {
                camera.moveIn = true; 
            }
            else if(event.keyCode == 83)
            {
                camera.moveOut = true;
            }
            else if(event.keyCode == 65)
            {
                camera.moveLeft = true;
            }
            else if(event.keyCode == 68)
            {
                camera.moveRight = true;
            }
        });

        document.addEventListener("keyup", function(event)
        {
            if(event.keyCode == 87)
            {
                camera.moveIn = false; 
            }
            else if(event.keyCode == 83)
            {
                camera.moveOut = false;
            }
            else if(event.keyCode == 65)
            {
                camera.moveLeft = false;
            }
            else if(event.keyCode == 68)
            {
                camera.moveRight = false;
            }
        });

        document.addEventListener("mousemove", function(event)
        {
            var mousePos = wgf.vec2.fromValues(event.screenX, event.screenY);

            if(!camera.oldMousePos)
            {
                camera.oldMousePos = wgf.vec2.clone(mousePos);
                return;
            }

            var deltaMouse = wgf.vec2.sub(wgf.vec2.create(), camera.oldMousePos, mousePos);
            camera.oldMousePos = wgf.vec2.clone(mousePos);

            var transform = camera.gameObject.getModule("Transformation");
            camera.xOrient += deltaMouse[1] * 0.5 * deltaTime;
            camera.yOrient += deltaMouse[0] * 0.5 * deltaTime;

            var clampFactor = (Math.PI / 2) + 0.1;
            if(Math.abs(camera.xOrient) > clampFactor)
            {
                if(camera.xOrient > 0.0)
                {
                    camera.xOrient = clampFactor;
                }
                else
                {
                    camera.xOrient = -clampFactor;
                }
            }

            var tmpOrient = wgf.quat.rotateY(wgf.quat.create(), wgf.quat.create(), camera.yOrient);
            wgf.quat.rotateX(tmpOrient, tmpOrient, camera.xOrient);
            wgf.quat.normalize(tmpOrient, tmpOrient);

            transform.setOrientation(tmpOrient);
        });
    };

    CameraMovement.prototype.update = function() 
    {
        var translation = wgf.vec3.create();
        if(this.moveLeft)
        {
            translation[0] -= cameraSpeed * Math.cos(this.yOrient) * deltaTime;
            translation[2] += cameraSpeed * Math.sin(this.yOrient) * deltaTime;
        }
        if(this.moveRight)
        {
            translation[0] += cameraSpeed * Math.cos(this.yOrient) * deltaTime;
            translation[2] -= cameraSpeed * Math.sin(this.yOrient) * deltaTime;
        }
        if(this.moveIn)
        {
            translation[0] -= cameraSpeed * Math.sin(this.yOrient) * deltaTime;
            //translation[1] += cameraSpeed * Math.sin(this.xOrient);
            translation[2] -= cameraSpeed * Math.cos(this.yOrient) * deltaTime;
        }
        if(this.moveOut)
        {
            translation[0] += cameraSpeed * Math.sin(this.yOrient) * deltaTime;
            //translation[1] -= cameraSpeed * Math.sin(this.xOrient);
            translation[2] += cameraSpeed * Math.cos(this.yOrient) * deltaTime;
        }

        var cameraTransform = this.gameObject.getModule("Transformation");
        var currentPos = cameraTransform.getPosition();
        currentPos[1] = 2.0;
        cameraTransform.setPosition(wgf.vec3.add(currentPos, currentPos, translation));
    };

    return CameraMovement;
});