precision highp float;

#define MAX_POINT_LIGHTS 5

struct BaseLight
{
    vec3 color;
    vec2 intensity;
};

struct PointLight
{
    vec3 position;
    vec3 attenuation;
    BaseLight base;
};

uniform PointLight wgf_pointLights[MAX_POINT_LIGHTS];
uniform int wgf_numPointLights;
uniform BaseLight wgf_dirLightBase;
uniform vec3 wgf_dirLightDir;

uniform sampler2D tex0;
uniform vec3 wgf_eyePos;
uniform float specularIntensity;
uniform float specularPower;

varying vec2 texCoord;
varying vec3 varPos;
varying vec3 varNormal;

vec4 calcLightInternal(BaseLight light, vec3 lightDir, vec3 normal)
{
    vec4 ambientColor = vec4(light.color, 1.0) * light.intensity.x;
    float diffuseFactor = clamp(dot(normal, -lightDir), 0.0, 1.0);

    vec4 diffuseColor = vec4(0.0);
    vec4 specularColor = vec4(0.0);

    if(diffuseFactor > 0.0)
    {
        diffuseColor = vec4(light.color, 1.0) * light.intensity.y * diffuseFactor;
        vec3 vertexToEye = normalize(wgf_eyePos - varPos);
        vec3 reflectLight = normalize(reflect(lightDir, normal));
        float specularFactor = dot(vertexToEye, reflectLight);
        specularFactor = pow(specularFactor, specularPower);
        if(specularFactor > 0.0)
        {
            specularColor = vec4(light.color, 1.0) * specularIntensity * specularFactor;
        }
    } 

    return (ambientColor + diffuseColor + specularColor);
}

void main()
{
    vec3 normal = normalize(varNormal);
    vec4 totalLight = calcLightInternal(wgf_dirLightBase, normalize(wgf_dirLightDir), normal);

    for(int i = 0;i < MAX_POINT_LIGHTS;i++)
    {
        if(i < wgf_numPointLights)
        {
            vec3 lightDir = varPos - wgf_pointLights[i].position;
            float dist = length(lightDir);
            lightDir = lightDir / dist;

            vec4 color = calcLightInternal(wgf_pointLights[i].base, lightDir, normal);
            float atten = wgf_pointLights[i].attenuation.x + 
                          wgf_pointLights[i].attenuation.y * dist +
                          wgf_pointLights[i].attenuation.z * dist * dist;

            totalLight += color / atten;
        }
        else
        {
            break;
        }
    }

    gl_FragColor = vec4((vec4(0.7, 0.2, 0.2, 1.0) * totalLight).rgb, 1.0);// + vec4(texture2D(tex0, texCoord));
}