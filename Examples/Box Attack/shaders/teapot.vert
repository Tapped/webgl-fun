attribute vec3 position;
attribute vec3 normal;
attribute vec2 uvCoord;

uniform mat4 wgf_mMat;
uniform mat4 wgf_vpMat;
uniform mat3 wgf_normalMat;

varying vec2 texCoord;
varying vec3 varNormal;
varying vec3 varPos;

void main()
{
    gl_Position = wgf_vpMat * wgf_mMat * vec4(position.xyz, 1.0);
    varPos = vec3(wgf_mMat * vec4(position, 1.0));
    texCoord = uvCoord;
    varNormal = normalize(wgf_normalMat * normal);
}