#ifdef GL_ES
precision highp float;
#endif

#define MAX_RAYMARCH_ITER 32
#define PI 3.14159265359

uniform vec2 iResolution;
uniform float iGlobalTime;

const float precis = 0.1;

float sdCone(vec3 p, vec2 c)
{
    float q = length(p.xy);
    return dot(c,vec2(q,p.z));
}

float sdSphere( vec3 p, float s )
{
    return length(p)-s;
}

// Rotate a vector on the X axe.
// http://en.wikipedia.org/wiki/Rotation_(mathematics)
vec3 rotateX(vec3 p, float a) 
{
    float c = cos(a); float s = sin(a);
    return vec3(p.x, c * p.y - s * p.z, s * p.y + c * p.z);
}

// Rotate a vector on the Y axe.
// http://en.wikipedia.org/wiki/Rotation_(mathematics)
vec3 rotateY(vec3 p, float a) 
{
    float c = cos(a); float s = sin(a);
    return vec3(c * p.x + s * p.z, p.y, -s * p.x + c * p.z);
}

float mapScene(vec3 p)
{
    float dist = sdSphere(p + vec3(5.0 * cos(iGlobalTime + 1.5), 0.0, 5.0 * sin(iGlobalTime + 1.5)), 10.0);
    float disp = sin(iGlobalTime + 0.5 * p.x)*sin(iGlobalTime + 0.5 * p.y)*sin(iGlobalTime + 0.5 * p.z);
    return dist + disp;
}

vec3 rotateCamera(vec3 rayStart, vec3 rayDir, vec3 cameraTarget) 
{   
    // Create a vector from rayStart to cameraTarget.
    vec3 toTarget = normalize(cameraTarget - rayStart);
    
    // Find the angle on the Y axe by using atan.
    float angY = atan(toTarget.z, toTarget.x);
    rayDir = rotateY(rayDir, PI / 2.0 - angY);

    // Find the angle on the X axe by using atan.
    float angX = atan(toTarget.y, toTarget.z);
    rayDir = rotateX(rayDir, -angX);
    
    return rayDir;
}

void calcNormal(vec3 p, out vec3 n)
{
    vec3 eps = vec3(precis,0.0,0.0);
    n.x = mapScene(p+eps.xyy) - mapScene(p-eps.xyy);
    n.y = mapScene(p+eps.yxy) - mapScene(p-eps.yxy);
    n.z = mapScene(p+eps.yyx) - mapScene(p-eps.yyx);
    n = normalize(n);
}

void main(void)
{
    // Find out relative to view, where we are rendering.
    // We map it to [-1, 1] as the normal GL coordinate system.
    vec2 uv = vec2((gl_FragCoord.x - iResolution.x *.5) / iResolution.y, 
                   (gl_FragCoord.y - iResolution.y *.5) / iResolution.y);

    // Ray origin is the center point of the ray coordinate space.
    // We use it to find the direction relative to the screen.
    vec3 rayOrigin = vec3(0.0, 0.0, -2.0);

    // We start the ray at the origin
    // however we also move it -50 on the Z.
    // Think of it as the camera pos.
    vec3 rayStart = rayOrigin + vec3(0.0, 0.0, -80.0);
    
    // We rotate the camera to face a user defined direction.
    // Now we want to look straight down the negative Z axe.
    vec3 rayDir = rotateCamera(rayOrigin, normalize(vec3(uv,0.0) - rayOrigin), normalize(vec3(0.0, 0.0, -1.0)));

    // Current length of ray.
    float dist = 0.0;
    vec4 finalColor = vec4(0.0, 0.0, 0.0, 1.0);
    for(int i = 0;i < MAX_RAYMARCH_ITER;i++)
    {
        //sdSphere(rayStart + rayDir * dist, 10.0); 
        //float currentDist = sdCone(rayStart + rayDir * dist + vec3(0.0, -10.0, 0.0), normalize(vec2(0.8, -0.1)));
        float currentDist = mapScene(rayStart + rayDir * dist);
        // Check for intersection
        if(currentDist < precis)
        {
            vec3 normal;
            calcNormal(rayStart + rayDir * dist, normal);
            //We got intersection, so lets shade it.
            finalColor = vec4(sin(iGlobalTime*2.0) * 0.5 + 0.5, normal.y, 1.0 - normal.x, 1.0);
            break;
        }
        // Accumulate the distance, and effictively move the ray.
        dist += currentDist;
    }

    gl_FragColor = finalColor;
}