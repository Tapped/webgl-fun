/**
 * This class handles WebGL for you.
 * @class WebGLFun
 */
(function() {
    gl = null;// gl is a global var.

    var numLoadRequests = 0;
    var numComplete = 0;

    WebGLFun = function() 
    {
        // Function found here: 
        // http://stackoverflow.com/questions/6875625/does-javascript-provide-a-high-resolution-timer
        if (window.performance.now) 
        {
            console.log("Using high performance timer");
            this.getTimestamp = function() { return window.performance.now(); };
        } 
        else 
        {
            if (window.performance.webkitNow) 
            {
                console.log("Using webkit high performance timer");
                this.getTimestamp = function() { return window.performance.webkitNow(); };
            } 
            else 
            {
                console.log("Using low performance timer");
                this.getTimestamp = function() { return new Date().getTime(); };
            }
        }
    }

    /**
     * Setups WebGL for the user defined canvas.
     *
     * @param {Object} canvas The HTML element, which must be a canvas element.
     * @return Returns false if an error, else true.
     */
    WebGLFun.prototype.setup = function(canvas)
    {
        if(!window.WebGLRenderingContext)
        {
            alert("Your browser does not support WebGL.");
            return false;
        }

        try 
        {
            // Try to grab the standard context. If it fails, fallback to experimental.
            gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
        }
        catch(e) 
        {
            return false;
        }

        if(!gl)
        {
            alert("Failed to create a GL context.");
            return null;
        }

        this.width = canvas.width;
        this.height = canvas.height;

        gl.viewport(0, 0, this.width, this.height);

        gl.clearColor(0.3, 0.3, 0.3, 1.0);

        return true;
    }

    /**
     * Compiles a shader.
     * 
     * @param {String} src The shader source.
     * @param {Object} shaderType The type of shader, which maybe VERTEX_SHADER or FRAGMENT_SHADER.
     * @return Returns null if an error, else the shader object.
     */
    WebGLFun.prototype.compileShader = function(src, shaderType)
    {
        shader = gl.createShader (shaderType);
        gl.shaderSource(shader, src);
        gl.compileShader(shader);
        if (gl.getShaderParameter(shader, gl.COMPILE_STATUS) == 0)
        {
            alert("\n" + gl.getShaderInfoLog(shader));
        }

        return shader;
    }

    WebGLFun.prototype.loadFile = function(url, extraData, callback)
    {
        var request = new XMLHttpRequest();
        request.open('GET', url, true);

        // Hook the event that gets called as the request progresses
        request.onreadystatechange = function () 
        {
            // If the request is "DONE" (completed or failed)
            if (request.readyState == 4) 
            {
                // If we got HTTP status 200 (OK)
                if (request.status == 200) 
                {
                    callback(request.responseText, extraData);
                } else 
                { 
                    console.error("Couldn't download " + url);
                }
            }
        };

        request.send(null);
    }

    WebGLFun.prototype.loadFiles = function(urls, callback)
    {
        numLoadRequests += urls.length;
        var result = new Array;

        function callback_internal(str, urlIdx)
        {
            result[urlIdx] = str;
            ++numComplete;

            if(numComplete == numLoadRequests)
            {
                callback(result);
            }
        }

        for(var i = 0;i < numLoadRequests;++i)
        {
            this.loadFile(urls[i], i, callback_internal);
        }
    }
}());