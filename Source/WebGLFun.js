define(["Context", "Utils", "Math/MathExt", "libs/gl-matrix", 
		"Modules/Mesh", "Mesh/CubeMesh", "Scene/GameObject", "Scene/Scene",
		"Modules/Transformation", "Modules/Material", "Scene/Camera",
		"Math/vec3Ext", "Modules/Script", "Texture", "Math/AABBox",
		"Scene/BVHNode", "DebugDraw", "Modules/BoundingVolume", "Math/Plane",
		"Loaders/ObjParser", "Scene/PointLight", "Scene/DirectLight"],
function(context, utils, mathext, glMatrix, mesh, cubeMesh, gameObject, 
		scene, transform, material, camera, vec3Ext, script, texture,
		aabbox, bvhNode, debugDraw, boundingVolume, plane, objParser,
		pointLight, directLight)
{
	return {Context: context,
			Utils: utils, 
			Mesh: mesh,
			CubeMesh: cubeMesh,
			GameObject: gameObject,
			Scene: scene,
			Transformation: transform,
			Material: material,
			Camera: camera,
			Script: script,
			Texture: texture,
			AABBox: aabbox,
			BVHNode: bvhNode,
			DebugDraw: debugDraw,
			BoundingVolume: boundingVolume,
			Plane: plane,
			ObjParser: objParser,
			PointLight: pointLight,
			DirectLight: directLight,
			
			/**
			 * Wrappers for glMatrix library
			 */
			vec2: glMatrix.vec2, 
			vec3: glMatrix.vec3, 
			vec4: glMatrix.vec4,
			quat: glMatrix.quat,
			mat2: glMatrix.mat2,
			mat3: glMatrix.mat3,
			mat4: glMatrix.mat4,
			mat2d: glMatrix.mat2d
			};
});