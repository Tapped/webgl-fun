define(["Modules/Mesh"], function(Mesh)
{
	/**
	 * Used to parse models that are in the Obj model format. 
	 * @class ObjParser
	 * @constructor
	 */
	ObjParser = function()
	{
		/**
		 * Stores the vertices after the parse.
		 * @property vertices
		 * @type Array.Number
		 */
		this.vertices = new Array();

		/**
		 * Stores the texture coordinates after parsing obj data.
		 * @property textureCoords
		 * @type Array.Number
		 */
		this.textureCoords = new Array;

		/**
		 * Stores the normals after parsing obj data.
		 * @property normals
		 * @type Array.Number
		 */
		this.normals = new Array;

		/**
		 * Stores the indices for the mesh.
		 * @property indices
		 * @type Array.Number
		 */
		this.indices = new Array;

		/**
		 * Stores the texture coordinates in raw temporary format.
		 * @private
		 * @property _textureCoords
		 * @type Array.Number
		 */
		this._textureCoords = new Array;

		/**
		 * Stores the normals in raw temporary format.
		 * @private
		 * @property _normals
		 * @type Array.Number
		 */
		this._normals = new Array;

		/**
		 * Stores the vertices in raw temporary format.
		 * @private
		 * @property _vertices
		 * @type Array.Number
		 */
		 this._vertices = new Array;
	}

	/**
	 * Parses a string storing obj data.
	 * @param {String} data The data to be parsed.
	 */
	ObjParser.prototype.parseData = function(data)
	{
		var lines = data.split("\n");
		for(var i = 0;i < lines.length;i++)
		{
			var line = lines[i];
			var statements = line.split(" ");
			if(statements[0] == "v")
			{
				ObjParser._getData(statements, this._vertices);
			}
			else if(statements[0] == "vt")
			{
				ObjParser._getData(statements, this._textureCoords);
			}
			else if(statements[0] == "vn")
			{
				ObjParser._getData(statements, this._normals);
			}
			else if(statements[0] == "f")
			{
				var faceIndices = new Array();
				ObjParser._getData(statements, faceIndices, true);

				for(var j = 0;j < faceIndices.length;j++)
				{
					var elements = faceIndices[j].split("/");

					var vertIndex = parseInt(elements[0]);
					if(!isNaN(vertIndex))
					{
						this.vertices.push(this._vertices[(vertIndex-1)*3]);
						this.vertices.push(this._vertices[(vertIndex-1)*3 + 1]);
						this.vertices.push(this._vertices[(vertIndex-1)*3 + 2]);
					}

					if(this._textureCoords.length > 0)
					{
						var texIndex = vertIndex;
						if(elements.length == 2)
						{
							texIndex = parseInt(elements[1]);
						}
						
						if(!isNaN(texIndex))
						{
							this.textureCoords.push(this._textureCoords[(texIndex-1)*2]);
							this.textureCoords.push(this._textureCoords[(texIndex-1)*2 + 1]);
						}
					}
					
					if(this._normals.length > 0)
					{
						var normalIndex = vertIndex;
						if(elements.length == 3)
						{
							normalIndex = parseInt(elements[2]);
						}
						
						if(!isNaN(normalIndex))
						{
							this.normals.push(this._normals[(normalIndex-1)*3]);
							this.normals.push(this._normals[(normalIndex-1)*3 + 1]);
							this.normals.push(this._normals[(normalIndex-1)*3 + 2]);
						}
					}
				}
			}
		}
	};

	/**
	 * Converts the loaded obj file to renderable mesh.
	 * @method toMesh
	 * @return {Mesh} Returns the renderable mesh.
	 */
	ObjParser.prototype.toMesh = function()
	{
		var mesh = new Mesh;
		var normals = this.normals.length > 0 ? this.normals : null;
		var texCoords = this.textureCoords.length > 0 ? this.textureCoords : null;
		mesh.setVertices(this.vertices, normals, texCoords);
		return mesh;
	};

	/**
	 * Returns the data found in a statement.
	 * @private
	 * @method _getData
	 */
	ObjParser._getData = function(knots, out, parseRaw)
	{
		var counter = 0;
		for(var i = 1;i < knots.length;i++)
		{
			if(knots[i] != "")
			{
				++counter;
				if(parseRaw)
				{
					out.push(knots[i]);
				}
				else
				{
					out.push(parseFloat(knots[i]));
				}
			}
		}

		return counter;
	};

	return ObjParser;
});