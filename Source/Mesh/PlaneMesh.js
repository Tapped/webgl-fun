define(["Modules/Mesh"], function(Mesh)
{
	/**
	 * Plane mesh, is used to draw planes.
	 * 
	 * It saves memory by using a shared Mesh class across all instances.
	 * So to change the size etc. Use the transformation class.
	 * @class PlaneMesh
	 * @constructor
	 */
	PlaneMesh = function()
	{
		if(!PlaneMesh.staticMesh)
		{
			PlaneMesh.staticMesh = new Mesh();
			PlaneMesh.staticMesh.setVertices(PlaneMesh.vertices, PlaneMesh.normals, PlaneMesh.texCoords);
			
			// We let the GC do the shitty work
			PlaneMesh.vertices = null;
			PlaneMesh.normals = null;
			PlaneMesh.texCoords = null;
		}

		return PlaneMesh.staticMesh;
	};

	PlaneMesh.staticMesh = null;

	PlaneMesh.vertices = [-1.0, -1.0, 0.0, 
						   1.0, -1.0, 0.0,
						   1.0, 1.0, 0.0,

						   1.0, 1.0, 0.0,
						   -1.0, 1.0, 0.0,
						   -1.0, -1.0, 0.0];

	PlaneMesh.normals = [0, 0, -1,
						 0, 0, -1,
						 0, 0, -1,

						 0, 0, -1,
						 0, 0, -1,
						 0, 0, -1];

	PlaneMesh.texCoords = [0, 0,
						   1, 0,
						   1, 1,

						   1, 1,
						   0, 1,
						   0, 0];
    return PlaneMesh;						   
});