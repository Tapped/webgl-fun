/**
 * A series of different types of meshes, and utilities for that.
 *
 * @module Mesh
 */
define(["Modules/Mesh"], function(Mesh)
{
    var vertices = [
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
    
        1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0,
        1.0, 1.0, -1.0,
        -1.0, 1.0, -1.0,

        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,

        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, -1.0,

        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        -1.0, 1.0, -1.0,
        1.0, 1.0, -1.0,

        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0
    ];

    var normals = [
        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, -1.0,
        0.0, 0.0, -1.0,
        0.0, 0.0, -1.0,
        0.0, 0.0, -1.0,

        -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,

        1.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        1.0, 0.0, 0.0,

        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,

        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0
    ];

    var texCoords = [
        0, 0,
        1, 0,
        0, 1,
        1, 1,

        0, 0,
        1, 0,
        0, 1,
        1, 1,

        0, 0,
        1, 0,
        0, 1,
        1, 1,

        0, 0,
        1, 0,
        0, 1,
        1, 1,

        0, 0,
        1, 0,
        0, 1,
        1, 1,

        0, 0,
        1, 0,
        0, 1,
        1, 1
    ];

    var indices = [
        0, 1 ,2,
        1, 3, 2,

        4, 5, 6,
        5, 7, 6,

        8, 9, 10,
        9, 11, 10,

        12, 13, 14,
        13, 15, 14,

        16, 17, 18,
        17, 19, 18,

        20, 21, 22,
        21, 23, 22
    ];

    /**
     * Predefined mesh that represents a cube.
     * @static
     * @class CubeMesh
     * @constructor
     * @return {Mesh} Returns the mesh.
     */
    CubeMesh = function()
    {
        if(!CubeMesh.staticMesh)
        {
            CubeMesh.staticMesh = new Mesh;
            CubeMesh.staticMesh.setVertices(vertices, normals, texCoords);
            CubeMesh.staticMesh.setIndices(indices);
        }
        return CubeMesh.staticMesh;
    }

    CubeMesh.staticMesh = null;

    /**
     * Returns the vertices of a cube mesh.
     * @method getVertices
     * @return {Array.Number} Returns the vertices.
     */
    CubeMesh.getVertices = function()
    {
        return vertices;
    };

    return CubeMesh;
});