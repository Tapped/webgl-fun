/**
 * Root module
 *
 * @module Root
 * @main Root
 */
define(function() {
    gl = null;// gl is a global var.
    glExt = null;// glExt is global var
    ctx = null;// ctx is also a global var

    /**
     * This class handles the WebGL context for you.
     * @class Context
     * @constructor
     */
    Context = function() 
    {
        /**
         * A high performance time function.
         * @method getTimestamp
         * @return {Number} Returns the time in milliseconds, with microseconds in the fractional part.
         */       
        if (window.performance.now)
        {
            Context.getTimestamp = function() { return window.performance.now(); };
        } 
        else 
        {
            if (window.performance.webkitNow) 
            {
                Context.getTimestamp = function() { return window.performance.webkitNow(); };
            } 
            else 
            {
                Context.getTimestamp = function() { return new Date().getTime(); };
            }
        }

        ctx = this;
    }

    /**
     * Setups WebGL for the user defined canvas.
     *
     * @method setup
     * @param {Canvas} canvas The HTML element, which must be a canvas element.
     * @return {Bool} Returns false if an error, else true.
     */
    Context.prototype.setup = function(canvas)
    {
        if(!window.WebGLRenderingContext)
        {
            alert("Your browser does not support WebGL.");
            return false;
        }

        try 
        {
            // Try to grab the standard context. If it fails, fallback to experimental.
            gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
        }
        catch(e) 
        {
            return false;
        }

        if(!gl)
        {
            alert("Failed to create a GL context.");
            return null;
        }

        glExt = gl.getExtension("WEBKIT_WEBGL_depth_texture") || gl.getExtension("MOZ_WEBGL_depth_texture");
        gl.getExtension("OES_texture_float");

        this.width = canvas.width;
        this.height = canvas.height;

        gl.viewport(0, 0, this.width, this.height);
        gl.clearColor(0.3, 0.3, 0.3, 1.0);

        return true;
    }

    return Context;
});