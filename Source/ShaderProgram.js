define(function()
{
	/**
	 * Shader program wraps the GLSL Progam.
	 * You got more features with ShaderProgram.
	 * Use {{#crossLink "Utils/createShaderProgram:method"}}{{/crossLink}} to create a ShaderProgram.
	 *
	 * @class ShaderProgram
	 * @constructor
	 */
	ShaderProgram = function()
	{
	}

	/**
	 * Creates a ShaderProgram by using a GL Program.
	 *
	 * @private
	 * @method _create
	 * @param {Program} programObject GL program object.
	 * @return {ShaderProgram} Returns a ShaderProgram.
	 */
	ShaderProgram._create = function(programObject) 
	{
		var out = new ShaderProgram;

		/**
		 * GL Program Object
		 * @private
		 * @property _programObject
		 * @type Program
		 */
		out._programObject = programObject;

		/**
		 * Uniform locations
		 * @private
		 * @property _uniformLocations
		 * @type [String] = int
		 */
		out._uniformLocations = new Object;

		return out;
	};

	/**
	 * Makes this program as current program for rendering.
	 *
	 * @method use
	 */
	ShaderProgram.prototype.use = function() 
	{
		gl.useProgram(this._programObject);
	};

	/**
	 * Returns the uniform location of a uniform.
	 * @method getUniformLocation
	 * @param {String} name Uniform name.
	 */
	ShaderProgram.prototype.getUniformLocation = function(name) 
	{
		if(!(name in this._uniformLocations))
		{
			this._uniformLocations[name] = gl.getUniformLocation(this._programObject, name);
		}

		return this._uniformLocations[name];
	};
});