define(function()
{	
	/**
	 * Texture class is used to color your models using an image file as source.
	 * Textures can also be used for more advanced stuff, for example as a framebuffer target etc.
	 *
	 * @class Texture
	 * @constructor
	 * Creates a texture instance, from an image file.
	 * @param {String} fileName Path to an image file. Supported images equals to what the browser support.
	 */
	Texture = function(fileName) 
	{
		/**
		 * GL texture object.
		 *
		 * @property textureObj
		 * @type gl.TextureObject
		 */
		this.textureObj = null;

		/**
		 * Specifies if the image that belongs to the texture has been loaded.
		 * @property hasLoaded
		 * @type Bool
		 * @default false
		 */
		this.hasLoaded = false;

		/**
		 * Texture unit, that our texture is bound to.
		 * @private
		 * @property _textureUnit
		 */
		this._textureUnit = null;

		if(fileName)
		{
			this.loadImage(fileName);
		}
	}

	/**
	 * Loads an image, and creates the GL texture object for that.
	 *
	 * NOTE: This function deletes the old texture object and data,
	 * 		 if a texture object already exist.
	 * @method loadImage
	 * @param {String} fileName Path to the image file.
	 */
	Texture.prototype.loadImage = function(fileName) 
	{
		// Free texture object, if we already have an object.
		if(this.textureObj)
		{
			gl.deleteTexture(texture.textureObj);
		}
		this.textureObj = gl.createTexture();

		var image = new Image();
		image.src = fileName;

		var thisTexture = this;
		image.onload = function()
		{
			gl.bindTexture(gl.TEXTURE_2D, thisTexture.textureObj);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		  	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
			gl.generateMipmap(gl.TEXTURE_2D);
			gl.bindTexture(gl.TEXTURE_2D, null);

			thisTexture.hasLoaded = true;
		};
	};

	/**
	 * Binds the texture as the active texture.
	 * @method bind
	 * @param {GLenum} textureUnit The texure unit to bound to. Optional, default gl.TEXTURE0.
	 */
	Texture.prototype.bind = function(textureUnit)
	{
		if(!textureUnit)
		{
			textureUnit = gl.TEXTURE0;
		}
		this._textureUnit = textureUnit;
		gl.activeTexture(textureUnit);
		gl.bindTexture(gl.TEXTURE_2D, this.textureObj);
	};

	/**
	 * Unbinds the texture from being the active texture.
	 * @method unbind
	 */
	Texture.prototype.unbind = function() 
	{
		if(this._textureUnit)
		{
			gl.activeTexture(this._textureUnit);
			gl.bindTexture(gl.TEXTURE_2D, null);
			this._textureUnit = null;
		}
	};

	return Texture;
});