define(["libs/gl-matrix"], function(glm, GameObject)
{	
	/**
	 * Base light stores basic information 
	 * that are needed for light calculations.
	 * @class BaseLight
	 */
	BaseLight = function()
	{
		/**
		 * The color of the light.
		 * @property color
		 * @type vec3
		 */
		this.color = glm.vec3.create();

		/**
		 * The ambient intensity of the light.
		 * @property ambientIntensity
		 * @type Number
		 */
		this.ambientIntensity = 0;

		/**
		 * The diffuse intensity of the light.
		 * @property diffuseIntensity
		 * @type Number
		 */
		this.diffuseIntensity = 0;

		/**
		 * Used to differentiate between the GameObjects type.
		 * @property isLight
		 * @type Bool
		 */
		this.isLight = true;
	}

	return BaseLight;
});