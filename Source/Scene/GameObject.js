/**
 * A series of Scene related classes.
 * @module Scene
 */
define(function()
{
	/**
 	 * A GameObject is an empty object that can be customized by adding GameObject modules.
 	 * The GameObject is used to represent something in your game.
 	 *
 	 * @class GameObject
 	 * @constructor
 	 * @param {String} name The name of the GameObject.
 	 */
	GameObject = function(name)
	{
		/**
		 * Name of GameObject instance.
		 *
		 * @property name
		 * @type String
		 */
		this.name = name;

		/**
		 * True if the game object should NOT be rendered.
		 *
		 * NOTE: The Game Object may only be rendered if it can.
		 *
		 * @property isHidden
		 * @type Bool
		 * @default false
		 */
		this.isHidden = false;

		/**
		 * Stores the modules.
		 *
		 * @private
		 * @property _modules
		 */
		this._modules = new Object();

		/**
		 * A Script is a special case of a GameObject module. 
		 * So we store a pointer to the scripts, so that we 
		 * can easily call the scripts.
		 *
		 * @private
		 * @property _scripts
		 */
		this._scripts = new Object();

		/**
		 * A readonly property, that is true if the object is visible this frame.
		 * @property isVisible
		 * @type Bool
		 * @default false
		 */
		this.isVisible = false;

		/** 
		 * Stores events that the modules can send or subscribe for.
		 * @private
		 * @property _events 
		 */
		this._events = new Object();
		this._events.subscribers = new Array();
	}

	/**
	 * Adds a module to the current GameObject.
	 *
	 * @method addModule
	 * @param {Module} mod The module, which becomes part of the object.
	 * @return {Module} Returns the added module.
	 */
	GameObject.prototype.addModule = function(mod) 
	{
		this._modules[mod.name] = mod;
		mod.gameObject = this;

		if(mod.start)
		{
			mod.start();
		}

		if(mod.isScript)
		{
			this._scripts[mod.name] = mod;
		}

		return mod;
	};

	/**
	 * Sends an event to all the subscribers.
	 * @method addEvent
	 * @param {String} name The name of the event.
	 * @param {Module} fromModule The module that the event is sent from.
	 * @parm {Object} extraData Optional extra data, sent with the event.
	 */
	GameObject.prototype.sendEvent = function(name, fromModule, extraData)
	{
		for(var i = 0;i < this._events[name].subscribers.length;i++)
		{
			var subscriber = this._events[name].subscribers[i];
			subscriber.callback.call(subscriber.module, fromModule, extraData);
		}
	};

	/**
	 * Subscribes a module to a specific event.
	 * @method subscribeEvent
	 * @param {String} eventName Name of event.
	 * @param {Module} module The module that subscribes to the event.
	 * @param {Function} callback The function called when the event is received.
	 */
	GameObject.prototype.subscribeEvent = function(eventName, module, callback)
	{
		this._events[eventName].subscribers.push({module: module, callback: callback});
	};

	/**
	 * Updates the GameObject.
	 *
	 * This function is called from the scene.
	 * @method update
	 */
	GameObject.prototype.update = function()
	{
		for(var key in this._scripts)
		{
			this._scripts[key].update();
		}

		for(var key in this._modules)
		{
			if(this._modules[key].update)
			{
				this._modules[key].update();
			}
		}
	}

	/**
	 * Returns a module from the current GameObject.
	 *
	 * @method getModule
	 * @param {String} modName The module name.
	 * @return {Module} The module if we found one, else null.
	 */
	GameObject.prototype.getModule = function(modName) 
	{
		return this._modules[modName];
	};

	/**
	 * Removes a module from the current GameObject.
	 *
	 * @method removeModule
	 * @param {String} modName The module name of the module, which gonna be removed.
	 */
	GameObject.prototype.removeModule = function(modName)
	{
		if(this._modules[objName].isScript)
		{
			this._modules[objName].exit();
		}

        // We don't need to check if modName exist,
        // since we let delete do the work.
        delete this._modules[objName];
	};

	return GameObject;
});