/**
 * A series of Scene related classes.
 * @module Scene
 */
define([], function()
{

	/**
	 * Shadow map is used create shadows, using FBO and depth rendering.
	 * @class ShadowMap
	 * @constructor
	 */
	ShadowMap = function()
	{
		/**
		 * The depth texture that stores the depth information.
		 * @private
		 * @property _depthTexture
		 * @type Texture
		 */
		this._depthTexture = new Texture();

		/**
		 * Our frame buffer used of offscreen rendering.
		 * @private
		 * @property _frameBuffer
		 * @type gl.FRAMEBUFFER
		 */
		this._frameBuffer;
	};

	/**
	 * Initialize the shadow map, which creates a depth texture and framebuffer.
	 * @method init
	 * @param {Number} width The width of the shadow map, should be the canvas size.
	 * @param {Number} height The height of the shadow map, should be the canvas size.
	 * @return {Boolean} Returns true if no errors.
	 */
	ShadowMap.prototype.init = function(width, height) 
	{
		// Create the depth texture
		var texObj = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, texObj);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT, 
					  width, height, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_SHORT, null);
		this._depthTexture = texObj;

		// Create a framebuffer instance, and bind it to the depth texture.
		this._frameBuffer = gl.createFramebuffer();
		gl.bindFramebuffer(gl.FRAMEBUFFER, this._frameBuffer);
		this._frameBuffer.width = width;
		this._frameBuffer.height = height;
		gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, texObj, 0);

		var status = gl.checkFramebufferStatus(gl.FRAMEBUFFER);
		gl.bindFramebuffer(gl.FRAMEBUFFER, null);
		if(status != gl.FRAMEBUFFER_COMPLETE)
		{
			console.log("Failed to create a framebuffer!");
			return false;
		}

		return true;
	};

	/**
	 * Bind the shadow map for writing.
	 * @method bindForWriting
	 */
	ShadowMap.prototype.bindForWriting = function()
	{
		gl.bindFramebuffer(gl.FRAMEBUFFER, this._frameBuffer);
	};

	/**
	 * Bind the shadow map for reading.
	 * @method bindForReading
	 * @param {GLenum} textureUnit The texture unit to bind the shadow map to.
	 */
	ShadowMap.prototype.bindForReading = function(textureUnit)
	{
		gl.activeTexture(textureUnit);
		gl.bindTexture(gl.TEXTURE_2D, this._depthTexture);
	};

	return ShadowMap;
});