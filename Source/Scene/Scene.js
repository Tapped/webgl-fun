/**
 * A series of Scene related classes.
 * @module Scene
 */
define(["libs/gl-matrix", "Utils", "Scene/BVHNode", 
        "Math/AABBox", "Scene/GameObject", "Scene/LightManager", "Scene/ShadowMap", "Mesh/PlaneMesh"], 
        function(glm, Utils, BVHNode, AABBox, GameObject, LightManager, ShadowMap, PlaneMesh)
{
    var debugVertShader = 
     "attribute vec3 position;" +
     "uniform mat4 wgf_mMat;" +
     "uniform mat4 wgf_vpMat;" +
     "void main()" + 
     "{" +
        "gl_Position = wgf_vpMat * wgf_mMat * vec4(position, 1.0);" +
        "gl_PointSize = 2.0;" + 
     "}";

    var debugFragShader =
     "precision highp float;" + 
     "uniform vec4 color;" +
     "void main()" +
     "{" +
        "gl_FragColor = color;" +
     "}";

    /**
     * Scene handles what's in game at the moment.
     *
     * The scene is the root class, used during rendering.
     * 
     * Add objects that you want to access into the current scene.
     * Including objects that you want to render.
     * 
     * TODO: Sort objects in the scene by their ShaderProgram
     * to optimize rendering.
     *
     * @class Scene
     * @constructor
     */ 
    Scene = function()
    {
        /**
         * Stores the objects that we have in the scene.
         *
         * @private
         * @property _objects
         */
        this._objects = new Object();

        /**
         * Points to the current camera.
         * @property currentCamera
         * @type Camera
         */
        this.currentCamera = null;

        /**
         * Stores the debug objects.
         * @private
         * @property _debugObjects
         * @type DebugDraw
         */
        this._debugObjects = new Array();

        /**
         * Stores the current visible objects.
         * @private
         * @property _visibleObjects
         * @type Array
         */
        this._visibleObjects = new Array();

        /**
         * Has the current root node of the BVH.
         * @property rootNode
         * @type BVHNode
         */
        this.rootNode = null;

        /**
         * Light manager used for the management of lights.
         * @property lightManager
         * @type LightManager
         */
        this.lightManager = new LightManager;

        /**
         * Shadow map used for creating shadows for directional light and spot lights.
         * @private
         * @property shadowMap
         * @type ShadowMap
         */
        this.shadowMap = new ShadowMap();
        this.shadowMap.init(ctx.width, ctx.height);
    }

    /**
     * Adds an object to the scene.
     *
     * @method addObject
     * @param {GameObject} obj The object that becomes added to the scene.
     */
    Scene.prototype.addObject = function(obj) 
    {
        this._objects[obj.name] = obj;

        if(obj.isCamera && !this.currentCamera)
        {
            this.currentCamera = obj;
        }
        
        var light = obj.getModule("PointLight") || obj.getModule("DirectLight");
        if(light)
        {
            this.lightManager.addLight(obj);
        }
    };

    /**
     * Adds an debug object to the scene.
     *
     * Debug objects are only stored one frame.
     * @method addDebugObject
     * @param {DebugDraw} obj The debug object. 
     */
    Scene.prototype.addDebugObject = function(obj)
    {
        this._debugObjects.push(obj);
        if(!this._debugProgram)
        {
            var vertShader = Utils.compileShader(debugVertShader, gl.VERTEX_SHADER);
            var fragShader = Utils.compileShader(debugFragShader, gl.FRAGMENT_SHADER);

            this._debugProgram = Utils.createShaderProgram([vertShader, fragShader], ["position"]);
            this._debugMaterial = new Material(this._debugProgram);
        }
    }

    /**
     * Removes an object from the scene.
     *
     * @method removeObject
     * @param {String} objName The name of the object to delete.
     */
    Scene.prototype.removeObject = function(objName)
    {
        // We don't need to check if objName exist,
        // since we let delete do the work.
        delete this._objects[objName];
    };

    var testVertShader = 
                         "attribute vec3 position;" +
                         "attribute vec3 normal;" +
                         "attribute vec2 texCoord;" +

                         "varying vec2 varTexCoord;" +

                         "void main(){" +
                         "gl_Position = vec4(position.xy, 0.0, 1.0);" +
                         "varTexCoord = position.xy * 0.5 + 0.5;" +
                         "}";
    var testFragShader = 
                         "precision highp float;" +
                         "varying vec2 varTexCoord;" +
                         "uniform sampler2D tex0;" +
                         "void main(){" +
                         "gl_FragColor = vec4(texture2D(tex0, varTexCoord).r, 0.0, 0.0, 1.0);}";

    /**
     * Render the scene, which would efficiently render all objects that has a mesh.
     * 
     * It will also simulate lights and shadows if specified.
     * @method render
     */
    Scene.prototype.render = function()
    {
        this.lightManager.reset();
        this._visibleObjects.length = 0;

        var viewProjMat;
        var viewMat;
        if(this.currentCamera)
        {
            viewProjMat = this.currentCamera.getViewProjMatrix();
            viewMat = this.currentCamera.getViewMatrix();
        }
        else
        {
            viewProjMat = glm.mat4.create();
            viewMat = glm.mat4.create();
        }

        if(this.rootNode)
        {
            for(var i in this._objects)
            {
                this._objects[i].isVisible = false;
            }

            this.rootNode.findObjectsInFrustum(this.currentCamera.planes);
        }

        for(var i in this._objects)
        {
            var currentObj = this._objects[i];
            currentObj.update();

            var volume = currentObj.getModule("BoundingVolume");
            if(volume)
            {
                if(!currentObj.bvhNode)
                {
                    if(!this.rootNode)
                    {
                        this.rootNode = new BVHNode(null, volume.boundingVolume, currentObj);
                        currentObj.bvhNode = this.rootNode;
                    }
                    else
                    {
                        var result = this.rootNode.insert(currentObj, volume.boundingVolume);
                        if(result.oldNode)
                            this._objects[result.oldNode.gameObject.name].bvhNode = result.oldNode;
                        
                        currentObj.bvhNode = result.newNode;
                    }
                }

                if(volume._isDirty)
                {
                    if(currentObj.bvhNode)
                    {
                        var oldNode = currentObj.bvhNode.destroyMe();
                        if(oldNode && oldNode.gameObject)
                        {
                            this._objects[oldNode.gameObject.name].bvhNode = oldNode;
                        }
                    }
                    
                    if(!this.rootNode)
                    {
                        this.rootNode = new BVHNode(null, volume.boundingVolume, currentObj);
                        currentObj.bvhNode = this.rootNode;
                    }
                    else
                    {
                        var result = this.rootNode.insert(currentObj, volume.boundingVolume);
                        if(result.oldNode)
                            this._objects[result.oldNode.gameObject.name].bvhNode = result.oldNode;
                        
                        currentObj.bvhNode = result.newNode;
                    }
                }
            }
            
            if(currentObj.isVisible)
            {
                this._visibleObjects.push(currentObj);
            }
        }

        if(!this._debugProgram)
        {
            var vertShader = Utils.compileShader(testVertShader, gl.VERTEX_SHADER);
            var fragShader = Utils.compileShader(testFragShader, gl.FRAGMENT_SHADER);

            this._debugProgram = Utils.createShaderProgram([vertShader, fragShader], ["position", "normal", "texCoord"]);
            this._debugMaterial = new Material(this._debugProgram);
        }

        /*this.shadowPass(viewMat, viewProjMat);

        this.shadowMap.bindForReading(gl.TEXTURE0);

        this._debugMaterial.use();
        var planeMesh = new PlaneMesh();
        planeMesh.render();*/

        for(var i = 0;i < this._visibleObjects.length;i++)
        {
            this.drawObject(this._visibleObjects[i], viewProjMat, viewMat, true);
        }

        if(this._debugObjects.length > 0)
        {   
            gl.enable(gl.BLEND);
            gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
            this._debugMaterial.setUniform("wgf_vpMat", viewProjMat);
            
            // Lets render the debug objects.
            for(var i = 0;i < this._debugObjects.length;i++)
            {
                var obj = this._debugObjects[i];
                this._debugMaterial.setUniform("wgf_mMat", obj.modelMatrix);
                this._debugMaterial.setUniform("color", obj.color);
                this._debugMaterial.use();

                var oldRenderMode = obj.mesh.renderMode;
                obj.mesh.renderMode = obj.type;
                obj.mesh.render();

                obj.mesh.renderMode = oldRenderMode;
            }
            gl.disable(gl.BLEND);
        }

        this._debugObjects = new Array();
    }

    /**
     * Shadow pass pre-renders the scene to a depth texture 
     * used for lookup during the real rendering.
     * @method shadowPass
     */
    Scene.prototype.shadowPass = function(viewMat, viewProjMat)
    {   
        gl.bindTexture(gl.TEXTURE_2D, null);
        this.shadowMap.bindForWriting();

        gl.clear(gl.DEPTH_BUFFER_BIT);

        //for(var i = 0;i < this._visibleObjects.length;i++)
        //{
        this.drawObject(this._objects["Teapot"], viewProjMat, viewMat, false);
        //}

        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    };

    /**
     * Draws an object.
     * @method drawObject
     * @param {GameObject} object The object to be drawn.
     * @param {mat4} viewProjMat The view->proj matrix.
     * @param {mat4} viewMat The view matrix.
     * @param {Boolean} [withLight=true] If true the light is rendered with light, else not.
     */
    Scene.prototype.drawObject = function(object, viewProjMat, viewMat, withLight)
    {
        // Set withLight to default value, which is true.
        withLight = withLight === undefined ? true : withLight;

        var materialModule = object.getModule("Material");
        var meshModule = object.getModule("Mesh");
        if(!object.isHidden && materialModule && meshModule)
        {   
            var transformModule = object.getModule("Transformation");
            var modelMat = transformModule.getModelMatrix();

            if(materialModule.hasUniform("wgf_mvpMat"))
            {
                var mvpMat;
                if(transformModule)
                {
                    mvpMat = glm.mat4.mul(glm.mat4.create(), viewProjMat, modelMat);
                }
                else
                {
                    mvpMat = viewProjMat;
                }

                materialModule.setUniform("wgf_mvpMat", mvpMat);
            }

            if(materialModule.hasUniform("wgf_mMat"))
            {
                materialModule.setUniform("wgf_mMat", modelMat);
            }

            if(materialModule.hasUniform("wgf_vMat"))
            {
                materialModule.setUniform("wgf_vMat", viewMat);
            }

            if(materialModule.hasUniform("wgf_vmMat"))
            {
                var vmMat = glm.mat4.mul(glm.mat4.create(), viewMat, modelMat);
                if(materialModule.hasUniform("wgf_vmMat"))
                {
                    materialModule.setUniform("wgf_vmMat", vmMat);
                }
            }

            if(materialModule.hasUniform("wgf_normalMat"))
            {
                var inverseVM = glm.mat4.invert(glm.mat4.create(), modelMat);
                inverseVM = glm.mat3.fromMat4(glm.mat3.create(), inverseVM);
                glm.mat3.transpose(inverseVM, inverseVM);
                materialModule.setUniform("wgf_normalMat", inverseVM);
            }

            if(materialModule.hasUniform("wgf_vpMat"))
            {
                materialModule.setUniform("wgf_vpMat", viewProjMat);
            }

            if(materialModule.hasUniform("wgf_eyePos"))
            {
                materialModule.setUniform("wgf_eyePos", [viewMat[12], viewMat[13], viewMat[14]]);
            }

            if(withLight)
            {
                this.lightManager.useOn(materialModule);
            }

            materialModule.use();

            meshModule.render();
        }
    };

    return Scene;
});