/**
 * A series of Scene related classes.
 * @module Scene
 */
define(["Scene/GameObject", "libs/gl-matrix", "Modules/Transformation", "Math/Plane", "Math/vec3Ext"], 
		function(GameObject, glm, Transformation, Plane, vec3Ext)
{
	/**
	 * Camera is a predefined GameObject used for camera related stuff.
	 * 
	 * Every scene should have a camera.
	 * You can have multiple cameras in the scene, but then you have to specify which camera to use.
	 *
	 * @class Camera
	 * @constructor
	 * @extends GameObject
	 * @param {String} name Name of the camera.
	 */
	Camera = function(name)
	{
		/**
		 * Name of Camera instance.
		 *
		 * @property name
		 * @type String
		 */
		this.name = name;

		/** 
		 * Projection matrix.
		 *
		 * @property projMat
		 * @type Mat4
		 */
		this.projMat = glm.mat4.create();

		/** 
		 * A switch so that we know that this is a special case of a GameObject.
		 *
		 * @property isCamera
		 * @type Bool
		 * @default true
		 */
		this.isCamera = true;

		/**
		 * Override the isHidden and set it to true.
		 * Since the camera can't be rendered.
		 *
		 * @property isHidden
		 * @type Bool
		 * @default true
		 */
		this.isHidden = true;

		/**
		 * Stores the frustum planes for this camera.
		 * It is updated each time 
		 * {{#crossLink "Camera/getViewProjMatrix:method"}}{{/crossLink}} is called,
		 * or when calling {{#crossLink "Camera/updateFrustumPlanes:method"}}{{/crossLink}}
		 * @property planes
		 * @type Array(6).Plane
		 */
		this.planes = new Array(new Plane, new Plane, new Plane, 
							    new Plane, new Plane, new Plane);

		/**
		 * Stores the latest view->projection matrix.
		 * @private
		 * @property _viewProjMat
		 * @type mat4
		 */
		this._viewProjMat = null;

		/**
		 * Camera uses the transformation module.
		 * @property Transformation
		 * @type Module
		 */
		this.addModule(new Transformation);
	}

	Camera.prototype = new GameObject("Camera");

	Camera.PlaneEnum = {
		NEAR: 0,
		FAR: 1,
		BOTTOM: 2,
		TOP: 3,
		LEFT: 4,
		RIGHT: 5
	};

	/**
	 * Updates the frustum planes.
	 * @method updateFrustumPlanes
	 */
	Camera.prototype.updateFrustumPlanes = function()
	{
		var m = this._viewProjMat;
		var Plane = Camera.PlaneEnum;

		this.planes[Plane.NEAR].setCoefficients(m[3]+ m[2],
									   			m[7]+ m[6],
									   			m[11]+m[10],
									   			m[15]+m[14]);
		
		this.planes[Plane.FAR].setCoefficients(m[3]- m[2],
											   m[7]- m[6],
											   m[11]-m[10],
											   m[15]-m[14]);
		
		this.planes[Plane.BOTTOM].setCoefficients(m[3]+ m[1],
											   	  m[7]+ m[5],
												  m[11]+m[9],
												  m[15]+m[13]);
		
		this.planes[Plane.TOP].setCoefficients(m[3]- m[1],
											   m[7]- m[5],
											   m[11]-m[9],
											   m[15]-m[13]);

		this.planes[Plane.LEFT].setCoefficients(m[3]+ m[0],
											    m[7]+ m[4],
											    m[11]+m[8],
											    m[15]+m[12]);

		this.planes[Plane.RIGHT].setCoefficients(m[3]- m[0],
											     m[7]- m[4],
												 m[11]-m[8],
												 m[15]-m[12]);
	};

	/**
	 * Sets the projection matrix to perspective.
	 * @method makePerspective
	 * @param {Number} Field of view
	 * @param {Number} Aspect ratio
	 * @param {Number} Z-near plane
	 * @param {Number} Z-far plane
	 */
	Camera.prototype.makePerspective = function(fov, aspect, near, far)
	{
		glm.mat4.perspective(this.projMat, fov, aspect, near, far);
	}

	/**
	 * Returns the view projection matrix.
	 *
	 * @method getViewProjMatrix
	 * @return {mat4} Returns the view->projection matrix.
	 */
	Camera.prototype.getViewProjMatrix = function()
	{
		var transformModule = this.getModule("Transformation");
		this._viewMat = glm.mat4.invert(glm.mat4.create(), transformModule.getModelMatrix());
		this._viewProjMat = glm.mat4.mul(glm.mat4.create(), this.projMat, this._viewMat);

		this.updateFrustumPlanes();

		return this._viewProjMat;
	};

	/**
	 * Returns the view matrix.
	 * @return {mat4} Returns the view matrix.
	 */
	Camera.prototype.getViewMatrix = function()
	{
		return this._viewMat;
	};

	return Camera;
});