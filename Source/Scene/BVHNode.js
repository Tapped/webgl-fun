/**
 * A series of Scene related classes.
 * @module Scene
 */
define(["libs/gl-matrix", "DebugDraw"], function(glm, DebugDraw)
{
	/**
	 * A base class for nodes in a bounding volume hierarchy.
	 * @constructor
 	 * @class BVHNode
 	 * @param {BVHNode} parentNode parentNode node. Optional
 	 * @param {AABBox} volume Bounding volume. Optional
 	 * @param {GameObject} gameObject The GameObject. Optional
	 */
	BVHNode = function(parentNode, volume, gameObject)
	{
		/**
		 * This node's GameObject.
		 * @property gameObject
		 * @type {GameObject}
		 */
		this.gameObject = gameObject;

		/**
		 * Stores the bounding volume for the node.
		 * @property boundingVolume
		 * @type {AABBox}
		 */
		this.boundingVolume = volume;

		/**
		 * Stores a reference to the parentNode node.
		 * @property parentNode
		 * @type {BVHNode}
		 */
		this.parentNode = parentNode;

		/**
		 * Stores the child nodes, which are two.
		 * @property children
		 * @type {BVHNode}
		 */
		this.children = new Array(2);
	}

	/**
	 * Destroys the BVHNode, and efficently removes it from the tree.
	 * It will also delete the children.
	 * @method destroyMe 
	 * @return {BVHNode} The sibling node that was moved.
	 */
	BVHNode.prototype.destroyMe = function()
	{
		if(this.parentNode)
		{
			// Find our sibling.
			var sibling;
			if(this.parentNode.children[0] == this)
			{
				sibling = this.parentNode.children[1];
			}
			else
			{
				sibling = this.parentNode.children[0];
			}

			this.parentNode.boundingVolume = sibling.boundingVolume;
			this.parentNode.gameObject = sibling.gameObject;
			this.parentNode.children[0] = sibling.children[0];
			this.parentNode.children[1] = sibling.children[1];

			if(sibling.children[0])
			{
				sibling.children[0].parentNode = this.parentNode;
				sibling.children[1].parentNode = this.parentNode;
			}

			this.parentNode.recalculateVolume();

			return this.parentNode;
		}

		this.parentNode = null;

		return null;
	}

	/**
	 * Searches the tree for objects that are in the frustum,
	 * and fills an array with the result.
	 * @method findObjectsInFrustum
	 * @param {Plane} planes An array of planes.
	 */
	BVHNode.prototype.findObjectsInFrustum = function(planes)
	{
		var isInFrustum = this.boundingVolume.isInFrustum(planes);

		if(isInFrustum)
		{
			if(!this.isLeaf())
			{
				this.children[0].findObjectsInFrustum(planes);
				this.children[1].findObjectsInFrustum(planes);
			}
			else
			{
				this.gameObject.isVisible = true;
			}
		}
	};

	/**
	 * Returns true if this node is at the bottom of the hierarchy.
	 * @method isLeaf
	 * @return {Bool} True if this node is counted as a leaf, else false.
	 */
	BVHNode.prototype.isLeaf = function() 
	{
		return this.gameObject != null;
	};

	/**
	 * Insert a GameObject into the tree, with the given bounding volume.
	 * @method insert
	 * @param {GameObject} gameObject The GameObject.
	 * @param {AABBox} volume Bounding volume.
	 * @return {BVHNode, BVHNode} Returns one of the node that have been moved and the new node.
	 */
	BVHNode.prototype.insert = function(gameObject, volume)
	{
		// If we are a leaf, then we would move the gameObject
		// into the list of children.
		if(this.isLeaf())
		{
			//Copy of current
			this.children[0] = new BVHNode(this, this.boundingVolume, this.gameObject);

			//New node
			this.children[1] = new BVHNode(this, volume, gameObject);

			this.gameObject = null;

			//Recalculate our bounding volume.
			this.recalculateVolume();

			return {oldNode: this.children[0], newNode: this.children[1]};
		}
		else
		{
			var childOne = this.children[0];
			var childTwo = this.children[1];

			// We put the node in the child that would grow the least.
			if(childOne.boundingVolume.getGrowth(volume) <
			   childTwo.boundingVolume.getGrowth(volume))
			{
				return childOne.insert(gameObject, volume);
			}
			else
			{
				return childTwo.insert(gameObject, volume);
			}
		}
	};

	/**
	 * Recalculate the bounding volume for this node.
	 * It would only work for non-leaf nodes.
	 * @method recalculateVolume
	 */
	BVHNode.prototype.recalculateVolume = function()
	{
		if(!this.isLeaf())
		{
			var childOne = this.children[0];
			var childTwo = this.children[1];
			this.boundingVolume = new AABBox.createFromAABBox(childOne.boundingVolume, 
															  childTwo.boundingVolume);
		}

		if(this.parentNode)
		{
			this.parentNode.recalculateVolume();
		}
	};

	/**
	 * Debug draw the node, and its children.
	 * @method debugDraw
	 * @param {Scene} scene The scene to draw to.
	 */ 
	BVHNode.prototype.debugDraw = function(scene, level)
	{
		if(!level)
		{
			level = 0;
		}

		DebugDraw.drawCube(scene, gl.TRIANGLES, glm.vec3.scale(glm.vec3.create(),
							this.boundingVolume.getSize(), 0.51),
						   glm.quat.create(), this.boundingVolume.center(), 
						   glm.vec4.fromValues(1 - level, level / 3, 1, 0.5));
		if(!this.isLeaf())
		{
			++level;
			this.children[0].debugDraw(scene, level);
			this.children[1].debugDraw(scene, level);
		}
	};

	return BVHNode;
});