define(["Scene/BaseLight", "libs/gl-matrix"], function(BaseLight, glm)
{
	/**
	 * Directional light, only support for one per scene!
	 * @class DirectLight
	 * @constructor
	 */
	DirectLight = function()
	{
		this.name = "DirectLight";

		/**
		 * Light direction
		 * @property direction
		 * @type vec3 
		 */
		this.direction = glm.vec3.create();

		/**
		 * The base light part of the light.
		 * @property base
		 * @type BaseLight
		 */
		this.base = new BaseLight();
	}

	return DirectLight;
});