define(["libs/gl-matrix", "Scene/BaseLight", 
		"Modules/BoundingVolume", "Math/AABBox", "Scene/GameObject"], 
	function(glm, BaseLight, BoundingVolume, AABBox, GameObject)
{
	/**
	 * Point light is a type of light, that simulates
	 * lights that goes in all direction however with attenuation.
	 * 
	 * A light bulb is in this category.
	 * @class PointLight
	 * @constructor
	 * @extends BaseLight
	 * @extends GameObject
	 */
 	PointLight = function()
 	{
		this.name = "PointLight";

		/**
		 * The position of the point light.
		 * @property position
		 * @type vec3
		 */
		this.position = glm.vec3.create();

		/**
		 * Attenuation factors.
		 * @property attenuation
		 * @type {constant, linear, exp}
		 */
		this.attenuation = {constant: 0, linear: 0, exp: 0};

		/**
		 * The point light is dirty if one of the members have been changed.
		 * The user must set this, to update the bounding volume.
		 * @property isDirty
		 * @type Bool
		 */
		this.isDirty = false;

		/**
		 * Base light data.
		 * @property base
		 * @type BaseLight
		 */
		this.base = new BaseLight();
 	} 

	PointLight.prototype.constructor = PointLight;

	/**
	 * Updates the bounding volume for the light.
	 * @method update
	 */
	PointLight.prototype.update = function()
	{
		var volume = this.gameObject.getModule("BoundingVolume");
		if(volume && (this.isDirty || !volume.boundingVolume))
		{
			var radius = 2.0 * this.calculateRadius();
			var aabb = new AABBox;
			var pointA = glm.vec3.sub(glm.vec3.create(),
										     this.position, 
										     glm.vec3.fromValues(radius, radius, radius));
			var pointB = glm.vec3.add(glm.vec3.create(),
										     this.position, 
										     glm.vec3.fromValues(radius, radius, radius));
			aabb.addPoints(pointA);
			aabb.addPoints(pointB);
			volume.set(aabb);
			volume._isDirty = true;
		}
		else
		{
			volume._isDirty = false;
		}
	} 

	/**
	 * Calculates the radius of the point light,
	 * according to the parameters of the light.
	 * @method calculateRadius
	 * @return {Number} The radius of the point light.
	 */
	PointLight.prototype.calculateRadius = function() 
	{
		var maxChannel = Math.max(Math.max(this.base.color[0], this.base.color[1]), this.base.color[2]);
		return (-this.attenuation.linear + 
					Math.sqrt(this.attenuation.linear * this.attenuation.linear - 
					4 * this.attenuation.exp * (this.attenuation.exp - maxChannel * 
							                        this.base.diffuseIntensity))) /
					2 * this.attenuation.exp;
	};

	return PointLight;
});