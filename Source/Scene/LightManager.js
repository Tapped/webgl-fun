define(["Scene/PointLight", "libs/gl-matrix"], function(PointLight, glm)
{
    /**
     * LightManager manages your lights.
     * @class LightManager
     * @constructor
     */
    LightManager = function()
    {
        /**
         * Stores the point lights.
         * @private
         * @property _pointLights
         */
        this._pointLights = new Object;

        /**
         * The framebuffer object used 
         * for the off-screen rendering.
         * @private
         * @property _fbo
         */
        this._fbo = null;

        /**
         * Stores the textures.
         * @private
         * @property _textures
         */
        this._textures = new Array();

        /**
         * Our directional light.
         * @private
         * @property _dirLight
         * @type DirectLight
         */
        this._dirLight = null;

        /**
         * The light mode used for rendering.
         * Can either be LightModes.DEFERRED_RENDERING or LightModes.FORWARD_RENDERING
         * @property _lightMode
         */
        this._lightMode = LightManager.LightModes.FORWARD_RENDERING;

        this._tmpPointLights = new Array();
    }

    LightManager.LightModes = {DEFERRED_RENDERING: 1, FORWARD_RENDERING: 2};

    LightManager.NUM_TEXTURES = 4;

    LightManager.MAX_POINT_LIGHT_RENDERING = 5;

    /**
     * Adds a light to the light manager.
     *
     * NOTE: There can only be one direct light per scene!
     * @method addLight
     * @param {BaseLight} light Light to be added.
     */
    LightManager.prototype.addLight = function(light) 
    {
        var pointLightMod = light.getModule("PointLight");
        var dirLightMod = light.getModule("DirectLight");

        // If the light is a point light, add the point light to the list.
        if(pointLightMod)
        {
            this._pointLights[light.name] = light;
        }
        else if(dirLightMod)
        {
            this._dirLight = light;
        }
    };

    /**
     * Removes a light from the light manager.
     * @method removeLight
     * @param {String} name Name of light.
     */
    LightManager.prototype.removeLight = function(name)
    {
        delete this._pointLights[name];
    };

    /**
     * Setups what's needed to render the lights for the specific material
     * @method useOn
     * @param {Material} material The material that reflects/absorbs the lights.
     */
    LightManager.prototype.useOn = function(material)
    {
        if(this._tmpPointLights.length == 0)
        {
            for(var i in this._pointLights)
            {
                var light = this._pointLights[i];
                if(light.isVisible && 
                    this._tmpPointLights.length < LightManager.MAX_POINT_LIGHT_RENDERING)
                {
                    this._tmpPointLights.push(light);
                }
            }
        }

        material.setUniform("wgf_numPointLights", this._tmpPointLights.length,
                            Material.UNIFORM_TYPES.INT);
        for(var i = 0;i < this._tmpPointLights.length;i++)
        {
            var light = this._tmpPointLights[i];
            var lightModule = light.getModule("PointLight");
            var rootName = "wgf_pointLights[" + i + "].";
            material.setUniform(rootName + "position", lightModule.position);
            material.setUniform(rootName + "attenuation", 
            glm.vec3.fromValues(
                lightModule.attenuation.constant, 
                lightModule.attenuation.linear,
                lightModule.attenuation.exp));
            material.setUniform(rootName + "base.color", lightModule.base.color);
            material.setUniform(rootName + "base.intensity", 
                        glm.vec2.fromValues(lightModule.base.ambientIntensity, lightModule.base.diffuseIntensity));
        }

        if(this._dirLight)
        {
            var lightModule = this._dirLight.getModule("DirectLight");
            material.setUniform("wgf_dirLightBase.color", lightModule.base.color);
            material.setUniform("wgf_dirLightBase.intensity", 
                glm.vec2.fromValues(lightModule.base.ambientIntensity, lightModule.base.diffuseIntensity));
            material.setUniform("wgf_dirLightDir", lightModule.direction);
        }
    };

    /**
     * Resets information that were temporary used during rendering
     * for optimization.
     * @method reset
     */
    LightManager.prototype.reset = function()
    {
        this._tmpPointLights.length = 0;
    };

    /**
     * Initialize the light manager for rendering.
     * @method init
     * @param {Number} contextWidth Width of context.
     * @param {Number} contextHeight Height of context.
     * @param {LightModes} lightMode The type of lightning used for rendering. 
     */
    LightManager.prototype.init = function(contextWidth, contextHeight, lightMode)
    {
        if(lightMode != null)
        {
            this._lightMode = lightMode;
        }

        if(this._lightMode == LightManager.LightModes.DEFERRED_RENDERING)
        {
            console.log("Deferred rendering is under development!");
            this._fbo = gl.createFramebuffer();
            gl.bindFramebuffer(gl.FRAMEBUFFER, this._fbo);

            for(var i = 0;i < LightManager.NUM_TEXTURES;i++)
            {
                var texture = gl.createTexture();
                gl.bindTexture(gl.TEXTURE_2D, texture);
                //gl.texImage2D(gl.TEXTURE_2D, 0, )
            }

            gl.bindFramebuffer(gl.FRAMEBUFFER, 0);
        }
    };

    return LightManager;
});