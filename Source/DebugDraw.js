define(["libs/gl-matrix", "Mesh/CubeMesh"], function(glm, cubeMesh)
{
	/**
	 * @class DebugDraw
	 * @static
	 */
	DebugDraw = {};

    /**
     * Draws a cube. You can set the rendering mode.
     * @method drawCube
     * @param {Scene} scene The scene to draw to.
     * @param {gl.TRIANGLES, gl.LINES, gl.POINTS} type The rendering mode.
     * @param {vec3} scale Scale of cube.
     * @param {quat} orient Orientation of cube.
     * @param {vec3} position Position of cube.
     * @param {vec4} color The color of the cube.
     */
    DebugDraw.drawCube = function(scene, type, scale, orient, position, color)
    {
		var scaleMat = glm.mat4.create(); 
		scaleMat[0] = scale[0];
		scaleMat[5] = scale[1];
		scaleMat[10] = scale[2];

    	var modelMat = glm.mat4.fromRotationTranslation(glm.mat4.create(), orient, position);
    	glm.mat4.mul(modelMat, modelMat, scaleMat);

    	var mesh = new cubeMesh;
    	scene.addDebugObject({mesh: mesh, modelMatrix: modelMat, color: color, type: type});
    };

    return DebugDraw;
});