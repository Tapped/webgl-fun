/**
 * Math module contains math related stuff.
 *
 * @module Math
 */

/**
 * Some extra functions for glMatrix.vec3.
 *
 * @class vec3
 * @static
 */
define(["libs/gl-matrix", "Math/MathExt"], function(glMatrix)
{
	/**
	 * Checks if two vectors are equal.
	 *
	 * It does a safe floating point check.
	 * @method isEqual
	 * @param {vec3} a Left side
	 * @param {vec3} b Right side
	 * @return {Bool} Returns true if the vectors are equal, else false. 
	 */
	glMatrix.vec3.isEqual = function(a, b)
	{
		return Math.isEqual(a[0], b[0]) && 
			   Math.isEqual(a[1], b[1]) &&
			   Math.isEqual(a[2], b[2]);
	}

	/**
	 * Computes an out code for a point.
	 * 
	 * An outcode is a flag variable, 
     * which specifices which of the clip planes 
     * that the point is outside of.
     *
     * The data is stored in each bit as following shows:
     *
     *     | Clip plane = Bit |
     *     |    Left    =  0  |
     *     |    Right   =  1  |
     *     |    Bottom  =  2  |
     *     |    Top     =  3  |
     *     |    Near    =  4  |
     *     |    Far     =  5  |
     *
     * @method computeOutCode
	 * @param {vec3} p The point in question.
	 * @param {mat4} m The clip matrix.
	 */
	glMatrix.vec3.computeOutCode = function(p, m)
	{
		var code = 0;
		var point = glMatrix.vec4.fromValues(p[0], p[1], p[2], 1.0);
		glMatrix.vec4.transformMat4(point, point, m);

        var w = point[3];
        if(point[0] < -w)
            code |= 0x01;
        if(point[0] > w)
            code |= 0x02;
        if(point[1] < -w)
            code |= 0x04;
        if(point[1] > w)
            code |= 0x08;
        if(point[2] < -w)
            code |= 0x10;
        if(point[2] > w)
            code |= 0x20;

        return code;
	};
});