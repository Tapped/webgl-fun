define(["libs/gl-matrix"], function(glm)
{
	/**
	 * Plane is a mathematical representation of an infinity subdivision of space.
	 * @class Plane
	 */
	Plane = function()
	{
		/**
		 * The plane normal.
		 * @property normal
		 * @type vec3
		 */
		this.normal = glm.vec3.create();

		/**
		 * The d that solves the plane equation.
		 * @property d
		 * @type Number
		 */
		this.d = 0.0;
	}

	/**
	 * Set the plane coefficients, it will normalize the result too.
	 *
	 * In other words:
	 *
	 * 	 a(x1 - x0) + b(y1 - y0) + c(z1 - z0) + d = 0
	 *
	 * Where the normal is n = [a, b, c].
	 * @method setCoefficients
	 * @param {Number} a The x coordinate of the plane normal.
	 * @param {Number} b The y coordinate of the plane normal.
	 * @param {Number} c The z coordinate of the plane normal.
	 * @param {Number} d The plane constant.
	 */
	Plane.prototype.setCoefficients = function(a, b, c, d) 
	{
		this.normal = glm.vec3.fromValues(a, b, c);
		var length = glm.vec3.len(this.normal);
		glm.vec3.scale(this.normal, this.normal, 1.0 / length);
		this.d = d /length;
	};

	/**
	 * Computes the distance from the plane to a point.
	 * @method distance
	 * @param {Number} p The point to compute distance for.
	 * @return {Number} The distance to the point.
	 */
	Plane.prototype.distance = function(p)
	{
		// Calculates the distance.
		return glm.vec3.dot(p, this.normal) + this.d;
	};

	return Plane;
});