/**
 * Bounding volumes stores different types of volumes used to encapsulate objects.
 *
 * @module Bounding Volumes
 */
define(["libs/gl-matrix"], function(glm)
{
    /** 
     * AABBox is an axially aligned bounding box.
     * @constructor
     * @class AABBox
     */
    AABBox = function()
    {
        /**
         * Minimum point of an AABBox.
         * @property min
         * @type vec3
         */
        this.min = glm.vec3.fromValues(Number.MAX_VALUE, Number.MAX_VALUE, Number.MAX_VALUE);

        /**
         * Maximum point of an AABBox.
         * @property max
         * @type vec3
         */
        this.max = glm.vec3.fromValues(-Number.MAX_VALUE, -Number.MAX_VALUE, -Number.MAX_VALUE);
    }

    /**
     * Clones the AABBox.
     * @method clone
     * @return {AABBox} Returns a new instance of a AABBox.
     */
    AABBox.prototype.clone = function()
    {
        var aabb = new AABBox;
        aabb.min = glm.vec3.clone(this.min);
        aabb.max = glm.vec3.clone(this.max);
        return aabb;
    };

    /**
     * Returns one of the corner points.
     *
     *        .6------7
     *      .' |    .'|
     *     2---+--3'  |
     *     |   |  |   |
     *     |  ,4--+---5
     *     |.'    | .' 
     *     0------1'
     * 
     * @method getCornerPoint
     * @param {Number} index The index of the corner point.
     * @return {vec3} The corner point. 
     */
    AABBox.prototype.getCornerPoint = function(index)
    {
        return glm.vec3.fromValues((index & 1) ? this.max[0] : this.min[0],
                                   (index & 2) ? this.max[1] : this.min[1],
                                   (index & 3) ? this.min[2] : this.max[2]);
    };

    /**
     * Compute an outcode for an AABBox in clipspace.
     * 
     * An outcode is a flag variable, 
     * which specifices which of the clip planes 
     * that the AABBox is outside of.
     *
     * The data is stored in each bit as following shows:
     *
     *     | Clip plane = Bit |
     *     |    Left    =  0  |
     *     |    Right   =  1  |
     *     |    Bottom  =  2  |
     *     |    Top     =  3  |
     *     |    Near    =  4  |
     *     |    Far     =  5  |
     *
     * @method computeOutCode
     * @param {mat4} clipMat The clip matrix to use.
     */
    AABBox.prototype.computeOutCode = function(clipMat)
    {
        var offScreen = true;
        for(var i = 0;i < 8;i++)
        {
            var point = this.getCornerPoint(i);
            offScreen = offScreen && glm.vec3.computeOutCode(point, clipMat);
        }

        return offScreen;
    };

    /**
     * Checks if the AABBox is inside the frustum, 
     * defined by a set of six planes.
     * @param {Plane} planes An array of planes.
     * @return {Bool} Returns true if the AABBox is inside the frustum.
     */
    AABBox.prototype.isInFrustum = function(planes)
    {
        for(var i = 0;i < 6;i++)
        {
            var vertices = this.computeVertexNP(planes[i].normal);
            if(planes[i].distance(vertices.p) < 0)
                return false;
        }

        return true;
    };

    /**
     * Returns the center of the AABBox.
     * @method center
     * @return {vec3} The center of the AABBox.
     */
    AABBox.prototype.center = function()
    {
        var minMax = glm.vec3.add(glm.vec3.create(), this.min, this.max);
        return glm.vec3.scale(minMax, minMax, 0.5);
    };

    /**
     * Computes the positive and negative vertex projected on the normal.
     * Used for faster frustum check.
     * @method computeVertexNP
     * @param {vec3} normal The normal
     * @return {vec3 p, vec3 n} Returns the positive vertex and negative vertex.
     */
    AABBox.prototype.computeVertexNP = function(normal)
    {
        var p = glm.vec3.clone(this.min);
        var n = glm.vec3.clone(this.max);
        if(normal[0] >= 0)
        {
            p[0] = this.max[0];
            n[0] = this.min[0];
        }
        if(normal[1] >= 0)
        {
            p[1] = this.max[1];
            n[1] = this.min[1];
        }
        if(normal[2] >= 0)
        {
            p[2] = this.max[2];
            n[2] = this.min[2];
        }

        return {p: p, n: n};
    };

    /**
     * Set the AABBox to a transformed box.
     * @method setToTransformedBox
     * @param {AABBox} box The box that becomes transformed.
     * @param {mat4} m The matrix which transforms the given box.
     */
    AABBox.prototype.setToTransformedBox = function(box, m)
    {
        if(this == box)
        {
            box = this.clone();
        }

        // First take the translation part.
        this.min[0] = this.max[0] = m[12];
        this.min[1] = this.max[1] = m[13];
        this.min[2] = this.max[2] = m[14];

        if(m[0] > 0)
        {
            this.min[0] += m[0] * box.min[0]; this.max[0] += m[0] * box.max[0];
        }
        else
        {
            this.min[0] += m[0] * box.max[0]; this.max[0] += m[0] * box.min[0];
        }

        if(m[1] > 0)
        {
            this.min[0] += m[1] * box.min[0]; this.max[0] += m[1] * box.max[0];
        }
        else
        {
            this.min[0] += m[1] * box.max[0]; this.max[0] += m[1] * box.min[0];
        }

        if(m[2] > 0)
        {
            this.min[0] += m[2] * box.min[0]; this.max[0] += m[2] * box.max[0];
        }
        else
        {
            this.min[0] += m[2] * box.max[0]; this.max[0] += m[2] * box.min[0];
        }

        if(m[4] > 0)
        {
            this.min[1] += m[4] * box.min[1]; this.max[1] += m[4] * box.max[1];
        }
        else
        {
            this.min[1] += m[4] * box.max[1]; this.max[1] += m[4] * box.min[1];
        }

        if(m[5] > 0)
        {
            this.min[1] += m[5] * box.min[1]; this.max[1] += m[5] * box.max[1];
        }
        else
        {
            this.min[1] += m[5] * box.max[1]; this.max[1] += m[5] * box.min[1];
        }

        if(m[6] > 0)
        {
            this.min[1] += m[6] * box.min[1]; this.max[1] += m[6] * box.max[1];
        }
        else
        {
            this.min[1] += m[6] * box.max[1]; this.max[1] += m[6] * box.min[1];
        }

        if(m[8] > 0)
        {
            this.min[2] += m[8] * box.min[2]; this.max[2] += m[8] * box.max[2];
        }
        else
        {
            this.min[2] += m[8] * box.max[2]; this.max[2] += m[8] * box.min[2];
        }

        if(m[9] > 0)
        {
            this.min[2] += m[9] * box.min[2]; this.max[2] += m[9] * box.max[2];
        }
        else
        {
            this.min[2] += m[9] * box.max[2]; this.max[2] += m[9] * box.min[2];
        }

        if(m[10] > 0)
        {
            this.min[2] += m[10] * box.min[2]; this.max[2] += m[10] * box.max[2];
        }
        else
        {
            this.min[2] += m[10] * box.max[2]; this.max[2] += m[10] * box.min[2];
        }
    };

    /**
     * Make the AABBox encapsulate a set of points.
     * @method addPoints
     * @param {Array.Number} points The points represented as 3 x numbers for each point.
     */
    AABBox.prototype.addPoints = function(points) 
    {
        for(var i = 2;i < points.length;i += 3)
        {
            var pointX = points[i - 2];
            var pointY = points[i - 1];
            var pointZ = points[i];

            if(pointX < this.min[0]) 
                this.min[0] = pointX;
            if(pointX > this.max[0]) 
                this.max[0] = pointX; 

            if(pointY < this.min[1]) 
                this.min[1] = pointY;
            if(pointY > this.max[1])
                this.max[1] = pointY;

            if(pointZ < this.min[2]) 
                this.min[2] = pointZ;
            if(pointZ > this.max[2])
                this.max[2] = pointZ;
        }
    };

    /**
     * Returns the size of the AABBox.
     * @method getSize
     * @return {vec3} Size of AABBox.
     */
    AABBox.prototype.getSize = function()
    {
        return glm.vec3.sub(glm.vec3.create(), this.max, this.min);
    }

    /**
     * Returns the volume of the AABBox.
     * @method getVolume
     * @return {Number} Volume of AABBox.
     */
    AABBox.prototype.getVolume = function()
    {
        var size = this.getSize();
        return size[0] * size[1] * size[2];
    };

    /**
     * Returns the amount of growth needed to encapsulate an other AABB.
     * @method getGrowth
     * @param {AABBox} a The box to check.
     * @return {Number} The amount of growth needed, represented as volume.
     */
    AABBox.prototype.getGrowth = function(a)
    {
        return AABBox.createFromAABBox(this, a).getVolume();
    };

    /**
     * Check if two AABBs intersect, and return true if so.
     * @static
     * @method intersectAABB
     * @param {AABBox} a One of the AABBox to check for intersection.
     * @param {AABBox} b One of the AABBox to check for intersection.
     * @return {Bool} True if the boxes intersect, else false.
     */
    AABBox.intersectAABB = function(a, b)
    {
        if(a.min[0] > b.max[0]) return false;
        if(a.max[0] < b.min[0]) return false;
        if(a.min[1] > b.max[1]) return false;
        if(a.max[1] < b.min[1]) return false;
        if(a.min[2] > b.max[2]) return false;
        if(a.max[2] < b.min[2]) return false;

        return true;
    };

    /**
     * Checks if a point is inside the bounding box.
     * @method isPointInside
     * @param {vec3} p The point to check.
     * @return True 
     */
    AABBox.prototype.isPointInside = function(p)
    {
        return (p[0] >= this.min[0]) && (p[0] <= this.max[0]) &&
               (p[1] >= this.min[1]) && (p[1] <= this.max[1]) &&
               (p[2] >= this.min[2]) && (p[2] <= this.max[2]);  
    };

    /**
     * Create an new AABBox from two AABBoxes.
     * @static
     * @method createFromAABBox
     * @param {AABBox} a One of the AABBox to use in creation.
     * @param {AABBox} b One of the AABBox to use in creation.
     */
    AABBox.createFromAABBox = function(a, b)
    {
        var result = new AABBox;

        result.addPoints(a.min);
        result.addPoints(a.max);
        result.addPoints(b.min);
        result.addPoints(b.max);

        return result;
    }

    return AABBox;
});