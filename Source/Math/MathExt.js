/**
 * Math module contains math related stuff.
 *
 * @module Math
 */

/**
 * Added some constants and functions to the built in Math library.
 *
 * @class Math
 * @static
 */
define(function()
{
	/**
	 * Added a new constant to the built in Math library.
	 * The value equals to Pi / 360.
	 *
	 * @property Math.PI_OVER_360
	 * @type Float
	 */
	Math.PI_OVER_360 = Math.PI / 360.0;

	/**
	 * Added a new constant to the built in Math library.
	 * The value equals to Pi / 180.
	 *
	 * @property Math.PI_OVER_180
	 * @type Float
	 */
	Math.PI_OVER_180 = Math.PI / 180.0;

	/**
	 * Added a new constant to the built in Math library.
	 * The value equals to 180 / Pi.
	 *
	 * @property Math._180_OVER_PI
	 * @type Float
	 */
	Math._180_OVER_PI = 180.0 / Math.PI;

	/**
	 * Checks if two floating point numbers are equal.
	 *
	 * @method isEqual
	 * @param {Float} a Left side
	 * @param {Float} b Right side
	 * @param {Float} epsilon Epsilon defines the precision of the check. Optional, default: 0.00000001
	 * @return {Bool} Returns true if a == b, else false.
	 */
	Math.isEqual = function(a, b, epsilon)
	{
		if(!epsilon)
		{
			epsilon = 0.00000001;
		}
		return Math.abs(a - b) < epsilon;
	}

    /**
     * Converts a angle in degrees to radians.
     *
     * @method degToRad
     * @param {Float} deg Degrees to convert to radians.
     * @return {Float} The angle in radians.
     */
    Math.degToRad = function(deg) 
    {
        return Math.PI_OVER_180 * deg;
    };

    /**
     * Converts a angle in radians to degrees.
     *
     * @method radToDeg
     * @param {float} rad Radians to convert to degrees.
     * @return {Float} The angle in degrees.
     */
    Math.radToDeg = function(rad) 
    {
        return Math._180_OVER_PI * rad;
    };
});