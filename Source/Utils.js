/**
 * This class gives you access to some nice to have functions.
 *
 * @class Utils
 * @static
 */
 define(["Math/MathExt", "ShaderProgram"], function()
 {
 	Utils = {};

    var numLoadRequests = 0;
    var numComplete = 0;

	/**
     * Compiles a shader.
     * 
     * @method compileShader
     * @param {String} src The shader source.
     * @param {Object} shaderType The type of shader, which maybe VERTEX_SHADER or FRAGMENT_SHADER.
     * @return Returns null if an error, else the shader object.
     */
    Utils.compileShader = function(src, shaderType)
    {
        shader = gl.createShader (shaderType);
        gl.shaderSource(shader, src);
        gl.compileShader(shader);
        if (gl.getShaderParameter(shader, gl.COMPILE_STATUS) == 0)
        {
            alert("\n" + gl.getShaderInfoLog(shader));
        }

        return shader;
    }

    /**
     * Downloads a file from the server, using XMLHttpRequest.
     * If you want to load more than one file, you should instead use {{#crossLink "Utils/loadFiles:method"}}{{/crossLink}}.
     * 
     * @method loadFile
     * @param {String} url Location of file.
     * @param {Object} extraData This parameter would be passed 
     *                           to the callback function as a parameter.
     * @param {Function} callback A function that would be called when loading is ready.
     * @async
     */
    Utils.loadFile = function(url, extraData, callback)
    {
        var request = new XMLHttpRequest();
        request.open('GET', url, true);
        var realThis = this;

        // Hook the event that gets called as the request progresses
        request.onreadystatechange = function () 
        {
            // If the request is "DONE" (completed or failed)
            if (request.readyState == 4) 
            {
                // If we got HTTP status 200 (OK)
                if (request.status == 200) 
                {
                    callback.call(realThis, request.responseText, extraData);
                } else 
                { 
                    console.error("Couldn't download " + url);
                }
            }
        };

        request.send(null);
    }

    /**
     * Downloads files from the server, using XMLHttpRequest.
     *
     * @method loadFiles
     * @param {Array(String)} urls Locations of the files.
     * @param {Function} callback A function that would be called when loading is ready.
     * @async
     */
    Utils.loadFiles = function(urls, callback)
    {
        numLoadRequests += urls.length;
        var result = new Array;

        function callback_internal(str, urlIdx)
        {
            result[urlIdx] = str;
            ++numComplete;

            if(numComplete == numLoadRequests)
            {
                callback.call(this, result);
            }
        }

        for(var i = 0;i < numLoadRequests;++i)
        {
            Utils.loadFile.call(this, urls[i], i, callback_internal);
        }
    }

    /**
     * Creates a shader program, and attaches the given shaders, 
     * and finally links the program.
     *
     * @method createShaderProgram
     * @param {Shader} shaders Shader objects, which would be attached to the program.
     * @param {Strings} attributes An array of strings, which stores the name of the program attributes in GLSL. 
     * @return {Program} Returns the program object, else null. 
     */
    Utils.createShaderProgram = function(shaders, attributes)
    {
        var prog = gl.createProgram();

        for(var i = 0;i < shaders.length;++i)
        {
            gl.attachShader(prog, shaders[i]);
        }
        
        for(var i = 0;i < attributes.length;++i)
        {
            gl.bindAttribLocation(prog, i, attributes[i]);
        }
        
        gl.linkProgram(prog);

        return ShaderProgram._create(prog);
    }

    /**
     * Implements isInteger for all browsers.
     *
     * Found here:
     * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger
     *
     * @method Number.isInteger
     * @param {Number} nVal Number to check.
     * @return {Bool} Returns true if nVal is a integer.
     */
    if (!Number.isInteger) 
    {
        Number.isInteger = function isInteger (nVal) 
        {
            return typeof nVal === "number" && isFinite(nVal) && nVal > -9007199254740992 && nVal < 9007199254740992 && Math.floor(nVal) === nVal;
        };
    }

    return Utils;
 });