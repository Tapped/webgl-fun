/**
 * A series of Game Object modules.
 * @module Modules
 */
define(["libs/gl-matrix"], function(glm)
{
	/**
	 * Transformation is a module that can be added to a game object.
	 * It gives the game object the ability to transform itself.
	 * Transformation yields to scale, orientation and translation.
	 *
	 * @class Transformation
	 * @constructor
	 */
	Transformation = function()
	{
		/**
		 * Name of the module
		 * 
		 * @property name
		 * @type String
		 */
		this.name = "Transformation";

		/**
		 * Stores the orientation.
	 	 *
		 * @private
		 * @property _orientation
		 * @type quat
		 * @default [0, 0, 0, 1]
		 */
		this._orientation = glm.quat.create();

		/**
		 * Stores the position.
		 *
		 * @private
		 * @property _position
		 * @type vec3
		 * @default [0, 0, 0]
		 */
		this._position = glm.vec3.create();

		/**
		 * Stores the scale.
		 *
		 * @private
		 * @property _scale
		 * @type vec3
		 * @default [1, 1, 1]
		 */
		this._scale = glm.vec3.fromValues(1, 1, 1);

		/**
		 * Stores the model matrix.
		 * 
		 * The matrix may not be up to date.
		 * Use {{#crossLink "Transformation/getModelMatrix:method"}}{{/crossLink}} to get the matrix!
		 *
		 * @private
		 * @property _modelMatrix
		 * @type mat4
		 */
		this._modelMatrix = glm.mat4.create();

		/**
		 * True when the model matrix has not been updated.
		 * @private
		 * @property _isDirty
		 * @type Bool
		 * @default false
		 */
		this._isDirty = false;
	}

	/**
	 * Set the position.
	 * @method setPosition
	 * @param {vec3} p Position to set.
	 */
	Transformation.prototype.setPosition = function(p)
	{
		this._position = glm.vec3.clone(p);
		this._isDirty = true;
	};

	/**
	 * Set the scale.
	 * @method setScale
	 * @param {vec3} s Scale to set.
	 */
	Transformation.prototype.setScale = function(s)
	{
		this._scale = glm.vec3.clone(s);
		this._isDirty = true;
	};

	/**
	 * Set the orientation.
	 * @method setOrientation
	 * @param {quat} o Orientation to set.
	 */
	Transformation.prototype.setOrientation = function(o)
	{
		this._orientation = glm.quat.clone(o);
		this._isDirty = true;
	};

	/**
	 * Get the position.
	 * @method getPosition
	 * @return {vec3} Returns a copy of the position.
	 */
	Transformation.prototype.getPosition = function()
	{
		return glm.vec3.clone(this._position);
	};

	/**
	 * Get the scale.
	 * @method getScale
	 * @return {vec3} Returns a copy of the scale.
	 */
	Transformation.prototype.getScale = function()
	{
		return glm.vec3.clone(this._scale);
	};

	/**
	 * Get the orientation.
	 * @method getOrientation
	 * @return {quat} Returns a copy of the orientation.
	 */
	Transformation.prototype.getOrientation = function()
	{
		return glm.quat.clone(this._orientation);
	};

	/**
	 * Returns the model matrix, 
	 * which is calculated from the orientation, translation and scale.
	 *
	 * @method getModelMatrix
	 * @return {mat4} The model matrix.
	 */
	Transformation.prototype.getModelMatrix = function() 
	{
		if(this._isDirty)
		{
			this._updateMatrix();
			this._isDirty = false;
		}
		return this._modelMatrix;
	};

	/**
	 * Updates the model matrix.
	 * @private
	 * @method _updateMatrix
	 */
	Transformation.prototype._updateMatrix = function()
	{
		//Optimized scaling, because we are working with a identity matrix.
		var scaleMat = glm.mat4.create(); 
		scaleMat[0] = this._scale[0];
		scaleMat[5] = this._scale[1];
		scaleMat[10] = this._scale[2];

		glm.mat4.fromRotationTranslation(this._modelMatrix, this._orientation, this._position);
		glm.mat4.multiply(this._modelMatrix, this._modelMatrix, scaleMat);
	};

	return Transformation;
});