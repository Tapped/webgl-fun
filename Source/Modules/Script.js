/**
 * A series of Game Object modules.
 * @module Modules
 */
 define(function()
 {
 	/**
 	 * Script is a Game Object module, which gives you the ability to add logic to your Game Object.
 	 * Inherit this class and override the functions that you want.
 	 * @class Script
 	 * @constructor
 	 * @param {String} The name of the string.
 	 */
 	Script = function(name)
 	{
		/**
		 * Name of the script/module
		 * 
		 * @property name
		 * @type String
		 */
 		this.name = name;

 		/**
 		 * The GameObject that we belongs to.
 		 * @property gameObject
 		 * @type GameObject
 		 */
 		this.gameObject = null;

 		/**
 		 * True if we are a script.
 		 * Used to check for module type.
 		 * @property isScript
 		 * @default true
 		 */
 		this.isScript = true;
 	}

 	/**
 	 * Start is called when this module is added to a GameObject.
 	 * @method start
 	 */
 	Script.prototype.start = function() 
 	{
 	};

 	/**
 	 * Update is called for each update frame.
 	 * @method update
 	 */
 	Script.prototype.update = function()
 	{

 	};

 	/**
 	 * Exit is called when this module is removed from a GameObject.
 	 * @method exit
 	 */
 	Script.prototype.exit = function()
 	{

 	};

 	return Script;
 });