/**
 * A series of Game Object modules.
 * @module Modules
 */
define(["Math/AABBox"], function(AABBox)
{
	/**
	 * Mesh is a GameObject module, which stores 3D data, used during rendering.
	 * It stores the data on the GPU.
	 * @class Mesh
	 * @constructor
	 */
	Mesh = function()
	{
		/**
		 * Name of the module
		 * 
		 * @property name
		 * @type String
		 */
		this.name = "Mesh";

		/**
		 * Vertex buffer object for the vertices, normals and uv-coordinates. 
		 *
		 * @private
		 * @property _vbo
		 */
		this._vbo = null;

		/** 
		 * Index buffer object used for triangle indices.
		 * @private
		 * @property _ibo
		 */
		this._ibo = null;

		this._numIndices = 0;

		this._indexDataType = 0;

		this._uvCoordsOffset = 0; 

		/**
		 * The render mode, which may be any GL render mode supported.
		 *
		 * @property renderMode
		 * @type GLenum
		 * @default	gl.TRIANGLES
		 */
		this.renderMode = gl.TRIANGLES;

		/**
		 * Stores the bounding volume for the mesh.
		 * @property boundingVolume
		 * @type AABBox
		 */
		this.boundingVolume = new AABBox;

		/**
		 * Number of vertices for this mesh.
		 * NOTE: READ ONLY 
		 *
		 * @property numVertices
		 * @type Int
		 * @default 0 
		 */
		this.numVertices = 0;

		/**
		 * True if the mesh have normals.
		 *
		 * @property hasNormals
		 * @type Bool
		 * @default	false
		 */
		this.hasNormals = false;

		/**
		 * True if the mesh have uv-coordinates.
		 *
		 * @property hasUVCoords
		 * @type Bool
		 * @default	false
		 */
		this.hasUVCoords = false;
	}

	/**
	 * Sets the vertices of the mesh, including normals and texture coords, if you want.
	 * 
	 * @method setVertices
	 * @param {vec3} vertices An array of vec3 for each vertex.
	 * @param {vec3} normals The normals of the mesh.
	 * @param {vec2} uvCoords The uv coordinates of the mesh.
	 */
	Mesh.prototype.setVertices = function(vertices, normals, uvCoords) 
	{
		// Create bounding volume.
		this.boundingVolume = new AABBox;
		this.boundingVolume.addPoints(vertices);

		if(this._vbo)
		{
			// Be sure to delete the old _vbo
			gl.deleteBuffer(this._vbo);
		}

		this._vbo = gl.createBuffer();

		var bufferSize = vertices.length;
		if(normals)
		{
			bufferSize += normals.length;
		}
		if(uvCoords)
		{
			bufferSize += uvCoords.length;
		}

		this.numVertices = vertices.length / 3;
		this._uvCoordsOffset = vertices.length;

		var buffer = new Float32Array(bufferSize);
		buffer.set(vertices);

		if(normals)
		{
			buffer.set(normals, vertices.length);
			this.hasNormals = true;
			this._uvCoordsOffset += normals.length;
		}

		if(uvCoords)
		{
			buffer.set(uvCoords, this._uvCoordsOffset);
			this.hasUVCoords = true;
		}

		gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo);
		gl.bufferData(gl.ARRAY_BUFFER, buffer, gl.STATIC_DRAW);
	};

	/**
	 * Set the indices of the mesh.
	 *
	 * @method setIndices
	 * @param {int} indices The indices of the mesh.
	 */
	Mesh.prototype.setIndices = function(indices)
	{
		if(this._ibo)
		{
			gl.deleteBuffer(this._ibo);
		}
		
		this._ibo = gl.createBuffer();
		this._numIndices = indices.length;

		// Find the most suitable data type for the indices.
		var buffer;
		if(this.numVertices > 0xFFFF)
		{
			this._indexDataType = gl.UNSIGNED_INT;
			buffer = new Uint32Array(indices);
		}
		else if(this.numVertices > 0xFF)
		{
			this._indexDataType = gl.UNSIGNED_SHORT;
			buffer = new Uint16Array(indices);
		}
		else
		{
			this._indexDataType = gl.UNSIGNED_BYTE;
			buffer = new Uint8Array(indices);
		}

		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._ibo);
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, buffer, gl.STATIC_DRAW);
	};

	/**
	 * Render the mesh.
	 * @method render
	 */
	Mesh.prototype.render = function()
	{
		if(this._ibo)
		{
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._ibo);
		}

		gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo);

		gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(0);

		gl.vertexAttribPointer(1, 3, gl.FLOAT, false, 0, this.numVertices * 3 * 4);
        gl.enableVertexAttribArray(1);

		gl.vertexAttribPointer(2, 2, gl.FLOAT, false, 0, this._uvCoordsOffset * 4);
        gl.enableVertexAttribArray(2);

        if(!this.hasNormals)
        {
        	gl.disableVertexAttribArray(1);
        }

        if(!this.hasUVCoords)
        {
			gl.disableVertexAttribArray(2);
        }

        if(this._ibo)
        {
        	gl.drawElements(this.renderMode, this._numIndices, this._indexDataType, 0);
        }
        else
        {
        	gl.drawArrays(this.renderMode, 0, this.numVertices);
        }
	};

	return Mesh;
});