/**
 * A series of Game Object modules.
 * @module Modules
 */
define(["Math/AABBox", "libs/gl-matrix", "Math/vec3Ext"], function(AABBox, glm)
{
	/**
	 * BoundingVolume is a module for GameObjects. 
	 * Bounding volume encapsulates an GameObject with a simple primitive.
	 * @class BoundingVolume
	 * @constructor
	 * @param {Bool} autoUpdate Set to true if you want the bounding volume 
	 * to update so that the GameObject always fit.
	 */
	BoundingVolume = function(autoUpdate)
	{
		/**
		 * Name of the module
		 * 
		 * @property name
		 * @type String
		 */
		this.name = "BoundingVolume";

		/**
		 * Stores the bounding volume data.
		 * @property boundingVolume 
		 */
		this.boundingVolume = null;

		/**
		 * If this property is true, the bounding volume would be linked with the
		 * GameObject, when added.
		 * @property autoUpdate
		 */
		this.autoUpdate = autoUpdate;

		/**
		 * True if the bounding volume have changed lastly.
		 * @private
		 * @property _isDirty
		 * @type Bool
		 */
		this._isDirty = false;
	}

	/**
	 * Start the function.
	 * @method start
	 */
	BoundingVolume.prototype.start = function()
	{
		var mesh = this.gameObject.getModule("Mesh");
		if(mesh)
		{
			this.boundingVolume = mesh.boundingVolume;
		}
	}

	/**
	 * Function called by the GameObject.
	 * @private
	 * @method update
	 */
	BoundingVolume.prototype.update = function()
	{
		if(this.autoUpdate)
		{
			var transformation = this.gameObject.getModule("Transformation");
			var mesh = this.gameObject.getModule("Mesh");
			if(mesh && transformation)
			{
				var oldVolume = null;
				if(this.boundingVolume)
				{
					oldVolume = this.boundingVolume.clone();
				}
				this.boundingVolume = new AABBox;
				this.boundingVolume.setToTransformedBox(mesh.boundingVolume, 
									transformation.getModelMatrix());

				if(oldVolume)
				{
					if( !glm.vec3.isEqual(oldVolume.min, this.boundingVolume.min) ||
						!glm.vec3.isEqual(oldVolume.max, this.boundingVolume.max))
					{
						this._isDirty = true;
					}
					else
					{
						this._isDirty = false;
					}
				}
			}
		}
	};

	/**
	 * Set the bounding volume, which would overwrite the existing one.
	 * If you call the constructor with the autoUpdate parameter as true,
	 * the bounding volume would automatically be generated for you.
	 * 
	 * The autoUpdate uses the Mesh and Transformation module.
	 * @method set
	 * @param {AABBox} volume The volume that this instance would use.
	 */
	BoundingVolume.prototype.set = function(volume) 
	{
		this.boundingVolume = volume;
	};

	return BoundingVolume;
});