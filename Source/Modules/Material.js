/**
 * A series of Game Object modules.
 * @module Modules
 */
define(function()
{	
	/**
	 * Material is a Game Object module.
	 *
	 * Material is a handler for shader programs.
	 * Used during rendering.
	 *
	 * @class Material
	 * @constructor
	 * @param {ShaderProgram} prog Reference to a program, which the material will use.
	 */
	Material = function(prog)
	{
		/**
		 * Name of the module
		 * 
		 * @property name
		 * @type String
		 */
		this.name = "Material";

		/**
		 * ShaderProgram that belongs to this material.
		 * The program can be shared between materials.
		 *
		 * @private
		 * @property _program
		 * @type ShaderProgram
		 */
		this._program = prog;

		/** 
		 * Stores the values for the uniforms.
		 * @private
		 * @property _uniforms
		 * @type [String] = {value, type}
		 */
		this._uniforms = new Object;

		/**
		 * Textures that are bound to the material.
		 * @private
		 * @property _textures
		 * @type [String] = Texture
		 */
		this._textures = new Object;
	}

	/**
	 * The basic type of a uniform.
	 * @property UNIFORM_TYPES
	 * @type Int
	 */
	Material.UNIFORM_TYPES = {INT: 1, FLOAT: 2} 

	/**
	 * Set this material to current material.
	 *
	 * @method use
	 */
	Material.prototype.use = function() 
	{
		if(!this._program)
		{
			console.log("Can't use material without a shader program!");
			return;
		}

		this._program.use();

		var texCount = 0;
		for(var key in this._textures)
		{
			this._textures[key].bind(gl.TEXTURE0 + texCount);
			gl.uniform1i(this._program.getUniformLocation(key), texCount);

			++texCount;
		}

		for(key in this._uniforms)
		{
			var value = this._uniforms[key].value;
			var type = this._uniforms[key].type;
			var loc = this._program.getUniformLocation(key);
			
			if(type == Material.UNIFORM_TYPES.FLOAT)
			{
				if(value.length != null)
				{
					// I have tried to prioritize the if checks for optimization.
					if(value.length == 3)
					{
						gl.uniform3fv(loc, value);
					}
					else if(value.length == 16)
					{
						gl.uniformMatrix4fv(loc, gl.FALSE, value);
					}
					else if(value.length == 2)
					{
						gl.uniform2fv(loc, value);
					}
					else if(value.length == 4)
					{
						gl.uniform4fv(loc, value);
					}
					else if(value.length == 9)
					{
						gl.uniformMatrix3fv(loc, gl.FALSE, value);
					}
					else
					{
						console.log("Unknown number of elements for uniform " + key)
					}
				}
				else
				{
					gl.uniform1f(loc, value);
				}
			}
			else if(type == Material.UNIFORM_TYPES.INT)
			{
				if(value.length != null)
				{
					// I have tried to prioritize the if checks for optimization.
					if(value.length == 3)
					{
						gl.uniform3iv(loc, value);
					}
					else if(value.length == 16)
					{
						gl.uniformMatrix4iv(loc, gl.FALSE, value);
					}
					else if(value.length == 2)
					{
						gl.uniform2iv(loc, value);
					}
					else if(value.length == 4)
					{
						gl.uniform4iv(loc, value);
					}
					else if(value.length == 9)
					{
						gl.uniformMatrix3iv(loc, gl.FALSE, value);
					}
					else
					{
						console.log("Unknown number of elements for uniform " + key)
					}
				}
				else
				{
					gl.uniform1i(loc, value);
				}
			}
			else
			{
				console.log("Unknown type of uniform " + key + "\n GL only support float or int.");
			}
		}
	};

	/**
	 * Set the shader program for the material.
	 *
	 * @method setShaderProgram
	 * @param {ShaderProgram} prog The shader program.
	 */
	Material.prototype.setShaderProgram = function(prog)
	{
		this._program = prog;
		
		delete this._uniforms;
		this._uniforms = new Object();
	}

	/**
	 * Binds a texture to a uniform sampler.
	 * @method bindTexture
	 * @param {String} uniformName Name of the uniform.
	 * @param {Texture} texture The texture that becomes bound to the uniform.
	 */
	Material.prototype.bindTexture = function(uniformName, texture)
	{
		this._textures[uniformName] = texture;
	};

	/**
	 * Returns true if we got the specific uniform in the program.
	 * @method hasUniform
	 * @return True if we got the uniform else false.
	 */
	Material.prototype.hasUniform = function(uniformName)
	{
		return this._program.getUniformLocation(uniformName) != null;
	};

	/**
	 * Set a uniform.
	 * @method setUniform
	 * @param {String} name Name of the uniform to set.
	 * @param {int, float, vec2, vec3, vec4, mat3, mat4} value 
	 * Can be of different types depending on what you want.
	 * @param {UNIFORM_TYPES} type Type of uniform, which can either be INT, or FLOAT.
	 * Optional, default is FLOAT.
	 */
	Material.prototype.setUniform = function(name, value, type)
	{
		if(!type)
		{
			type = Material.UNIFORM_TYPES.FLOAT;
		}

		this._uniforms[name] = {value: value, type: type};
	};

	return Material;
});