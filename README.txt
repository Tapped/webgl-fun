********************************************************************
*
* WebGL-Fun is a library that makes WebGL both easy and fun.
* It is a very powerful library, because of the module based design.
*
********************************************************************


********************************************************************
*
* INSTALL
*
********************************************************************
You would need node.js to run the compressor and the doc generator.
http://nodejs.org/dist/v0.10.17/x64/node-v0.10.17-x64.msi

You can find a compressed version of the library in the Bin folder.

However the docs are not built. Follow this guide to install yuidoc:
http://yui.github.io/yuidoc/##install
But instead of running [yuidoc .] you execute CompileDocs.bat 
or use ServerDocs.bat to create a local server, where you can access
the docs here: http://127.0.0.1:3000/


********************************************************************
*
* USAGE
*
********************************************************************
You must have a webserver up and running.
So that you can use XHTMLRequest.

I highly recommend XAMPP, found here: 
http://sourceforge.net/projects/xampp/

Make a hard-link or move the library directory to the htdocs folder,
which lies where you installed XAMPP.

Then you can run one of the examples by opening the relevant HTML file.